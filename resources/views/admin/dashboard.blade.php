@extends('admin.layouts.master')
@section('content')
  <div class="social grid">
    <div class="grid-info">
    
    @if(((\Auth::user()->user_type)) == 1)   
        <div class="col-md-3 top-comment-grid">
          <a href="{!! route('customer') !!}">
            <div class="comments likes">
                <div class="comments-icon">
                    <i class="fa fa-user"></i>
                </div>
                <div class="comments-info likes-info">
                    <h3>{!! $user !!}</h3>
                    <a href="#">Total Users</a>
                </div>
                <div class="clearfix"> </div>
            </div>
          </a>
        </div>
        <div class="col-md-3 top-comment-grid">
          <a href="{!! route('customer') !!}">
            <div class="comments likes">
                <div class="comments-icon">
                    <i class="fa fa-user"></i>
                </div>
                <div class="comments-info likes-info">
                    <h3>{!! $newusers !!}</h3>
                    <a href="#">New Users</a>
                </div>
                <div class="clearfix"> </div>
            </div>
          </a>
        </div>
    @endif

    <div class="col-md-3 top-comment-grid">
          <a href="{!! route('customer') !!}">
            <div class="comments likes" style="background: #00b08c;">
                <div class="comments-icon">
                    <i class="fa fa-file"></i>
                </div>
                <div class="comments-info likes-info">
                    <h3>{!! $reports !!}</h3>
                    <a href="#">Total Reports</a>
                </div>
                <div class="clearfix"> </div>
            </div>
          </a>
        </div>
        <div class="col-md-3 top-comment-grid">
          <a href="{!! route('customer') !!}">
            <div class="comments likes" style="background: #00b08c;">
                <div class="comments-icon">
                    <i class="fa fa-file"></i>
                </div>
                <div class="comments-info likes-info">
                    <h3>{!! $newreports !!}</h3>
                    <a href="#">New Reports</a>
                </div>
                <div class="clearfix"> </div>
            </div>
          </a>
        </div>

        
        <div class="clearfix"> </div>
    </div>
  </div>
  
@endsection