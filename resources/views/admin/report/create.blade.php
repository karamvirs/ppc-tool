@extends('admin.layouts.master')
@section('content')
@include('admin.layouts.messages')
@php
    $route  = \Route::currentRouteName(); 
    if(isset($result)){
        $report = json_decode($result->data, true);
    }
    
@endphp
<div class="agile-grids">   
    <div class="grids">       
        <div class="row">
            <div class="col-md-12">
                             
<div id="report-sidebar">           
<div class="steps-list">
<h1>PPC Audit Checklist For <span id="show_account_name"> @if(isset($result)) {{ $report['account_name'] }} @endif</span></h1> 
<ul class="nav list-group">
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#introduction">
<span class="step-name content-blurred">Introduction:</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#goals">
<span class="step-name content-blurred">Goals:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#input-basic-information">
<span class="step-name content-blurred">
Input basic information </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#determine-account-goals">
<span class="step-name content-blurred">
Determine account goals </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<!-- <li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#approval-account-goals">
<span class="step-name content-blurred">
Approval: Account goals </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li> -->
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#choose-data-sources">
<span class="step-name content-blurred">
Choose data sources </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#select-the-time-frame">
<span class="step-name content-blurred">
Select the time frame </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#tracking">
<span class="step-name content-blurred">Tracking:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-adwords-tracking-code-is-added-to-your-site">
<span class="step-name content-blurred">
Verify AdWords tracking code is added to your site </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#ensure-conversion-actions-have-been-defined-appropriately">
<span class="step-name content-blurred">
Ensure conversion actions have been defined appropriately </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#campaign-settings"><span class="step-name content-blurred">Campaign settings:</span></a></li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-the-delivery-method-selected">
<span class="step-name content-blurred">
Verify the ‘Delivery Method’ selected </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-the-ad-rotation-option-selected">
<span class="step-name content-blurred">
Verify the ‘Ad Rotation’ option selected </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-your-ad-schedule-settings">
<span class="step-name content-blurred">
Verify your Ad Schedule settings </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-if-all-features-has-been-selected-for-your-campaign">
<span class="step-name content-blurred">
Check if ‘All Features’ has been selected for your campaign </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-if-you-want-to-opt-for-search-partners-from-search-network">
<span class="step-name content-blurred">
Verify if you want to opt for ‘Search partners’ from Search Network </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-the-number-of-ad-variations-per-ad-group">
<span class="step-name content-blurred">
Check the number of ad variations per ad group </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#determine-if-dynamic-keyword-insertion-dki-should-be-used">
<span class="step-name content-blurred">
Determine if dynamic keyword insertion (DKI) should be used </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#budget--bidding">
<span class="step-name content-blurred">Budget &amp; bidding:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-that-budget-is-sufficient">
<span class="step-name content-blurred">
Check that budget is sufficient </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#validate-your-bidding-strategy">
<span class="step-name content-blurred">
Validate your bidding strategy </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-bid-adjustments">
<span class="step-name content-blurred">
Verify bid adjustments </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#targeting">
<span class="step-name content-blurred">Targeting:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-location-targeting">
<span class="step-name content-blurred">
Check location targeting </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#set-proper-device-performance-and-targeting">
<span class="step-name content-blurred">
Set proper device performance and targeting </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#ensure-proper-demographic-specifications-if-any">
<span class="step-name content-blurred">
Ensure proper demographic specifications, if any </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-the-display-ad-placements-for-inappropriate-websites">
<span class="step-name content-blurred">
Check the display ad placements for inappropriate websites </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#ads">
<span class="step-name content-blurred">Ads:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-ad-guidelines-are-followed">
<span class="step-name content-blurred">
Verify ad guidelines are followed </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#use-keyword-in-ad-copy">
<span class="step-name content-blurred">
Use keyword in ad copy </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#ensure-the-landing-page-is-appropriate">
<span class="step-name content-blurred">
Ensure the landing page is appropriate </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-for-any-offer-or-timebound-messaging">
<span class="step-name content-blurred">
Check for any offer or time-bound messaging </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-your-ads-for-grammatical-or-spelling-mistakes">
<span class="step-name content-blurred">
Check your ads for grammatical or spelling mistakes </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#make-sure-ad-has-clear-calltoaction">
<span class="step-name content-blurred">
Make sure ad has clear call-to-action </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#prepare-ab-test-review">
<span class="step-name content-blurred">
Prepare A/B test review </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-display-campaigns-are-using-both-image-and-text-ads">
<span class="step-name content-blurred">
Verify display campaigns are using both image and text ads </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#keywords">
<span class="step-name content-blurred">Keywords:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-your-search-terms-for-negative-keywords">
<span class="step-name content-blurred">
Check your search terms for negative keywords </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-keyword-match-types">
<span class="step-name content-blurred">
Check keyword match types </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#ensure-there-are-no-keyword-conflicts">
<span class="step-name content-blurred">
Ensure there are no keyword conflicts </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#identify-new-search-query-opportunities">
<span class="step-name content-blurred">
Identify new search query opportunities </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right">
</i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-proper-search-query-triggered-keyword-results">
<span class="step-name content-blurred">
Verify proper search query triggered keyword results </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-the-number-of-keywords-per-ad-group">
<span class="step-name content-blurred">
Check the number of keywords per ad group </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#remove-keywords-with-zero-impressions">
<span class="step-name content-blurred">
Remove keywords with zero impressions </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#quality-score">
<span class="step-name content-blurred">Quality score:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-your-keywords-quality-score-performance">
<span class="step-name content-blurred">
Check your keyword's quality score performance  </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#eliminate-keywords-with-poor-quality-score">
<span class="step-name content-blurred">
Eliminate keywords with poor quality score </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#ad-extension">
<span class="step-name content-blurred">Ad extension:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#ensure-gmb-is-linked-to-your-ads-account-to-enable-location-extensions-if-applicable">
<span class="step-name content-blurred">
Ensure GMB is linked to your Ads account to enable location extensions (if applicable) </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
 <a class="list-group-wrap" href="#check-for-campaigns-with-missing-ad-extensions">
<span class="step-name content-blurred">
Check for campaigns with missing ad extensions </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#remove-any-nonperforming-automated-extensions">
<span class="step-name content-blurred">
Remove any non-performing automated extensions </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-the-status-of-ad-extension-approved-or-disapproved">
<span class="step-name content-blurred">
Check the status of ad extension (approved or disapproved) </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#ensure-sitelinks-have-relevant-descriptions">
<span class="step-name content-blurred">
Ensure sitelinks have relevant descriptions </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#make-sure-your-call-extension-is-scheduled-during-operational-hours">
<span class="step-name content-blurred">
Make sure your call extension is scheduled during operational hours </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#check-if-call-reporting-is-enabled-to-track-conversions-from-your-call-extensions">
<span class="step-name content-blurred">
 Check if ‘call reporting’ is enabled to track conversions from your call extensions </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#remarketing-campaigns">
<span class="step-name content-blurred">Remarketing campaigns:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#validate-remarketing-code-has-been-added-correctly">
<span class="step-name content-blurred">
Validate remarketing code has been added correctly </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#validate-remarketing-lists-are-properly-collecting-visitors">
<span class="step-name content-blurred">
Validate remarketing lists are properly collecting visitors </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
 <li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#verify-the-remarketing-lists-created">
<span class="step-name content-blurred">
Verify the remarketing lists created </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#metric-review">
<span class="step-name content-blurred">Metric review:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#review-primary-kpis">
<span class="step-name content-blurred">
Review primary KPI's </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#validate-budget-utilization">
<span class="step-name content-blurred">
Validate budget utilization </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#reportaction-items">
<span class="step-name content-blurred">Report/Action items:</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#prioritize-action-items">
<span class="step-name content-blurred">
Prioritize action items </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#create-a-report-if-needed">
<span class="step-name content-blurred">
Create a report (if needed) </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#prepare-report-for-review">
<span class="step-name content-blurred">
Prepare report for review </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li>
<!-- <li class="list-group-item step" tabindex="-1">
<a class="list-group-wrap" href="#approval-audit-report">
<span class="step-name content-blurred">
Approval: Audit report </span>
<span class="step-show-hide">
<span class="btn btn-showhide">
<i class="fa fa-chevron-right"></i>
</span>
</span>
</a>
</li> -->
<li class="list-group-item step heading" tabindex="-1">
<a class="list-group-wrap" href="#sources"><span class="step-name content-blurred">Sources:</span></a></li>
<li class="list-group-item step heading" tabindex="-1"><a class="list-group-wrap" href="#related-checklists"><span class="step-name content-blurred">Related checklists:</span></a></li>
</ul>
</div>   

                </div>              
                <div id="form-side"> 
                <div class="master_side"> 	
                <div class="panel panel-widget forms-panel">
                    <div class="forms">
                        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
                            <div class="form-body">
                                @if($route == 'report.create')
                                    {!! Form::open(array('method' => 'POST', 'route' => array('report.store'), 'id' => 'ajaxSave', 'class' => '', 'files'=>'true')) !!}
                                @elseif($route == 'report.edit')
                                    {!! Form::model($result, array('route' => array('report.update', $result->id), 'method' => 'PATCH', 'id' => 'report-form', 'class' => '', 'files'=>'true')) !!}
                                @else
                                    Nothing
                                @endif
     
                                 
							<div id="introduction">
							<h2>Introduction:</h2>
							<div class="image-content">
							<figure>
							<a href="https://ps-attachments.s3.amazonaws.com/21873709-0f74-41dc-9a07-06e258d2927e/oQ_98oanuVDSOhAjhSpJtA.png" alt="Introduction:" target="_blank"> <img src="{{ url('/') }}/images/PPC-audit-Checklist.jpg"> </a>
							</figure>
							</div>
							<div class="text-content">
							<p>A comprehensive <strong>PPC audit checklist</strong> takes countless hours of testing and iterating to properly create.</p>
							<p>Most existing examples are too specific to adapt for yourself, and all of that effort could be in vain if you haven't accounted for how complicated SERPs are. That's why we've created this PPC Audit Checklist. This is an <strong>in-depth&nbsp;framework</strong> that covers the core focus areas that should be reviewed for each new campaign or after 3 months of data has been collected.&nbsp;</p>
							<p>This checklist will walk through setting up goals, ensuring campaign settings are optimized, allocating budget, reviewing keywords, choosing proper ad extensions, and much more. Before moving into this audit keep the following quote in mind.</p>
							<p class="style-blockquote no_bottom_space"><span>Nobody counts the number of ads you run; they just remember the impression you make. -&nbsp;<a href="https://blog.hubspot.com/agency/bill-bernbach-quotes" rel="nofollow" target="_blank">Bill Bernbach</a></span></p>
							</div>
							</div>

                            <div id="goals">
							<h2>Goals:</h2>
							</div>  

							<div id="input-basic-information">
                            <h2>Input basic information</h2>                      
							<div class="text-content" style="margin-bottom: 40px;">
							<p>The first step is to record basic information about the account being audited. These details will be useful for automating basic tasks later on in the process.</p>
							<p class="style-info"><span>If you haven't already, you can&nbsp;<strong>set up&nbsp;</strong><strong><a href="https://www.process.st/business-process-automation-guide/" rel="nofollow" target="_blank">Zapier</a></strong><strong>&nbsp;to launch instances of this checklist template&nbsp;</strong>in the future&nbsp;and push information you already have stored in other tools into any task's&nbsp;form fields automatically, based on a trigger like changing a lead's status from "qualified" to "customer".</span></p>
							<p><strong>Fill out the form fields</strong> below.</p>
							</div>
                             <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                            <label>Date audit started</label>
                                            <input type="date" @if(isset($result)) value="{{ $report['date_audit_started'] }}" @endif name="date_audit_started" class="validate[required]">
                                            @if($errors->has('date_audit_started'))
                                             <span class="text-danger"><br>{{$errors->first('date_audit_started')}}</span>
                                            @endif
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Account Name</label>
                                            <input type="text" @if(isset($result)) value="{{ $report['account_name'] }}" @endif name="account_name" id="account_name" class="validate[required]">
                                            @if($errors->has('account_name'))
                                             <span class="text-danger"><br>{{$errors->first('account_name')}}</span>
                                            @endif
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Primary contact first name</label>
                                            <input type="text" @if(isset($result)) value="{{ $report['primary_contact_first_name'] }}" @endif name="primary_contact_first_name" class="validate[required]">
                                            @if($errors->has('primary_contact_first_name'))
                                             <span class="text-danger"><br>{{$errors->first('primary_contact_first_name')}}</span>
                                            @endif
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Primary contact last name</label>
                                            <input type="text" @if(isset($result)) value="{{ $report['primary_contact_last_name'] }}" @endif name="primary_contact_last_name" class="validate[required]">
                                            @if($errors->has('primary_contact_last_name'))
                                             <span class="text-danger"><br>{{$errors->first('primary_contact_last_name')}}</span>
                                            @endif
                                        </div> 
                                    </div>
                            </div>
                            </div>

                            <div id="determine-account-goals">
							<h2>Determine account goals</h2>
							<div class="text-content">
							<p>Whether you are putting this together for a client or are part of an internal&nbsp;team, you will want to think about what the goals are for the account.</p>
							<ul>
							<li>Was the account setup based off of old KPI's?</li>
							<li>What are expected conversion rates?</li>
							<li>Has the target market changed?</li>
							</ul>
							<p><strong>Fill out the form fields</strong> below.</p>
							</div> 
                                <div class="row">
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Account Goals</label>
                                            <textarea name="account_goals" class="validate[required]"> @if(isset($result)) {{ $report['account_goals'] }} @endif</textarea>
                                            @if($errors->has('account_goals'))
                                             <span class="text-danger"><br>{{$errors->first('account_goals')}}</span>
                                            @endif
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Account KPI's</label>
                                            <textarea name="account_kpi" class="validate[required]">@if(isset($result)) {{ $report['account_kpi'] }} @endif</textarea>
                                            @if($errors->has('account_kpi'))
                                             <span class="text-danger"><br>{{$errors->first('account_kpi')}}</span>
                                            @endif
                                        </div> 
                                    </div>
                                </div>
                            <div class="text-content" style="margin-top: 15px;">
								<p>Using the Members form field below,&nbsp;<strong>assign the individual who will approve the account goals.</strong></p>
							</div>    
							</div>
                                    
						<!-- 	<div id="approval-account-goals">
							<h2>Approval: Account goals</h2>
							<div class="approval-content">
							<div class="header">
							<div class="list-title">
							Will be submitted for approval:
							</div>
							</div>
							</div>
							</div> -->

							<div id="choose-data-sources">
							<h2>Choose data sources</h2>
							<div class="text-content">
							<p><span><strong>List the data sources</strong> you will be using for this audit and how they will be accessed. When conducting an audit it is important to make sure your data is as accurate as possible. </span></p>
							<p><span><span style="background-color: #e5f5fb;"><strong>PRO TIP:</strong>&nbsp;Downloading the data from Google, Bing, etc... and incorporating it into an Excel spreadsheet can make it easier to analyze and visualize the data. This also allows you to show the data&nbsp;to clients and your manager to support your recommendations.&nbsp;</span></span></p>
							<p><strong>Fill in the below form field </strong>with the data sources you will be using. You should also upload any data you are analyzing to keep track of it.&nbsp;</p>
							</div>
                            <div class="row">
                                <div class="col-md-10" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Data sources</label>
                                        <textarea name="data_sources" class="validate[required]">@if(isset($result)) {{ $report['data_sources'] }} @endif</textarea>
                                        @if($errors->has('data_sources'))
                                            <span class="text-danger"><br>{{$errors->first('data_sources')}}</span>
                                        @endif
                                    </div> 
                                </div>
                                <div class="col-md-10" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Audit data</label>
                                        @if(isset($result)) 
                                        <input type="file" name="audit_data">
                                        <a href="{{ url('/') }}{{  $report['audit_data'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Audit Data</a>
                                        @else 
                                        <input type="file" name="audit_data" class="validate[required]">
                                        @endif

                                        @if($errors->has('audit_data'))
                                            <span class="text-danger"><br>{{$errors->first('audit_data')}}</span>
                                        @endif
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="select-the-time-frame">
							<h2>Select the time frame</h2>
							<div class="text-content">
							<p><strong>Select the time frame of this audit.</strong></p>
							<p>The time frame being analyzed will greatly impact the insights that you gain. We typically recommend that you have at least 3 months of data. <span style="background-color: #e5f5fb;"></span></p>
							<p><strong>Fill out the form fields</strong> below.</p>
							</div>
							<div class="row">
                                <div class="col-md-3" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Start date</label>
                                        <input type="date" @if(isset($result)) value="{{ $report['start_date'] }}" @endif name="start_date" class="validate[required]">
                                        @if($errors->has('start_date'))
                                            <span class="text-danger"><br>{{$errors->first('start_date')}}</span>
                                        @endif
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>End date</label>
                                        <input type="date" @if(isset($result)) value="{{ $report['end_date'] }}" @endif name="end_date" class="validate[required]">
                                        @if($errors->has('end_date'))
                                            <span class="text-danger"><br>{{$errors->first('end_date')}}</span>
                                        @endif
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="tracking">
							<h2>Tracking:</h2>
							</div>

							<div id="verify-adwords-tracking-code-is-added-to-your-site">
							<h2>Verify AdWords tracking code is added to your site</h2>
							<div class="text-content">
							<p>Check that your <strong>AdWords tracking code is properly added</strong> to your site.</p>
							<p>To verify the conversion tag you have added on the page is correct you can use Google Tag Assistant. To do this just follow these steps:</p>
							<ul>
							<li>Install Google Tag Assistant</li>
							<li>Open your thank you page in a new tab</li>
							<li>Click on the tag assistant icon</li>
							<li>Enable it and reload your page</li>
							</ul>
							<p>You will then see the list of tags that are being fired on your page. This will include your conversion tag.</p>
							</div>
                            <div class="row">
                                <div class="col-md-10" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Google Tag Assistant</label>
                                        @if(isset($result)) 
                                        <input type="file" name="google_tag_assistant" accept="image/*" onchange="loadFile(event)">
                                        <img src="{!! asset($report['google_tag_assistant']) !!}" id="output" alt="Google Tag Assistant"> 
                                        @else 
                                        <input type="file" name="google_tag_assistant" accept="image/*" onchange="loadFile(event)" class="validate[required]">
                                        <img id="output"/>
                                        @endif
                                        @if($errors->has('google_tag_assistant'))
                                            <span class="text-danger"><br>{{$errors->first('google_tag_assistant')}}</span>
                                        @endif
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="ensure-conversion-actions-have-been-defined-appropriately">
							<h2>Ensure conversion actions have been defined appropriately</h2>
							<div class="text-content">
							<p><strong>Review current conversion actions</strong> and ensure they are relevant.</p>
							<p>A conversion action is a specific action that you have defined as being valuable to your business. Examples of this could be an online purchase or a phone call. You can setup conversion actions for different sources. Some of these sources could include website actions, an in-app action, an app download, and calls.&nbsp;</p>
							<p>If you need to track multiple instances of the same kind of conversion, such as a form submission, you will need to create multiple conversion actions.&nbsp;</p>
							</div>
							<div class="row">
                                <div class="col-md-10" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Conversion actions</label>
                                        @if(isset($result)) 
                                        <input type="file" name="conversion_actions" accept="image/*" onchange="loadFile1(event)">
                                        <img src="{!! asset($report['conversion_actions']) !!}" id="output1" alt="Google Tag Assistant"> 
                                        @else 
                                        <input type="file" name="conversion_actions" accept="image/*" onchange="loadFile1(event)" class="validate[required]">
                                        <img id="output1"/>
                                        @endif

                                        @if($errors->has('conversion_actions'))
                                            <span class="text-danger"><br>{{$errors->first('conversion_actions')}}</span>
                                        @endif
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="campaign-settings">
							<h2>Campaign settings:</h2>
							</div>


<div id="verify-the-delivery-method-selected">
<h2>Verify the ‘Delivery Method’ selected</h2>
<div class="text-content">
<p><strong>Verify the delivery method being used.</strong></p>
<p>The delivery method that you choose will determine how long your budget will last. The default choice for most accounts is "standard" delivery. This is because Google will optimize the delivery of your ads and spread them out throughout the day. You may miss out on some searches but it ensure that your budget will last.</p>
<p>If you choose "accelerated" your budget will most likely get fully utilized. Google doesn't optimize the delivery as much as they do with "standard". Additionally, the funds are usually heavily used for morning ad placements. This can cause your ads not&nbsp;to be shown in the afternoon.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Delivery method</label>
<input type="text" @if(isset($result)) value="{{ $report['delivery_method'] }}" @endif name="delivery_method" class="validate[required]">
@if($errors->has('delivery_method'))
<span class="text-danger"><br>{{$errors->first('delivery_method')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="google_ads" accept="image/*" onchange="loadFile2(event)">
<img src="{!! asset($report['google_ads']) !!}" id="output2" alt="Google Tag Assistant"> 
@else 
<input type="file" name="google_ads" accept="image/*" onchange="loadFile2(event)" class="validate[required]">
<img id="output2"/>
@endif
@if($errors->has('google_ads'))
<span class="text-danger"><br>{{$errors->first('google_ads')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="verify-the-ad-rotation-option-selected">
<h2>Verify the ‘Ad Rotation’ option selected</h2>
<div class="text-content">
<p><strong>Verify that the correct option</strong> is selected for ad rotation.&nbsp;</p>
<p>Ad rotation is the method that Google Ads are delivered on both the Search and Display Networks. If you have more than one ad in your ad group then they will be forced to rotate. Google doesn't allow more than one ad from your account to show at a time.&nbsp;</p>
<p>To review your ad rotation settings you will need to ensure that "all features" are enabled. From the ad rotation section, you can choose the frequency that your ads will be served relative to each other. Please be aware that ad rotation settings are not available&nbsp;in a Smart Display campaign.&nbsp;</p>
<p>There are two primary options. You can choose "optimize" or "rotate indefinitely."</p>
<p>Below are instructions as provided by Google.</p>
<h5>Set ad rotation for your campaign</h5>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow">Google Ads</a><span>&nbsp;</span>account.</li>
<li>Click<span>&nbsp;</span><strong>All campaigns<span>&nbsp;</span></strong>in the navigation panel.</li>
<li>Click the campaign you’d like to change.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu.</li>
<li>Click<span>&nbsp;</span><strong>Additional settings</strong>.</li>
<li>Choose<span>&nbsp;</span><strong>Ad rotation</strong>.</li>
<li>Choose an ad rotation method:<span>&nbsp;</span><strong>Optimize</strong><span>&nbsp;</span>or<span>&nbsp;</span><strong>Rotate indefinitely</strong>.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
<h5>Set ad rotation for multiple campaigns at once</h5>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow">Google Ads</a><span>&nbsp;</span>account.</li>
<li>Click<span>&nbsp;</span><strong>All campaigns</strong><span>&nbsp;</span>in the navigation panel.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu.</li>
<li>Check the box next to the campaigns or ad groups you want to change.</li>
<li>Click<span>&nbsp;</span><strong>Edit</strong>.</li>
<li>In the drop-down menu, select<span>&nbsp;</span><strong>Change ad rotation</strong>.</li>
<li>Choose an ad rotation method.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
<h5>Set ad rotation by ad group</h5>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow">Google Ads</a><span>&nbsp;</span>account.</li>
<li>Click<span>&nbsp;</span><strong>Ad groups</strong><span>&nbsp;</span>in the page menu.</li>
<li>Click the ad group that you’d like to change.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu.</li>
<li>Click<span>&nbsp;</span><strong>Ad rotation</strong><span>&nbsp;</span>and then select a method.</li>
</ol>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="new_campaign" accept="image/*" onchange="loadFile3(event)">
<img src="{!! asset($report['new_campaign']) !!}" id="output3" alt="Google Tag Assistant"> 
@else 
<input type="file" name="new_campaign" accept="image/*" onchange="loadFile3(event)" class="validate[required]">
<img id="output3"/>
@endif
@if($errors->has('new_campaign'))
<span class="text-danger"><br>{{$errors->first('new_campaign')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="verify-your-ad-schedule-settings">
<h2>Verify your Ad Schedule settings</h2>
<div class="text-content">
<p><strong>Choose a relevant ad schedule.</strong></p>
<p><span>If you have specific times that you would like your ads to show then this is the section you should modify. However, if no one searches for your specific keyword during the time that you have chosen then your ad will not appear. You can also use this section to increase or decrease your spend on specific days and times.</span><span></span></p>
<p><span>By default, Google ads campaigns are set to display "all day."</span></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="schedule_settings" accept="image/*" onchange="loadFile4(event)">
<img src="{!! asset($report['schedule_settings']) !!}" id="output4" alt="Google Tag Assistant"> 
@else 
<input type="file" name="schedule_settings" accept="image/*" onchange="loadFile4(event)" class="validate[required]">
<img id="output4"/>
@endif
@if($errors->has('schedule_settings'))
<span class="text-danger"><br>{{$errors->first('schedule_settings')}}</span>
@endif
</div> 
</div>
</div>
</div>


<div id="check-if-all-features-has-been-selected-for-your-campaign">
<h2>Check if ‘All Features’ has been selected for your campaign</h2>
<div class="text-content">
<p><strong>Always make sure "All features"&nbsp;</strong>has been selected for your Search campaign. This makes sure that any advanced options will be accessible for that campaign. Not having this selected will limit features and could impact ROI.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="all_features" accept="image/*" onchange="loadFile5(event)">
<img src="{!! asset($report['all_features']) !!}" id="output5" alt="Google Tag Assistant"> 
@else 
<input type="file" name="all_features" accept="image/*" onchange="loadFile5(event)" class="validate[required]">
<img id="output5"/>
@endif
@if($errors->has('all_features'))
<span class="text-danger"><br>{{$errors->first('all_features')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="verify-if-you-want-to-opt-for-search-partners-from-search-network">
<h2>Verify if you want to opt for ‘Search partners’ from Search Network</h2>
<div class="text-content">
<p><strong>Confirm search partners being used.</strong></p>
<p>If you have a campaign that will be using the Search Network it will automatically include all search partners. This is a default option.&nbsp;</p>
<p>It is also important to note that Google Ads will not provide partner-level reports on ad performance that utilize search partner sites.&nbsp;</p>
<p>Follow these steps to review your current settings:&nbsp;</p>
<ol>
<li>Sign into your<span>&nbsp;</span><a href="http://ads.google.com/" rel="nofollow" target="_blank">Google Ads account</a>.&nbsp;</li>
<li>In the page menu, click<span>&nbsp;</span><strong>Settings</strong>, then click the campaign you want to include or remove search partners to.</li>
<li>Click<span>&nbsp;</span><strong>Networks</strong>.</li>
<li>Check the box next to "Include Google search partners" to enable ads from this campaign to appear on search partner websites, or uncheck it to disable this campaign’s ads from showing on search partner websites.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="search_partners" accept="image/*" onchange="loadFile6(event)">
<img src="{!! asset($report['search_partners']) !!}" id="output6" alt="Google Tag Assistant"> 
@else 
<input type="file" name="search_partners" accept="image/*" onchange="loadFile6(event)" class="validate[required]">
<img id="output6"/>
@endif
@if($errors->has('search_partners'))
<span class="text-danger"><br>{{$errors->first('search_partners')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="check-the-number-of-ad-variations-per-ad-group">
<h2>Check the number of ad variations per ad group</h2>
<div class="text-content">
<p><strong>Review the number of ad variations per ad group.</strong></p>
<p><span>If you are conducting a search network campaign then Google recommends creating at least </span><strong>2-3 ads</strong><span>&nbsp;per ad group.&nbsp; Another good rule of thumb is to review the number of impressions your ad group will receive and make sure it is in line with what you need to hit your conversion goals.</span></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Overview</label>
@if(isset($result)) 
<input type="file" name="ad_variations" accept="image/*" onchange="loadFile7(event)">
<img src="{!! asset($report['ad_variations']) !!}" id="output7" alt="Google Tag Assistant"> 
@else 
<input type="file" name="ad_variations" accept="image/*" onchange="loadFile7(event)" class="validate[required]">
<img id="output7"/>
@endif
@if($errors->has('ad_variations'))
<span class="text-danger"><br>{{$errors->first('ad_variations')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="determine-if-dynamic-keyword-insertion-dki-should-be-used">
<h2>Determine if dynamic keyword insertion (DKI) should be used</h2>
<div class="text-content">
<p><strong>Decide if dynamic keyword insertion (DKI) will be used.</strong></p>
<p><strong>Dynamic keyword insertion</strong> allows you to create ads that serve up more relevant and personalized ads based off of the searchers' keyword. This feature is available on Google Ads and other ad networks. For a more in-depth&nbsp;understanding of DKI, we would highly recommend <a href="https://www.wordstream.com/dynamic-keyword-insertion&nbsp;" rel="nofollow" target="_blank">this guide</a> put together by WordStream.&nbsp;&nbsp;</p>
<p></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Dynamic Search Ads</label>
@if(isset($result)) 
<input type="file" name="dynamic_search_ads" accept="image/*" onchange="loadFile8(event)">
<img src="{!! asset($report['dynamic_search_ads']) !!}" id="output8" alt="Google Tag Assistant"> 
@else 
<input type="file" name="dynamic_search_ads" accept="image/*" onchange="loadFile8(event)" class="validate[required]">
<img id="output8"/>
@endif
@if($errors->has('dynamic_search_ads'))
<span class="text-danger"><br>{{$errors->first('dynamic_search_ads')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Will DKI be used?</label>
<select name="will_dki" class="validate[required]"> 
	<option>Select</option> 
	@if(isset($result))
    <option value="Test 1" @if($report['will_dki'] == "Test 1") selected @endif>Test 1</option> 
	<option value="Test 2" @if($report['will_dki'] == "Test 2") selected @endif>Test 2</option>  
	<option value="Test 3" @if($report['will_dki'] == "Test 3") selected @endif>Test 3</option> 
	<option value="Test 4" @if($report['will_dki'] == "Test 4") selected @endif>Test 4</option> 
	@else
	<option value="Test 1">Test 1</option> 
	<option value="Test 2">Test 2</option>  
	<option value="Test 3">Test 3</option> 
	<option value="Test 4">Test 4</option> 
	@endif
</select>
@if($errors->has('will_dki'))
<span class="text-danger"><br>{{$errors->first('will_dki')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="budget--bidding">
<h2>Budget &amp; bidding:</h2>
</div>

<div id="check-that-budget-is-sufficient">
<h2>Check that budget is sufficient</h2>
<div class="text-content">
<p><strong>Check the current&nbsp;</strong><strong>budget and make sure it is appropriate.</strong></p>
<p><span>Within Google Ads you can set a daily budget for each campaign. Your budget should be based on your traffic or conversion goals. Make sure that you review this to ensure you have sufficient budget to hit your metrics.&nbsp;</span></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="current_budget" accept="image/*" onchange="loadFile9(event)">
<img src="{!! asset($report['current_budget']) !!}" id="output9" alt="Google Tag Assistant"> 
@else 
<input type="file" name="current_budget" accept="image/*" onchange="loadFile9(event)" class="validate[required]">
<img id="output9"/>
@endif
@if($errors->has('current_budget'))
<span class="text-danger"><br>{{$errors->first('current_budget')}}</span>
@endif
</div> 
</div>
</div>
</div>


<div id="validate-your-bidding-strategy">
<h2>Validate your bidding strategy</h2>
<div class="text-content">
<p><strong>Validate the bidding strategy.</strong></p>
<p><span>It is important to have a solid bid strategy behind your effort. You can choose a Manual CPC strategy or utilize one of the automated strategies that Google Ads has created. Make sure that you have taken the time to understand all the bid strategies available. That way you can determine which strategy is best for hitting your&nbsp;KPI's.&nbsp;</span></p>
<p><span>If goals do change you can always update your strategy by following these steps:</span></p>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow" target="_blank">Google Ads account</a>.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Campaigns</strong>.</li>
<li>Select the campaign you want to edit.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu for this campaign.</li>
<li>Open<span>&nbsp;</span><strong>Bidding</strong><span>&nbsp;</span>and then click<span>&nbsp;</span><strong>Change bid strategy</strong>.</li>
<li>Select your new bid strategy from the drop-down menu.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
<p>Google also put together a great article on how to determine your bid strategy. It can be seen by <a href="https://support.google.com/google-ads/answer/2472725" rel="nofollow" target="_blank">clicking here</a>.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Bidding strategy</label>
<textarea rows="3" name="bidding_strategy" class="form-control" class="validate[required]">@if(isset($result)){{ $report['bidding_strategy'] }} @endif</textarea>
@if($errors->has('bidding_strategy'))
<span class="text-danger"><br>{{$errors->first('bidding_strategy')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Google Ads</label>
@if(isset($result)) 
<input type="file" name="bidding_strategy_image" accept="image/*" onchange="loadFile10(event)">
<img src="{!! asset($report['bidding_strategy_image']) !!}" id="output10" alt="Google Tag Assistant"> 
@else 
<input type="file" name="bidding_strategy_image" accept="image/*" onchange="loadFile10(event)" class="validate[required]">
<img id="output10"/>
@endif
@if($errors->has('bidding_strategy_image'))
<span class="text-danger"><br>{{$errors->first('bidding_strategy_image')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content">
<p>If you would like to learn more or need a refresher on bid strategies we have included a very informative video below.&nbsp;</p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/_i7qWiDs4AA?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
</div>

<div id="verify-bid-adjustments">
<h2>Verify bid adjustments</h2>
<div class="text-content">
<p><strong>Review your current bids per interaction</strong>. Then determine if adjustments need to be made.</p>
<p>Sometimes the source or type of interaction from of a click can be worth more than others. Bid adjustments allow you to specify if you would like to increase spend to get more exposure based on certain types of interactions. Examples of the choices you have include time of day, location, device type, etc...</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Bid Adjustments</label>
@if(isset($result)) 
<input type="file" name="bid_adjustments" accept="image/*" onchange="loadFile11(event)">
<img src="{!! asset($report['bid_adjustments']) !!}" id="output11" alt="Google Tag Assistant"> 
@else 
<input type="file" name="bid_adjustments" accept="image/*" onchange="loadFile11(event)" class="validate[required]">
<img id="output11"/>
@endif
@if($errors->has('bid_adjustments'))
<span class="text-danger"><br>{{$errors->first('bid_adjustments')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="targeting">
<h2>Targeting:</h2>
</div>

<div id="check-location-targeting">
<h2>Check location targeting</h2>
<div class="text-content">
<p><strong>Make sure the locations chosen are correct.&nbsp;&nbsp;</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Locations</label>
<textarea  rows="3" name="locations" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['locations'] }} @endif</textarea>
@if($errors->has('locations'))
<span class="text-danger"><br>{{$errors->first('locations')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p>You want to make sure that you are targeting customers that you feel will convert the best. If your ideal customer is only in English speaking countries then it wouldn't make any sense to have your ads running in non-English speaking countries. This would be wasted spend and artificially inflate your customer acquisition costs.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Location Targeting</label>
@if(isset($result)) 
<input type="file" name="location_targeting" accept="image/*" onchange="loadFile12(event)">
<img src="{!! asset($report['location_targeting']) !!}" id="output12" alt="Google Tag Assistant"> 
@else 
<input type="file" name="location_targeting" accept="image/*" onchange="loadFile12(event)" class="validate[required]">
<img id="output12"/>
@endif
@if($errors->has('location_targeting'))
<span class="text-danger"><br>{{$errors->first('location_targeting')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="set-proper-device-performance-and-targeting">
<h2>Set proper device performance and targeting</h2>
<div class="text-content">
<p><strong>Make sure that targeting is properly selected.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Targeting selected</label>
<input type="text" @if(isset($result)) value="{{ $report['targeting_selected'] }}" @endif name="targeting_selected" class="validate[required]">
@if($errors->has('targeting_selected'))
<span class="text-danger"><br>{{$errors->first('targeting_selected')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p><span>For both display and video campaigns you have the ability to target specific operating systems, ad inventory, device types, wireless networks, and more. Make sure you have fine tuned these to show up to the right people at the right time on the right device.&nbsp;</span></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Targeting</label>
@if(isset($result)) 
<input type="file" name="targeting_img" accept="image/*" onchange="loadFile13(event)">
<img src="{!! asset($report['targeting_img']) !!}" id="output13" alt="Google Tag Assistant"> 
@else 
<input type="file" name="targeting_img" accept="image/*" onchange="loadFile13(event)" class="validate[required]">
<img id="output13"/>
@endif
@if($errors->has('targeting_img'))
<span class="text-danger"><br>{{$errors->first('targeting_img')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="ensure-proper-demographic-specifications-if-any">
<h2>Ensure proper demographic specifications, if any</h2>
<div class="text-content">
<p><strong>Select demographic information for your target audience.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Demographic details</label>
<textarea  rows="3" name="demographic_details" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['demographic_details'] }} @endif</textarea>
@if($errors->has('demographic_details'))
<span class="text-danger"><br>{{$errors->first('demographic_details')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p>If you have a specific demographic that you are targeting then you will want to make sure you have that setup properly. Follow the below steps to create demographic groups:</p>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow" target="_blank">Google Ads account</a>.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Campaigns</strong>.</li>
<li>Click the name of the campaign to which you want to add demographic targeting. You can add demographic categories to Search, Display, and Video campaigns.</li>
<li>Click the name of the ad group to which you want to add demographic targeting.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Demographics</strong>.</li>
<li>Click the demographic you want to target along the top of the page:
<ul>
<li><strong>Gender</strong></li>
<li><strong>Age</strong></li>
<li><strong>Parental status</strong><span>&nbsp;</span>(Display and Video campaigns only)</li>
<li><strong>Household income</strong><span>&nbsp;</span>(currently available in the U.S., Japan, Australia, and New Zealand only)</li>
</ul> </li>
<li>Check the box next to the specific demographic groups you want to target.</li>
<li>Click the<span>&nbsp;</span><strong>Edit</strong><span>&nbsp;</span>drop-down in the blue banner that appears along the top of the table, then select<span>&nbsp;</span><strong>Enable</strong>. To exclude demographic groups, select<span>&nbsp;</span><strong>Exclude from ad group</strong>.</li>
</ol>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Demographic Specifications</label>
@if(isset($result)) 
<input type="file" name="semographic_specifications" accept="image/*" onchange="loadFile14(event)">
<img src="{!! asset($report['semographic_specifications']) !!}" id="output14" alt="Google Tag Assistant"> 
@else 
<input type="file" name="semographic_specifications" accept="image/*" onchange="loadFile14(event)" class="validate[required]">
<img id="output14"/>
@endif
@if($errors->has('semographic_specifications'))
<span class="text-danger"><br>{{$errors->first('semographic_specifications')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="check-the-display-ad-placements-for-inappropriate-websites">
<h2>Check the display ad placements for inappropriate websites</h2>
<div class="text-content">
<p><strong>Make sure inappropriate websites are on the exclusion list.&nbsp;&nbsp;</strong></p>
<p>If you have a list of websites or know of types of sites that you don't want your ad showing up on then you should include this in the negative site list. This is important from a branding standpoint. If your ad appears on a site then the common online user will associate your ad with the site they are viewing. Below are the step-by-step instructions provided by Google:</p>
<p></p>
<h5>Exclude individual sites from ad groups or campaigns</h5>
<ol>
<li>Select<span>&nbsp;</span><strong>Keywords and Targeting</strong><span>&nbsp;</span>&gt;<span>&nbsp;</span><strong>Placements, Negative</strong><span>&nbsp;</span>in the type list.</li>
<li>Click<span>&nbsp;</span><strong>Add negative</strong><span>&nbsp;</span>and select the type of negative site (campaign or ad group).</li>
<li>If prompted, select the destination for the new negative site and click<span>&nbsp;</span><strong>OK</strong>.</li>
<li>Enter the negative site in the edit panel.</li>
</ol>
<h5>Add multiple negative sites</h5>
<ol>
<li>Select<span>&nbsp;</span><strong>Keywords and Targeting</strong><span>&nbsp;</span>&gt;<span>&nbsp;</span><strong>Placements, Negative</strong><span>&nbsp;</span>in the type list.</li>
<li>Click<span>&nbsp;</span><strong>Make multiple changes</strong>.</li>
<li>Under "Destination,” select<span>&nbsp;</span><strong>My data includes columns for campaigns and/or ad groups</strong><span>&nbsp;</span>or<span>&nbsp;</span><strong>Use selected destinations</strong>.</li>
<li>Type or paste your changes into the grid.</li>
<li>Click<span>&nbsp;</span><strong>Process</strong>.</li>
<li>To continue, click<span>&nbsp;</span><strong>Finish and review changes</strong>.</li>
<li>To add the pending changes to your account, click<span>&nbsp;</span><strong>Keep</strong>.</li>
</ol>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Exclusion list</label>
<textarea name="exclusion_list" rows="3" class="validate[required]">@if(isset($result)) {{ $report['exclusion_list'] }} @endif</textarea>
@if($errors->has('exclusion_list'))
<span class="text-danger"><br>{{$errors->first('exclusion_list')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="ads">
<h2>Ads:</h2>
</div>

<div id="verify-ad-guidelines-are-followed">
<h2>Verify ad guidelines are followed</h2>
<div class="text-content">
<p><strong>Verify ad guidelines are being followed.</strong></p>
<p>Each company should have branding guidelines and style guides created that should be followed. Ensure that all ads are compliant with internal policies.</p>
<p>Additionally, Google has guidelines and an approval process that has to be adhered&nbsp;to. Please be sure to review<a href="https://support.google.com/adspolicy/answer/6008942" rel="nofollow" target="_blank">&nbsp;Google Ads policies</a>. &nbsp;</p>
<p>Typically any ad created will go through a standard review process to ensure quality and compliance. Most ads are approved within 1 business day. If it hasn't been reviewed or approved by the end of 2 business days then you need to contact them.&nbsp;</p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="380" src="https://www.youtube.com/embed/Dx0ukoJPO4Y?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
</div>

<div id="use-keyword-in-ad-copy">
<h2>Use keyword in ad copy</h2>
<div class="text-content">
<p><strong>Use keywords in the ad copy.</strong></p>
<p><span>Ads are driven by keywords. If your keyword is not in your ad then your ad will not perform or show up very much. Your quality score will also be very low due to irrelevance. This is because Google wants to show ads that are in line with user search intent.&nbsp;</span></p>
<p><span>One good strategy is to use Single Keyword Ad Groups. This is also lovingly called "SKAGs." The strategy is exactly what the name implies. Your ad should have just one keyword associated with each ad group. This allows you to ensure that your keywords are directly in line with the search terms you're trying to convert on.&nbsp;</span></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Search result</label>
@if(isset($result)) 
<input type="file" name="search_result" accept="image/*" onchange="loadFile15(event)">
<img src="{!! asset($report['search_result']) !!}" id="output15" alt="Google Tag Assistant"> 
@else 
<input type="file" name="search_result" accept="image/*" onchange="loadFile15(event)" class="validate[required]">
<img id="output15"/>
@endif
@if($errors->has('search_result'))
<span class="text-danger"><br>{{$errors->first('search_result')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keyword list</label>
@if(isset($result)) 
<input type="file" name="keyword_list">
<a href="{{ url('/') }}{{  $report['keyword_list'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Keyword list</a>
@else 
<input type="file" name="keyword_list" class="validate[required]">
@endif
@if($errors->has('keyword_list'))
<span class="text-danger"><br>{{$errors->first('keyword_list')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="ensure-the-landing-page-is-appropriate">
<h2>Ensure the landing page is appropriate</h2>
<div class="text-content">
<p><strong>Make sure your landing pages are following best practices.&nbsp;&nbsp;</strong></p>
<p>Unbounce put together a great best practices article that can be seen <a href="https://unbounce.com/landing-pages/checklist/" rel="nofollow" target="_blank">here</a>. Below are eight core items that should be checked for each landing page.</p>
</div>
<div class="multi-select-content form-field-content">
<ul class="items">
<li class="item">
<div class="item-name-static">
Does your landing page headline match the message on your ads?
</div> </li>
<li class="item">
<div class="item-name-static">
Does your page message have the clarity of a 30-second elevator pitch?
</div> </li>
<li class="item">
<div class="item-name-static">
Does the writing focus primarily on benefits rather than features?
</div> </li>
<li class="item">
<div class="item-name-static">
Are you using a relevant and original main image or video that shows it being used?
</div> </li>
<li class="item">
<div class="item-name-static">
Do you make it clear what the visitor will receive by clicking your CTA?
</div> </li>
<li class="item">
<div class="item-name-static">
Have you optimized your landing page to get a paid search quality score above 7?
</div> </li>
<li class="item">
<div class="item-name-static">
ave you removed extraneous links (like the global nav) – to remove page leaks?
</div> </li>
<li class="item">
<div class="item-name-static">
Do you have clear goals for this landing page and campaign?
</div> </li>
</ul>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Homepage vs. Landing Page</label>
@if(isset($result)) 
<input type="file" name="home_vs_landing" accept="image/*" onchange="loadFile16(event)">
<img src="{!! asset($report['home_vs_landing']) !!}" id="output16" alt="Google Tag Assistant"> 
@else 
<input type="file" name="home_vs_landing" accept="image/*" onchange="loadFile16(event)" class="validate[required]">
<img id="output16"/>
@endif
@if($errors->has('home_vs_landing'))
<span class="text-danger"><br>{{$errors->first('home_vs_landing')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="check-for-any-offer-or-timebound-messaging">
<h2>Check for any offer or time-bound messaging</h2>
<div class="text-content">
<p><strong>Make sure time-bound offers have the correct runtimes.</strong>&nbsp;</p>
<p>If your organization runs special campaigns for discounts, holidays, or special events it is important to keep track of the dates these offers are valid. In the form field below list, any offers that have run over the audit analysis period.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Time bound offer details</label>
<textarea  rows="3" name="time_bound_offer_details" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['time_bound_offer_details'] }} @endif</textarea>
@if($errors->has('time_bound_offer_details'))
<span class="text-danger"><br>{{$errors->first('time_bound_offer_details')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>New Offer</label>
@if(isset($result)) 
<input type="file" name="new_offer" accept="image/*" onchange="loadFile17(event)">
<img src="{!! asset($report['new_offer']) !!}" id="output17" alt="Google Tag Assistant"> 
@else 
<input type="file" name="new_offer" accept="image/*" onchange="loadFile17(event)" class="validate[required]">
<img id="output17"/>
@endif
@if($errors->has('new_offer'))
<span class="text-danger"><br>{{$errors->first('new_offer')}}</span>
@endif
</div> 
</div>
</div>
</div>


<div id="check-your-ads-for-grammatical-or-spelling-mistakes">
<h2>Check your ads for grammatical or spelling mistakes</h2>
<div class="text-content">
<p><strong>Review ads for spelling or grammar mistakes. </strong></p>
<p>Be sure to triple&nbsp;check for any grammatical errors. A great tool to use is <a href="https://www.process.st/how-to-write-a-proposal/" rel="nofollow" target="_blank">Grammarly</a>. You might even want to ask a colleague to take a look. It never hurts to have a fresh pair of eyes.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Grammatical or Spelling</label>
@if(isset($result)) 
<input type="file" name="grammatical_or_spelling" accept="image/*" onchange="loadFile18(event)">
<img src="{!! asset($report['grammatical_or_spelling']) !!}" id="output18" alt="Google Tag Assistant"> 
@else 
<input type="file" name="grammatical_or_spelling" accept="image/*" onchange="loadFile18(event)" class="validate[required]">
<img id="output18"/>
@endif
@if($errors->has('grammatical_or_spelling'))
<span class="text-danger"><br>{{$errors->first('grammatical_or_spelling')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="make-sure-ad-has-clear-calltoaction">
<h2>Make sure ad has clear call-to-action</h2>
<div class="text-content">
<p><strong>Ensure each&nbsp;ad</strong><strong>&nbsp;has a clear call-to-action.</strong></p>
<p>The newspaper industry has a great saying, "<strong>never bury the lead</strong>." This is extremely relevant when writing your ad. Make sure you clearly state why people should care to read your offer. Then you need to encourage them to take an action.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Call to Action</label>
@if(isset($result)) 
<input type="file" name="call_to_action" accept="image/*" onchange="loadFile19(event)">
<img src="{!! asset($report['call_to_action']) !!}" id="output19" alt="Google Tag Assistant"> 
@else 
<input type="file" name="call_to_action" accept="image/*" onchange="loadFile19(event)" class="validate[required]">
<img id="output19"/>
@endif
@if($errors->has('call_to_action'))
<span class="text-danger"><br>{{$errors->first('call_to_action')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="prepare-ab-test-review">
<h2>Prepare A/B test review</h2>
<div class="text-content">
<p><strong>Review current A/B tests</strong> that are running in the account. Are there any insights or issues?</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>A/B test insights</label>
<textarea  rows="3" name="a_b_test_insights" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['a_b_test_insights'] }} @endif</textarea>
@if($errors->has('a_b_test_insights'))
<span class="text-danger"><br>{{$errors->first('a_b_test_insights')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p>If the account isn't running any A/B tests then that is something you should plan on doing. There is always something that can be improved. Be sure that you setup a control and a variant so that you can accurately determine what is working and what isn't.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Test Insights</label>
@if(isset($result)) 
<input type="file" name="test_insights" accept="image/*" onchange="loadFile20(event)">
<img src="{!! asset($report['test_insights']) !!}" id="output20" alt="Google Tag Assistant"> 
@else 
<input type="file" name="test_insights" accept="image/*" onchange="loadFile20(event)" class="validate[required]">
<img id="output20"/>
@endif
@if($errors->has('test_insights'))
<span class="text-danger"><br>{{$errors->first('test_insights')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="verify-display-campaigns-are-using-both-image-and-text-ads">
<h2>Verify display campaigns are using both image and text ads</h2>
<div class="text-content">
<p><strong>Verify display campaigns are using text and images.&nbsp;</strong></p>
<p>The more real estate your ads can take the better. Having both text and image ads will increase your ads distribution through the display network.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Display Campaigns</label>
@if(isset($result)) 
<input type="file" name="display_campaigns" accept="image/*" onchange="loadFile21(event)">
<img src="{!! asset($report['display_campaigns']) !!}" id="output21" alt="Google Tag Assistant"> 
@else 
<input type="file" name="display_campaigns" accept="image/*" onchange="loadFile21(event)" class="validate[required]">
<img id="output21"/>
@endif
@if($errors->has('display_campaigns'))
<span class="text-danger"><br>{{$errors->first('display_campaigns')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="keywords">
<h2>Keywords:</h2>
</div>

<div id="check-your-search-terms-for-negative-keywords">
<h2>Check your search terms for negative keywords</h2>
<div class="text-content">
<p>Look over the campaign keywords and make sure negative keywords are being used.&nbsp;</p>
<p>Choosing the proper keyword to associate with your ads will determine success or failure in your PPC efforts. If you need a refresher on conducting keyword research we have put together an in-depth <span style="color: #007db6;"><strong><a href="https://www.process.st/checklist/seo-checklist-the-keyword-research-process/" rel="nofollow" target="_blank">Keyword Research Checklist</a></strong></span>. You can easily apply it to your Process Street account with one simple click.&nbsp;&nbsp;</p>
</div>

<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Search Terms</label>
@if(isset($result)) 
<input type="file" name="search_terms" accept="image/*" onchange="loadFile22(event)">
<img src="{!! asset($report['search_terms']) !!}" id="output22" alt="Google Tag Assistant"> 
@else 
<input type="file" name="search_terms" accept="image/*" onchange="loadFile22(event)" class="validate[required]">
<img id="output22"/>
@endif
@if($errors->has('search_terms'))
<span class="text-danger"><br>{{$errors->first('search_terms')}}</span>
@endif
</div> 
</div>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/j8x197-c2X0?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
<div class="text-content">
<p>Record negative keywords being used.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Negative keywords</label>
<textarea  rows="3" name="negative_keywords" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['negative_keywords'] }} @endif</textarea>
@if($errors->has('negative_keywords'))
<span class="text-danger"><br>{{$errors->first('negative_keywords')}}</span>
@endif
</div> 
</div>
</div>
</div>


<div id="check-keyword-match-types">
<h2>Check keyword match types</h2>
<div class="text-content">
<p><strong>Review the keyword match types</strong> that are associated with each ad. Then determine if they are correct. You can access the Keywords list by clicking on the Keywords tab in Google Ads.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keyword match types</label>
<textarea  rows="3" name="keyword_match_types" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['keyword_match_types'] }} @endif</textarea>
@if($errors->has('keyword_match_types'))
<span class="text-danger"><br>{{$errors->first('keyword_match_types')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p><span>Keyword match types will allow you to control the type of searches that will trigger your ad to appear. If yous goal is impressions and you&nbsp;want to get more eyes on your ad you could utilize broad match types. However, if you have a keyword that you know converts very well but is more targeted then you will want to use an exact match type.&nbsp;</span></p>
<p><span>Below are some additional resources:</span></p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/XAvTbSAHpxA?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
<div class="description">
Learn how to use the different keyword match types with your Google AdWords search campaigns. You'll learn about broad, phrase, exact and negative keyword match types. Plus broad match modified keywords and close variants. - Benjamin Mangold from Loves Data
</div>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Match Type</label>
@if(isset($result)) 
<input type="file" name="match_type" accept="image/*" onchange="loadFile23(event)">
<img src="{!! asset($report['match_type']) !!}" id="output23" alt="Google Tag Assistant"> 
@else 
<input type="file" name="match_type" accept="image/*" onchange="loadFile23(event)" class="validate[required]">
<img id="output23"/>
@endif
@if($errors->has('match_type'))
<span class="text-danger"><br>{{$errors->first('match_type')}}</span>
@endif
</div> 
</div>
</div>
</div>


<div id="ensure-there-are-no-keyword-conflicts">
<h2>Ensure there are no keyword conflicts</h2>
<div class="text-content">
<p><span>Review the account <strong>to ensure there are no keyword conflicts</strong> occurring.&nbsp;</span></p>
<p><span>If a keyword conflict is happening you will want to compare your keyword list versus your negative keyword list. When a conflict occurs your ads won't show, even when they are set to active,&nbsp;due to them being blocked. The first place to look for conflict would be in the search query data.</span></p>
<p>Below is an example tracking document that was developed by Google. You can get it <a href="https://developers.google.com/google-ads/scripts/docs/solutions/adsmanagerapp-negative-keyword-conflicts" rel="nofollow" target="_blank">here</a></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Negative Keyword Conflicts</label>
@if(isset($result)) 
<input type="file" name="negative_keyword_conflicts" accept="image/*" onchange="loadFile231(event)">
<img src="{!! asset($report['negative_keyword_conflicts']) !!}" id="output231" alt="Negative Keyword Conflicts"> 
@else 
<input type="file" name="negative_keyword_conflicts" accept="image/*" onchange="loadFile231(event)" class="validate[required]">
<img id="output231"/>
@endif
@if($errors->has('negative_keyword_conflicts'))
<span class="text-danger"><br>{{$errors->first('negative_keyword_conflicts')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keyword conflict</label>
<select name="keyword_conflict" class="validate[required]"> 
	<option>Select</option> 
	@if(isset($result))
    <option value="Test 1" @if($report['keyword_conflict'] == "Test 1") selected @endif>Test 1</option> 
	<option value="Test 2" @if($report['keyword_conflict'] == "Test 2") selected @endif>Test 2</option>  
	<option value="Test 3" @if($report['keyword_conflict'] == "Test 3") selected @endif>Test 3</option> 
	<option value="Test 4" @if($report['keyword_conflict'] == "Test 4") selected @endif>Test 4</option> 
	@else
	<option value="Test 1">Test 1</option> 
	<option value="Test 2">Test 2</option>  
	<option value="Test 3">Test 3</option> 
	<option value="Test 4">Test 4</option> 
	@endif
</select>
@if($errors->has('keyword_conflict'))
<span class="text-danger"><br>{{$errors->first('keyword_conflict')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Conflict tracking document</label>
@if(isset($result)) 
<input type="file" name="conflict_tracking_document">
<a href="{{ url('/') }}{{  $report['conflict_tracking_document'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Conflict Tracking Document</a>
@else 
<input type="file" name="conflict_tracking_document" class="validate[required]">
@endif
@if($errors->has('conflict_tracking_document'))
<span class="text-danger"><br>{{$errors->first('conflict_tracking_document')}}</span>
@endif
</div> 
</div>
</div>
</div>


<div id="identify-new-search-query-opportunities">
<h2>Identify new search query opportunities</h2>
<div class="text-content">
<p><strong>Brainstorm new keyword ideas.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>New keyword ideas</label>
<textarea  rows="3" name="new_keyword_ideas" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['new_keyword_ideas'] }} @endif</textarea>
@if($errors->has('new_keyword_ideas'))
<span class="text-danger"><br>{{$errors->first('new_keyword_ideas')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 20px;">
<p>There are countless ways to find inspiration for new keywords or search queries. Make sure you take some time to get into the mind of your ideal customer. Think about what keywords they would use to find your product. What "job" is your product&nbsp;solving for? Working through the JTBD (<a href="https://hbr.org/2016/09/know-your-customers-jobs-to-be-done" rel="nofollow" target="_blank">Jobs to Be Done</a>) framework can be extremely helpful.&nbsp;</p>
</div>
</div>

<div id="verify-proper-search-query-triggered-keyword-results">
<h2>Verify proper search query triggered keyword results</h2>
<div class="text-content">
<p><strong>Go to the Keyword tab of your campaign or adgroup&nbsp;</strong>and&nbsp;verify&nbsp;the which keywords are triggering your ads. You can do this by&nbsp;looking over the&nbsp;Search&nbsp;Query&nbsp;Report.&nbsp;</p>
<p>To access the Search Query report follow these steps:</p>
<ul>
<li>Log into your Google ads account</li>
<li>Click on the "Keywords" tab</li>
<li>Select the keywords you want more information on</li>
<li>Click the "Search Terms" button&nbsp;</li>
</ul>
<p>You will then be presented with a result like this:</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Verify proper search</label>
@if(isset($result)) 
<input type="file" name="verify_proper_search" accept="image/*" onchange="loadFile24(event)">
<img src="{!! asset($report['verify_proper_search']) !!}" id="output24" alt="Google Tag Assistant"> 
@else 
<input type="file" name="verify_proper_search" accept="image/*" onchange="loadFile24(event)" class="validate[required]">
<img id="output24"/>
@endif
@if($errors->has('verify_proper_search'))
<span class="text-danger"><br>{{$errors->first('verify_proper_search')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="check-the-number-of-keywords-per-ad-group">
<h2>Check the number of keywords per ad group</h2>
<div class="text-content">
<p><strong>Check the number of keywords per ad group.</strong></p>
<p>There is no rule that dictates how many keywords you should have per ad group. You will need to determine this as you review your accounts. However, many argue that 20 should be the maximum. The thought behind this is that if you have more than 20 keywords then your ad copy is probably not as aligned with search intent as it could be. Your results will be too broad to get the best conversion.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Number of keywords</label>
@if(isset($result)) 
<input type="file" name="number_of_keywords" accept="image/*" onchange="loadFile25(event)">
<img src="{!! asset($report['number_of_keywords']) !!}" id="output25" alt="Google Tag Assistant"> 
@else 
<input type="file" name="number_of_keywords" accept="image/*" onchange="loadFile25(event)" class="validate[required]">
<img id="output25"/>
@endif
@if($errors->has('number_of_keywords'))
<span class="text-danger"><br>{{$errors->first('number_of_keywords')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="remove-keywords-with-zero-impressions">
<h2>Remove keywords with zero impressions</h2>
<div class="text-content">
<p><strong>Remove any keywords that have zero impressions.</strong></p>
<p>If your keywords are getting zero impressions then they are just taking up space in your account. They are also distracting and can clutter your account.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keywords with zero impressions</label>
@if(isset($result)) 
<input type="file" name="zero_impressions" accept="image/*" onchange="loadFile26(event)">
<img src="{!! asset($report['zero_impressions']) !!}" id="output26" alt="Google Tag Assistant"> 
@else 
<input type="file" name="zero_impressions" accept="image/*" onchange="loadFile26(event)" class="validate[required]">
<img id="output26"/>
@endif
@if($errors->has('zero_impressions'))
<span class="text-danger"><br>{{$errors->first('zero_impressions')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="quality-score">
<h2>Quality score:</h2>
</div>

<div id="check-your-keywords-quality-score-performance">
<h2>Check your keyword's quality score performance</h2>
<div class="text-content" style="margin-bottom: 15px;">
<p><strong>Check&nbsp;keyword</strong><strong>&nbsp;quality score.</strong></p>
<p>To check the relevance of your ad in comparison to the keywords that are associated with it, you will want to look at the Quality Score.&nbsp;</p>
<p>The easiest way to check this is by following these steps:</p>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="http://ads.google.com/" rel="nofollow">Google Ads account</a>.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Keywords.</strong>&nbsp;</li>
<li>Hover over a keyword’s status in the “Status” column. You'll be able to see ratings for<span>&nbsp;</span><strong>expected clickthrough rate</strong>,<span>&nbsp;</span><strong>ad relevance</strong>, and l<strong>anding page experience</strong>.</li>
</ol>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/1oHwl0tFHz8?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
<div class="description">
AdHawk also put together a short tutorial.
</div>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Ad quality score</label>
<input  type="text" name="ad_quality_score" class="validate[required]" @if(isset($result)) value="{{ $report['ad_quality_score'] }}" @endif class="form-control">
@if($errors->has('ad_quality_score'))
<span class="text-danger"><br>{{$errors->first('ad_quality_score')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="eliminate-keywords-with-poor-quality-score">
<h2>Eliminate keywords with poor quality score</h2>
<div class="text-content">
<p><strong>Eliminate any keywords with a quality score under 7.&nbsp;</strong></p>
<p>To remove poor performing keywords all you need to do is go to the Keywords section in the page menu and then click on&nbsp;<strong>Search Keywords&nbsp;</strong>or&nbsp;<strong>Display/Video keywords</strong>. Select the keyword you want to remove and choose&nbsp;<strong>Edit</strong> then&nbsp;<strong>Remove</strong>.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keywords with poor quality</label>
@if(isset($result)) 
<input type="file" name="keywords_with_poor_quality" accept="image/*" onchange="loadFile27(event)">
<img src="{!! asset($report['keywords_with_poor_quality']) !!}" id="output27" alt="Google Tag Assistant"> 
@else 
<input type="file" name="keywords_with_poor_quality" accept="image/*" onchange="loadFile27(event)" class="validate[required]">
<img id="output27"/>
@endif
@if($errors->has('keywords_with_poor_quality'))
<span class="text-danger"><br>{{$errors->first('keywords_with_poor_quality')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Removed keywords</label>
<textarea  rows="3" name="removed_keywords" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['removed_keywords'] }} @endif</textarea>
@if($errors->has('removed_keywords'))
<span class="text-danger"><br>{{$errors->first('removed_keywords')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="ad-extension">
<h2>Ad extension:</h2>
</div>

<div id="ensure-gmb-is-linked-to-your-ads-account-to-enable-location-extensions-if-applicable">
<h2>Ensure GMB is linked to your Ads account to enable location extensions (if applicable)</h2>
<div class="text-content">
<p><strong>Make sure location extensions are setup. This can be done by linking Google Ads with Google My Business. </strong></p>
<p><span>By connecting Google My Business (GMB) to your Google Ads account you will be able to enable location extensions. If your business doesn't utilize a physical location and only handles support through email then this might not be as important to you. However, if your a business focusing on a specific geographic area or want to make your contact information more easily accessible, then this would be good to setup.&nbsp;</span></p>
<p><span>Location extensions will attach your address, a map to your location, or even the distance to your business to the ad you are running. Users will then be able to click the additional information about your location for further details. This could direct them to your location page or even a call button.&nbsp;</span></p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/Tc8LXO8y_a8?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
</div>

<div id="check-for-campaigns-with-missing-ad-extensions">
<h2>Check for campaigns with missing ad extensions</h2>
<div class="text-content">
<p>Review your campaigns and make sure that <strong>all extensions</strong> that are relevant have been enabled. You can go to the Ads &amp; extensions tab to see which extensions are currently enabled.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Missing ad extensions</label>
@if(isset($result)) 
<input type="file" name="missing_ad_extensions" accept="image/*" onchange="loadFile28(event)">
<img src="{!! asset($report['missing_ad_extensions']) !!}" id="output28" alt="Google Tag Assistant"> 
@else 
<input type="file" name="missing_ad_extensions" accept="image/*" onchange="loadFile28(event)" class="validate[required]">
<img id="output28"/>
@endif
@if($errors->has('missing_ad_extensions'))
<span class="text-danger"><br>{{$errors->first('missing_ad_extensions')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="remove-any-nonperforming-automated-extensions">
<h2>Remove any non-performing automated extensions</h2>
<div class="text-content">
<p><strong>Remove any extensions that are not converting.&nbsp;</strong></p>
<p>If an extension isn't bringing any value then it might be distracting from your ad. Be sure to review their performance.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Non-performing automated extensions</label>
@if(isset($result)) 
<input type="file" name="non_performing_automated_extensions" accept="image/*" onchange="loadFile29(event)">
<img src="{!! asset($report['non_performing_automated_extensions']) !!}" id="output29" alt="Google Tag Assistant"> 
@else 
<input type="file" name="non_performing_automated_extensions" accept="image/*" onchange="loadFile29(event)" class="validate[required]">
<img id="output29"/>
@endif
@if($errors->has('non_performing_automated_extensions'))
<span class="text-danger"><br>{{$errors->first('non_performing_automated_extensions')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="check-the-status-of-ad-extension-approved-or-disapproved">
<h2>Check the status of ad extension (approved or disapproved)</h2>
<div class="text-content">
<p><strong>Check the current approval status of each ad extension.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Current approval status</label>
@if(isset($result)) 
<input type="file" name="current_approval_status" accept="image/*" onchange="loadFile30(event)">
<img src="{!! asset($report['current_approval_status']) !!}" id="output30" alt="Google Tag Assistant"> 
@else 
<input type="file" name="current_approval_status" accept="image/*" onchange="loadFile30(event)" class="validate[required]">
<img id="output30"/>
@endif
@if($errors->has('current_approval_status'))
<span class="text-danger"><br>{{$errors->first('current_approval_status')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Ad extensions approved</label>
<select name="ad_extensions_approved" class="validate[required]"> 
	<option>Select</option> 
	@if(isset($result))
    <option value="Test 1" @if($report['ad_extensions_approved'] == "Test 1") selected @endif>Test 1</option> 
	<option value="Test 2" @if($report['ad_extensions_approved'] == "Test 2") selected @endif>Test 2</option>  
	<option value="Test 3" @if($report['ad_extensions_approved'] == "Test 3") selected @endif>Test 3</option> 
	<option value="Test 4" @if($report['ad_extensions_approved'] == "Test 4") selected @endif>Test 4</option>
	@else
	<option value="Test 1">Test 1</option> 
	<option value="Test 2">Test 2</option>  
	<option value="Test 3">Test 3</option> 
	<option value="Test 4">Test 4</option> 
	@endif
</select>
@if($errors->has('ad_extensions_approved'))
<span class="text-danger"><br>{{$errors->first('ad_extensions_approved')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Additional notes</label>
<textarea  rows="3" name="additional_notes" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['additional_notes'] }} @endif</textarea>
@if($errors->has('additional_notes'))
<span class="text-danger"><br>{{$errors->first('additional_notes')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="ensure-sitelinks-have-relevant-descriptions">
<h2>Ensure sitelinks have relevant descriptions</h2>
<div class="text-content">
<p><strong>Make sure the sitelink</strong><strong>&nbsp;descriptions being used are supporting your main ad.</strong></p>
<p>To update or edit your sitelinks&nbsp;follow these steps:</p>
<ol>
<li>Click<span>&nbsp;</span><strong>Ads &amp; extensions</strong><span>&nbsp;</span>in the page menu on the left, then click<span>&nbsp;</span><strong>Extensions</strong><span>&nbsp;</span>at the top of the page.</li>
<li>Click the blue plus button,&nbsp;then click&nbsp;<strong>+ Sitelink extension.</strong></li>
<li>From the “Add to” drop-down menu, choose the level you’d like to add the sitelink to.&nbsp;</li>
<li>To use an existing sitelink, click<span>&nbsp;</span><strong>Use existing</strong><span>&nbsp;</span>and select the sitelinks that you’d like to add.</li>
<li>To create a new sitelink, click<span>&nbsp;</span><strong>Create new</strong>.</li>
<li>Fill out the sitelink text and URL.</li>
<li>Enter additional text about your link in the Description fields. When you fill out both lines of description, your sitelink may be eligible to show with these details.</li> 
<li>Click<span>&nbsp;</span><strong>Save</strong><span>&nbsp;</span>to save your sitelink setting for your campaign.</li>
</ol>
<p><span>The more space your ad can take on relevant search engine result pages the better! Be sure you aren't wasting that space with irrelevant descriptions that could decrease your conversion rate and increase your customer acquisition cost.</span></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Ensure sitelinks</label>
@if(isset($result)) 
<input type="file" name="ensure_sitelinks" accept="image/*" onchange="loadFile31(event)">
<img src="{!! asset($report['ensure_sitelinks']) !!}" id="output31" alt="Google Tag Assistant"> 
@else 
<input type="file" name="ensure_sitelinks" accept="image/*" onchange="loadFile31(event)" class="validate[required]">
<img id="output31"/>
@endif
@if($errors->has('ensure_sitelinks'))
<span class="text-danger"><br>{{$errors->first('ensure_sitelinks')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="make-sure-your-call-extension-is-scheduled-during-operational-hours">
<h2>Make sure your call extension is scheduled during operational hours</h2>
<div class="text-content">
<p>Look over your call extensions and <strong>make sure they are scheduled to only run during business hours</strong>.&nbsp;</p>
<p>Nothing is worse than a hot lead turning cold. Don't have call extensions running if no one can answer the phone. This is a simple fix that could dramatically impact your campaigns.&nbsp;&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Call extension</label>
@if(isset($result)) 
<input type="file" name="call_extension" accept="image/*" onchange="loadFile32(event)">
<img src="{!! asset($report['call_extension']) !!}" id="output32" alt="Google Tag Assistant"> 
@else 
<input type="file" name="call_extension" accept="image/*" onchange="loadFile32(event)" class="validate[required]">
<img id="output32"/>
@endif
@if($errors->has('call_extension'))
<span class="text-danger"><br>{{$errors->first('call_extension')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="check-if-call-reporting-is-enabled-to-track-conversions-from-your-call-extensions">
<h2>Check if ‘call reporting’ is enabled to track conversions from your call extensions</h2>
<div class="text-content">
<p><strong>If you are using call extensions make sure that call reporting has been turned on.&nbsp;</strong></p>
<p>Please note that this is only available on the Search Network. Call reporting runs off of Google forwarding numbers. It lets you track the performance of your call extensions and call-only ads. This report will give you information on call duration, the start and stop time, the callers' area code, and if the call was successfully connected.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Call reporting</label>
@if(isset($result)) 
<input type="file" name="call_reporting" accept="image/*" onchange="loadFile33(event)">
<img src="{!! asset($report['call_reporting']) !!}" id="output33" alt="Google Tag Assistant"> 
@else 
<input type="file" name="call_reporting" accept="image/*" onchange="loadFile33(event)" class="validate[required]">
<img id="output33"/>
@endif
@if($errors->has('call_reporting'))
<span class="text-danger"><br>{{$errors->first('call_reporting')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="remarketing-campaigns">
<h2>Remarketing campaigns:</h2>
</div>

<div id="validate-remarketing-code-has-been-added-correctly">
<h2>Validate remarketing code has been added correctly</h2>
<div class="text-content">
<p><strong>Validate that the remarketing code has been added correctly.</strong></p>
<p>Remarketing allows you to show ads to individuals who have already visited your website. You do this by putting snippets of code on your site. You will need to ensure that the global site tag and event snippets are properly setup on your website. The global site snippet works with Google's conversion tracking and remarketing products. This allows you to target your ads to relevant visitors through tracking cookies.&nbsp;</p>
<p><strong>NOTE: With GDPR and other privacy updates it is important to inform your site visitors about any data collection processes.</strong>&nbsp;</p>
<p>To create or edit your remarketing event snippets and global site tag please follow the below steps:</p>
<ul></ul>
<ol>
<li>Sign in to<span>&nbsp;</span><a href="https://adwords.google.com/" rel="nofollow">AdWords</a>.</li>
<li>Click on the tool icon<span>&nbsp;</span>&nbsp;in the upper right and click&nbsp;<strong>Audience Manager&nbsp;</strong>under the section labeled<span>&nbsp;</span><strong>“Shared library”</strong></li>
<li>On the left, click<span>&nbsp;</span><strong>Audience sources</strong>. This opens a group of sources from which you can create remarketing lists.</li>
<li>In the “AdWords tag” card, click<span>&nbsp;</span><strong>SET UP TAG</strong>.
<ul>
<li>If you’ve already set up a tag, select<span>&nbsp;</span><strong>Edit source</strong><span>&nbsp;</span>from the 3-dot icon in its upper right of the “AdWords tag” card. If you’re just getting the code, skip to step 6.</li>
</ul> </li>
<li>Select which type of data the tag will collect, standard data or specific attributes/parameters.
<ul>
<li>You can also choose to include the user ID parameter with the tag. This will collect the user IDs of people who visit your website so you can show more targeted ads.</li>
<li>If you select specific attributes or parameters, you can choose the business type that best represents your products and services.&nbsp;</li>
</ul> </li>
<li>Click<span>&nbsp;</span><strong>CREATE AND CONTINUE</strong>.
<ul>
<li>This button will say<span>&nbsp;</span><strong>SAVE AND CONTINUE</strong><span>&nbsp;</span>for an existing tag.</li>
</ul> </li>
<li>When the installation screen appears, your global site tag and event snippet will be ready for use. You can copy the code, use Tag Manager, download the tag, or email the tag to a webmaster.
<ul>
<li>To integrate remarketing into your site, copy the code and paste it between the &lt;head&gt;&lt;/head&gt; tags of the website.</li>
<li>The global site tag must be added into every page of your site. The event snippet only needs to be added into the specific pages you want to track for dynamic remarketing events.</li>
</ul> </li>
<li>Click<span>&nbsp;</span><strong>DONE</strong>.</li>
<li>In the following “What’s Next” confirmation screen, click<span>&nbsp;</span><strong>DONE</strong><span>&nbsp;</span>again.
<ul>
<li>Optional: If you previously added the global site tag from another Google product such as Google Analytics, make sure you add the 'config' command from the AdWords tag (highlighted below) to every existing instance of the tag. Note that "AW-123456789" is only an example of an ID. You'll need to replace this part of the 'config' command with your customer ID in the global site tag box.<br>&nbsp;&nbsp; &nbsp;&lt;script async src="https://www.googletagmanager.com/gtag/js?id=AW-123456789"&gt;&lt;/script&gt;<br>&nbsp;&nbsp; &nbsp;&lt;script&gt;<br>&nbsp;&nbsp; &nbsp;window.dataLayer = window.dataLayer || [];<br>&nbsp;&nbsp; &nbsp;function gtag(){dataLayer.push(arguments);}<br>&nbsp;&nbsp; &nbsp;gtag('js', new Date());<br>&nbsp;&nbsp;&nbsp; &nbsp;<br>&nbsp;&nbsp; &nbsp;gtag('config', 'AW-123456789');<br>&nbsp;&nbsp; &nbsp;&lt;/script&gt;</li>
<li>Optional: If you’re using an event snippet for remarketing and don’t want the global site tag to send an extra hit, add the highlighted portion below to your global site tag’s ‘config’ command.<br>&nbsp;&nbsp; &nbsp;gtag('config', 'AW-123456789',<span>&nbsp;</span>{'send_page_view': false});</li>
</ul> </li>
</ol>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Validate remarketing code</label>
@if(isset($result)) 
<input type="file" name="validate_remarketing_code" accept="image/*" onchange="loadFile34(event)">
<img src="{!! asset($report['validate_remarketing_code']) !!}" id="output34" alt="Google Tag Assistant"> 
@else 
<input type="file" name="validate_remarketing_code" accept="image/*" onchange="loadFile34(event)" class="validate[required]">
<img id="output34"/>
@endif
@if($errors->has('validate_remarketing_code'))
<span class="text-danger"><br>{{$errors->first('validate_remarketing_code')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="validate-remarketing-lists-are-properly-collecting-visitors">
<h2>Validate remarketing lists are properly collecting visitors</h2>
<div class="text-content">
<p><strong>Make sure that your remarketing lists are properly collecting visitor data.</strong> If the data isn't showing impressions or you see a lot of zeros then you should ensure that remarketing snippets have been properly installed.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Validate remarketing lists</label>
@if(isset($result)) 
<input type="file" name="validate_remarketing_list" accept="image/*" onchange="loadFile35(event)">
<img src="{!! asset($report['validate_remarketing_list']) !!}" id="output35" alt="Google Tag Assistant"> 
@else 
<input type="file" name="validate_remarketing_list" accept="image/*" onchange="loadFile35(event)" class="validate[required]">
<img id="output35"/>
@endif
@if($errors->has('validate_remarketing_list'))
<span class="text-danger"><br>{{$errors->first('validate_remarketing_list')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="verify-the-remarketing-lists-created">
<h2>Verify the remarketing lists created</h2>
<div class="text-content">
<p>Be sure that the <strong>remarketing lists created are relevant to your ads</strong> and are properly named. Using a consistent naming convention will help you stay organized.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Verify the remarketing lists</label>
@if(isset($result)) 
<input type="file" name="verify_the_remarketing_lists" accept="image/*" onchange="loadFile36(event)">
<img src="{!! asset($report['verify_the_remarketing_lists']) !!}" id="output36" alt="Google Tag Assistant"> 
@else 
<input type="file" name="verify_the_remarketing_lists" accept="image/*" onchange="loadFile36(event)" class="validate[required]">
<img id="output36"/>
@endif
@if($errors->has('verify_the_remarketing_lists'))
<span class="text-danger"><br>{{$errors->first('verify_the_remarketing_lists')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="metric-review">
<h2>Metric review:</h2>
</div>

<div id="review-primary-kpis">
<h2>Review primary KPI's</h2>
<div class="text-content">
<p><strong>Review primary KPI's for the account.</strong></p>
<p>There are many metrics that can be analyzed when conducting a PPC Audit. Each account is different and has specific goals created for them. Below we have listed a few common metrics that are reviewed.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>KPI's measured</label>
<select name="kpis_measured" class="validate[required]"> 
	<option>Select</option> 
	@if(isset($result))
    <option value="Average Position" @if($report['kpis_measured'] == "Average Position") selected @endif>Average Position</option> 
	<option value="CTR (Click Through Rate)" @if($report['kpis_measured'] == "CTR (Click Through Rate)") selected @endif>CTR (Click Through Rate)</option>  
	<option value="CPC (Cost Per Click)" @if($report['kpis_measured'] == "CPC (Cost Per Click)") selected @endif>CPC (Cost Per Click)</option> 
<option value="CPA (Cost Per Conversion/ Acquisition)" @if($report['kpis_measured'] == "CPA (Cost Per Conversion/ Acquisition)") selected @endif>CPA (Cost Per Conversion/ Acquisition)</option> 
	<option value="Average Position" @if($report['kpis_measured'] == "Average Position") selected @endif>Average Position</option> 
	<option value="CPM (Cost Per Impression)" @if($report['kpis_measured'] == "CPM (Cost Per Impression)") selected @endif>CPM (Cost Per Impression)</option>  
	<option value="Other" @if($report['kpis_measured'] == "Other") selected @endif>Other</option> 
	@else
    <option value="Average Position">Average Position</option> 
	<option value="CTR (Click Through Rate)">CTR (Click Through Rate)</option>  
	<option value="CPC (Cost Per Click)">CPC (Cost Per Click)</option> 
	<option value="CPA (Cost Per Conversion/ Acquisition)">CPA (Cost Per Conversion/ Acquisition)</option> 
	<option value="Average Position">Average Position</option> 
	<option value="CPM (Cost Per Impression)">CPM (Cost Per Impression)</option>  
	<option value="Other">Other</option> 
	@endif
</select>
@if($errors->has('kpis_measured'))
<span class="text-danger"><br>{{$errors->first('kpis_measured')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>KPI notes</label>
<textarea  rows="3" name="kpi_notes" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['kpi_notes'] }} @endif</textarea>
@if($errors->has('kpi_notes'))
<span class="text-danger"><br>{{$errors->first('kpi_notes')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="validate-budget-utilization">
<h2>Validate budget utilization</h2>
<div class="text-content">
<p><strong>Make sure the budget was properly utilized.</strong> You should include notes on why the budget was or wasn't fully used. This can be input in the form field below.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Was the campaign budget fully attained?</label>
<select name="campaign_budget" class="validate[required]"> 
	<option>Select</option> 
	@if(isset($result))
    <option value="test 1" @if($report['campaign_budget'] == "test 1") selected @endif>test 1</option> 
	<option value="test 2" @if($report['campaign_budget'] == "test 2") selected @endif>test 2</option> 
	<option value="test 3" @if($report['campaign_budget'] == "test 3") selected @endif>test 3</option> 
	<option value="test 4" @if($report['campaign_budget'] == "test 4") selected @endif>test 4</option> 
	<option value="test 5" @if($report['campaign_budget'] == "test 5") selected @endif>test 5</option> 
	@else
	<option value="test 1">test 1</option> 
	<option value="test 2">test 2</option> 
	<option value="test 3">test 3</option> 
	<option value="test 4">test 4</option> 
	<option value="test 5">test 5</option> 
	@endif
</select>
@if($errors->has('campaign_budget'))
<span class="text-danger"><br>{{$errors->first('campaign_budget')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Budget notes</label>
<textarea  rows="3" name="budget_notes" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['budget_notes'] }} @endif</textarea>
@if($errors->has('budget_notes'))
<span class="text-danger"><br>{{$errors->first('budget_notes')}}</span>
@endif
</div> 
</div>
</div>
</div>

<div id="reportaction-items">
<h2>Report/Action items:</h2>
</div>

<div id="prioritize-action-items">
<h2>Prioritize action items</h2>
<div class="text-content">
<p>Now that you have completed the audit you should <strong>write down your major takeaways</strong> and prioritize action items.</p>
<p>The best place to start is to identify any concerns. List anything that you noticed was wrong or incorrectly setup. From the list of concerns, you should be able to identify quick wins. Some examples could be that the remarketing pixel isn't setup correctly or that incorrect demographic information had been chosen for an ad. These would be easy fixes that could make a significant impact.</p>
<p>After you have written out quick wins then you should put together a list of next steps. Think about what would make the most impact in the quickest amount of time.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Concerns</label>
<textarea  rows="3" name="concerns" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['concerns'] }} @endif</textarea>
@if($errors->has('concerns'))
<span class="text-danger"><br>{{$errors->first('concerns')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Quick wins</label>
<textarea  rows="3" name="quick_wins" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['quick_wins'] }} @endif</textarea>
@if($errors->has('quick_wins'))
<span class="text-danger"><br>{{$errors->first('quick_wins')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Next steps</label>
<textarea  rows="3" name="next_steps" class="validate[required]" class="form-control">@if(isset($result)) {{ $report['next_steps'] }} @endif</textarea>
@if($errors->has('next_steps'))
<span class="text-danger"><br>{{$errors->first('next_steps')}}</span>
@endif
</div> 
</div>
</div>
</div>

               
<div id="create-a-report-if-needed">
<h2>Create a report (if needed)</h2>
<div class="text-content">
<p><strong>Decide if a report needs to be made.</strong>&nbsp;If you do need to put together a&nbsp;client facing report then it should, at a minimum, include the following:</p>
<ul>
<li>Areas of concern</li>
<li>Quick wins</li>
<li>Action items</li>
</ul>
<p>Every organization has a different structure or tool utilized to create a PPC audit report.&nbsp; If you are unsure of what tool you should use we would recommend reviewing this article by <a href="https://www.wordstream.com/blog/ws/2018/02/09/best-reporting-tools" rel="nofollow" target="_blank">Wordstream</a>.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Report created</label>
<select name="report_created" class="validate[required]"> 
	<option>Select</option> 
	@if(isset($result))
    <option value="test 1" @if($report['report_created']== "test 1") selected @endif>test 1</option> 
	<option value="test 2" @if($report['report_created']== "test 2") selected @endif>test 2</option> 
	<option value="test 3" @if($report['report_created']== "test 3") selected @endif>test 3</option> 
	<option value="test 4" @if($report['report_created']== "test 4") selected @endif>test 4</option> 
	<option value="test 5" @if($report['report_created']== "test 5") selected @endif>test 5</option> 
	@else
	<option value="test 1">test 1</option> 
	<option value="test 2">test 2</option> 
	<option value="test 3">test 3</option> 
	<option value="test 4">test 4</option> 
	<option value="test 5">test 5</option> 
	@endif
</select>
@if($errors->has('report_created'))
<span class="text-danger"><br>{{$errors->first('report_created')}}</span>
@endif
</div> 
</div>
</div>
<div class="text-content" style="margin-top:15px;">
<p>Using the Members form field below,&nbsp;<strong>assign the individual who will approve the audit report.</strong></p>
</div>
</div>


<div id="prepare-report-for-review">
<h2>Prepare report for review</h2>
<div class="text-content">
<p>After your report is complete you will want to email it or print it.&nbsp;</p>
<p class="style-info"><span><strong>PRO TIP: A great way to merge your data from Process Street with your report is through our integration with <a href="https://www.process.st/help/docs/webmerge/" rel="nofollow" target="_blank">WebMerge.</a>&nbsp;</strong></span></p>
<p>To send the email you can&nbsp;use the <a href="https://www.process.st/help/docs/using-email-widget/" rel="nofollow">email widget</a> below. All you need to do is customize the message and upload your file.&nbsp;</p>
<p>Then just <strong>make sure everything looks okay and hit "Send".</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Report</label>
@if(isset($result)) 
<input type="file" name="report">
<a href="{{ url('/') }}{{  $report['report'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Report</a>
@else 
<input type="file" name="report" class="validate[required]">
@endif

@if($errors->has('report'))
<span class="text-danger"><br>{{$errors->first('report')}}</span>
@endif
</div> 
</div>
</div>
</div>

<!-- <div id="approval-audit-report">
<h2>Approval: Audit report</h2>
<div class="approval-content">
<div class="header">
<div class="list-title">
Will be submitted for approval:
</div>
</div>
</div>
</div> -->

<div id="sources">
<h2>Sources:</h2>
<div class="text-content">
<ul>
<li><a href="https://adespresso.com/blog/google-ads-negative-keywords/" rel="nofollow" target="_blank">Ad Espresso - Google Ads Negative Keywords List</a></li>
<li><a href="https://www.wordstream.com/allen-finn-3540" rel="nofollow" target="_blank">Allen Finn</a> - <a href="https://www.wordstream.com/blog/ws/2018/02/13/adwords-ad-variations" rel="nofollow" target="_blank">10X Your A/B Testing with Google Ads Variations</a></li>
<li><a href="https://www.pigzilla.co/author/dani-owens/" rel="nofollow" target="_blank">Dani Owens</a> -<a href="https://www.pigzilla.co/blog/local-conversion-actions-in-google-ads/" rel="nofollow" target="_blank"> Local Conversion Actions in Google Ads</a></li>
<li><a href="https://www.semrush.com/user/145674065/" rel="nofollow" target="_blank">Emma McHale</a> - <a href="https://www.semrush.com/blog/how-to-conduct-a-complete-ppc-audit/" rel="nofollow" target="_blank">How to Conduct a Complete PPC Audit</a></li>
<li><a href="https://support.google.com/google-ads/answer/7102466?co=ADWORDS.IsAWNCustomer%3Dfalse&amp;hl=en" rel="nofollow" target="_blank">Google - Google Ads Help</a></li>
<li><a href="https://www.marketdominationmedia.com/author/jlong/" rel="nofollow" target="_blank">Jonathan Long</a> - <a href="https://www.marketdominationmedia.com/write-strong-paid-search-ad-copy/" rel="nofollow" target="_blank">How to Write Strong Paid Search ad Copy</a></li>
<li><a href="https://www.karooya.com/blog/author/kirti/" rel="nofollow" target="_blank">Kirti</a> - <a href="https://www.karooya.com/blog/adwords-audit-template/" rel="nofollow" target="_blank">Adwords Audit Template</a></li>
<li><a href="http://marketlytics.com/blog/?author=5799fe06bebafb6333a620d5" rel="nofollow" target="_blank">Noman Karim</a> - <a href="http://marketlytics.com/blog/google-tag-manager-adwords-conversion-tracking" rel="nofollow" target="_blank">Google Tag Manager Adwords Conversion Tracking 2018</a>&nbsp;</li>
<li><a href="https://supermetrics.com/blog/google-adwords-audit-1" rel="nofollow" target="_blank">Supermetrics - Google Adwords Audit</a></li>
</ul>
<p></p>
</div>
</div>


<div id="related-checklists">
<h2>Related checklists:</h2>
<div class="text-content">
<ul>
<li><a href="https://process.st/marketing-process" rel="nofollow">Marketing Process Toolkit: 10 Checklists to Crush Your Competition</a></li>
<li><a href="https://www.process.st/checklist/ppc-audit-checklist/%20" rel="nofollow">PPC Audit Checklist</a></li>
<li><a href="https://www.process.st/checklist/new-facebook-ads-creation-checklist/%20" rel="nofollow">New Facebook Ads Creation Checklist</a></li>
<li><a href="https://www.process.st/checklist/ppc-daily-campaign-review-checklist/" rel="nofollow">PPC Daily Campaign Review Checklist</a></li>
<li><a href="https://www.process.st/checklist/ppc-weekly-campaign-review-checklist/" rel="nofollow">PPC Weekly Campaign Review Checklist</a></li>
<li><a href="https://www.process.st/checklist/ppc-monthly-campaign-review-checklist/" rel="nofollow">PPC Monthly Campaign Review Checklist</a></li>
<li><a href="https://www.process.st/checklist/performance-marketing-ppc-keyword-competition-analysis-checklist/%20" rel="nofollow">Performance Marketing (PPC) Keyword Competition Analysis Checklist</a></li>
<li><a href="https://www.process.st/checklist/seo-checklist-the-keyword-research-process/" rel="nofollow">SEO Checklist: The Keyword Research Process</a></li>
</ul>
</div>


<div class="row">
<div class="col-md-10" style="margin-top: 20px;margin-bottom: 20px;">
<div class="form-group"> 
<label>Enter email to send report</label>
<input type="email" @if(isset($result)) value="{{ $report['email'] }}" @endif name="email" class="validate[required]">
@if($errors->has('email'))
<span class="text-danger"><br>{{$errors->first('email')}}</span>
@endif
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;margin-bottom: 20px;">
<div class="form-group"> 
<label>Enter password to protect the file</label>
<input type="text" @if(isset($result)) value="{{ $report['password'] }}" @endif name="password" class="validate[required]">
@if($errors->has('password'))
<span class="text-danger"><br>{{$errors->first('password')}}</span>
@endif
</div> 
</div>
</div>                                    
</div>
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 20px;">
                                @if(isset($result)) 
                                    <button id="Go_to_Bottom" type="submit" class="btn btn-default w3ls-button">Submit</button> 
                                @else
                                    <input type="submit" id="Go_to_Bottom" class="save_report btn btn-default w3ls-button" value="Submit">
                                @endif
                                </div>
                            </div>        
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>
<id id="loading-image" style="display: none;">
<img src="{{ url('/') }}/images/loader.gif" alt="" />
</id>
<a id="Go_to_Bottom1" href="#Go_to_Bottom" title="Go to Bottom"><i class="fa fa-arrow-down"></i></a>
<style type="text/css">
.scrollable {
    padding-left: 0px;
}    
nav.main-menu{
    display: none;
}

.main-grid {
    padding-left: 0px !important;
    padding-right: 0px !important;
}
h1.page-header{
    padding-left: 35px;
    padding-right: 35px;
}
.forms label{
    font-weight: 700;
    font-size: 14px;
    width: 100%;    
}
body {
    background: #fff;
}
.panel.panel-widget.forms-panel{
    border:0px;
}
.dashboard-page .title-bar {
    position: fixed;
    width: 100%;
    z-index: 99999;
    background: #0a7b9d;
}
.logo h1 a{
	color: #fff;
}
span.prfil-img i.fa.fa-user {
    color: #0a7b9d;
    background: #fff;
}
textarea {
    min-height: 85px;
    border-radius: 5px;
}
input[type="file"]{
	border: 0px !important;
	padding-left: 0px !important;
}





</style>



<script type="text/javascript">
	
var account_name = document.getElementById('account_name');
  account_name.onkeyup = account_name.onkeypress = function(){
      document.getElementById('show_account_name').innerHTML = this.value;
}

</script>




@stop

