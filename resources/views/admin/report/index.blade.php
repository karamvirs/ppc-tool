@extends('admin.layouts.master')
@section('css')
<!-- tables -->
<link rel="stylesheet" type="text/css" href="{!! asset('css/table-style.css') !!}" />
<!-- //tables -->
@endsection
@section('content')

<div class="agile-grids">   
    <div class="grids">       
        <div class="row">
            <div class="col-md-12">                
                <h1 class="page-header">Reports Listing <a class="btn btn-sm btn-primary pull-right" href="{!! route('report.create') !!}" style="margin-left: 20px;"> <i class="fa fa-plus fa-fw"></i> Create Report </a></h1>

                <div class="agile-tables">
                    <div class="w3l-table-info">

                        {{-- for message rendering --}}
                        @include('admin.layouts.messages')
                        @if (Session::has('report_sent'))
                        <div class="alert alert-success">
                          <button data-dismiss="alert" class="close">
                              &times;
                          </button>
                          <i class="fa fa-check-circle"></i> &nbsp;
                          The report has been sent successfully
                      </div>
                        @endif
                        

                        <form action="{{ route('report.action') }}" method="post">
                            <div class="col-md-3 text-right pull-right padding0 marginbottom10">
                                {!! lang('common.per_page') !!}: {!! Form::select('name', ['20' => '20', '40' => '40', '100' => '100', '200' => '200', '300' => '300'], '20', ['id' => 'per-page']) !!}
                            </div>
                            <div class="col-md-3 padding0 marginbottom10">
                                {!! Form::hidden('page', 'search') !!}
                                {!! Form::hidden('_token', csrf_token()) !!}
                                {!! Form::text('name', null, array('class' => 'form-control live-search', 'placeholder' => 'Search')) !!}
                            </div>
                            <table id="paginate-load" data-route="{{ route('report.paginate') }}" class="table table-hover">
                            </table>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@stop

