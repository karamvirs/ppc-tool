<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th>Account Name</th>
    <th>Primary Contact Name</th>    
    <th>Date Audit Started</th> 
    @if(((\Auth::user()->user_type)) == 1)
    <th>Created By</th>  
    @endif 
    <th class="text-center">Send Report</th>  
    <th class="text-center">{!! lang('common.action') !!}</th>
    @if(((\Auth::user()->user_type)) == 1)
    <th class="text-center">Delete</th>  
    @endif 
</tr>
</thead>
<tbody>
<?php $index = 1; ?>


@foreach($data as $detail)
<?php
$report = json_decode($detail->data, true);

?>

<tr id="order_{{ $detail->id }}">
    <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
    <td><a href="{!! route('report.edit', [$detail->id]) !!}">{!! $report['account_name'] !!}</a></td>
    <td>{!! $report['primary_contact_first_name'] !!} {!! $report['primary_contact_last_name'] !!}</td>
    <td>{!! $report['date_audit_started'] !!}</td>
    @if(((\Auth::user()->user_type)) == 1)
    <td><a href="{!! route('customer.edit', [$detail->user_id]) !!}">{{ $detail->name }}</a></td>
    @endif
    <td class="text-center col-md-1"><a class="btn btn-xs btn-primary" href="{!! route('share_report', [$detail->id]) !!}"><i class="fa fa-share"></i></a></td>
    <td class="text-center col-md-1">
        <a class="btn btn-xs btn-primary" href="{{ route('report.edit', [$detail->id]) }}"><i class="fa fa-edit"></i></a>
    </td> 
    @if(((\Auth::user()->user_type)) == 1)
    <td class="text-center col-md-1">  
    <a title="{!! lang('common.delete') !!}" class="btn btn-xs btn-danger __drop" data-route="{!! route('report.drop', [$detail->id]) !!}" data-message="{!! lang('messages.sure_delete', string_manip(lang('report.report'))) !!}" href="javascript:void(0)"><i class="fa fa-times"></i></a> 
    </td>
    @endif
</tr>
@endforeach
@if (count($data) < 1)
<tr>
    <td class="text-center" colspan="8"> {!! lang('messages.no_data_found') !!} </td>
</tr>
@else
<tr>
    <td colspan="10">
        {!! paginationControls($page, $total, $perPage) !!}
    </td>
</tr>
@endif
</tbody>