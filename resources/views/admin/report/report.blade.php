@extends('admin.layouts.master')
@section('content')
@include('admin.layouts.messages')
@php
    $route  = \Route::currentRouteName(); 
    if(isset($result)){
        $report = json_decode($result->data, true);
    }
 
@endphp
<div class="agile-grids @if($view == 0) form-side-nitview @endif">   
    <div class="grids">       
        <div class="row">
            <div class="col-md-12">
                                          
                <div id="form-side"> 
                <div class="master_side"> 	
                <div class="panel panel-widget forms-panel">
                    <div class="forms">
                        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
                            <div class="form-body">
                            @if($view == 0)    
                            <form method="POST" class="view-report" action="{{ route('view-report') }}">
                                {{ csrf_field() }}
                                <label>Enter Password To View Your Report</label>
                                <input type="password" name="password" required="true">
                                @if(session()->has('wrong_password'))
                                   <p style="color: #f00; padding: 8px 0px; font-size: 14px;">Invalid Password</p>
                                @endif
                                <input type="hidden" name="id" value="{{ $result->id }}">
                                <input type="submit" value="Submit"> 
                            </form>
                            @else

                            <h1>PPC Audit Checklist For <span id="show_account_name"> @if(isset($result)) {{ $report['account_name'] }} @endif</span></h1>

                                 
							<div id="introduction">
							<h2>Introduction:</h2>
							<div class="image-content">
							<figure>
							<a href="https://ps-attachments.s3.amazonaws.com/21873709-0f74-41dc-9a07-06e258d2927e/oQ_98oanuVDSOhAjhSpJtA.png" alt="Introduction:" target="_blank"> <img src="{{ url('/') }}/images/PPC-audit-Checklist.jpg"> </a>
							</figure>
							</div>
							<div class="text-content">
							<p>A comprehensive <strong>PPC audit checklist</strong> takes countless hours of testing and iterating to properly create.</p>
							<p>Most existing examples are too specific to adapt for yourself, and all of that effort could be in vain if you haven't accounted for how complicated SERPs are. That's why we've created this PPC Audit Checklist. This is an <strong>in-depth&nbsp;framework</strong> that covers the core focus areas that should be reviewed for each new campaign or after 3 months of data has been collected.&nbsp;</p>
							<p>This checklist will walk through setting up goals, ensuring campaign settings are optimized, allocating budget, reviewing keywords, choosing proper ad extensions, and much more. Before moving into this audit keep the following quote in mind.</p>
							<p class="style-blockquote no_bottom_space"><span>Nobody counts the number of ads you run; they just remember the impression you make. -&nbsp;<a href="https://blog.hubspot.com/agency/bill-bernbach-quotes" rel="nofollow" target="_blank">Bill Bernbach</a></span></p>
							</div>
							</div>

                            <div id="goals">
							<h2>Goals:</h2>
							</div>  

							<div id="input-basic-information">
                            <h2>Input basic information</h2>                      
							<div class="text-content" style="margin-bottom: 40px;">
							<p>The first step is to record basic information about the account being audited. These details will be useful for automating basic tasks later on in the process.</p>
							<p class="style-info"><span>If you haven't already, you can&nbsp;<strong>set up&nbsp;</strong><strong><a href="https://www.process.st/business-process-automation-guide/" rel="nofollow" target="_blank">Zapier</a></strong><strong>&nbsp;to launch instances of this checklist template&nbsp;</strong>in the future&nbsp;and push information you already have stored in other tools into any task's&nbsp;form fields automatically, based on a trigger like changing a lead's status from "qualified" to "customer".</span></p>
							<p><strong>Fill out the form fields</strong> below.</p>
							</div>
                             <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                            <label>Date audit started</label>
                                            <input type="date" readonly="" @if(isset($result)) value="{{ $report['date_audit_started'] }}" @endif name="date_audit_started" required="true">
                                          
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Account Name</label>
                                            <input type="text" readonly="" @if(isset($result)) value="{{ $report['account_name'] }}" @endif name="account_name" id="account_name" required="true">
                                      
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Primary contact first name</label>
                                            <input type="text" readonly="" @if(isset($result)) value="{{ $report['primary_contact_first_name'] }}" @endif name="primary_contact_first_name" required="true">  
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Primary contact last name</label>
                                            <input type="text" readonly="" @if(isset($result)) value="{{ $report['primary_contact_last_name'] }}" @endif name="primary_contact_last_name" required="true">
                                        </div> 
                                    </div>
                            </div>
                            </div>

                            <div id="determine-account-goals">
							<h2>Determine account goals</h2>
							<div class="text-content">
							<p>Whether you are putting this together for a client or are part of an internal&nbsp;team, you will want to think about what the goals are for the account.</p>
							<ul>
							<li>Was the account setup based off of old KPI's?</li>
							<li>What are expected conversion rates?</li>
							<li>Has the target market changed?</li>
							</ul>
							<p><strong>Fill out the form fields</strong> below.</p>
							</div> 
                                <div class="row">
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Account Goals</label>
                                            <textarea readonly="" name="account_goals" required="true"> @if(isset($result)) {{ $report['account_goals'] }} @endif</textarea>
                                        </div> 
                                    </div>
                                    <div class="col-md-10" style="margin-top: 20px;">
                                        <div class="form-group"> 
                                            <label>Account KPI's</label>
                                            <textarea readonly="" name="account_kpi" required="true">@if(isset($result)) {{ $report['account_kpi'] }} @endif</textarea> 
                                        </div> 
                                    </div>
                                </div>
                            <div class="text-content" style="margin-top: 15px;">
								<p>Using the Members form field below,&nbsp;<strong>assign the individual who will approve the account goals.</strong></p>
							</div>    
							</div>
                                    
				<!-- 			<div id="approval-account-goals">
							<h2>Approval: Account goals</h2>
							<div class="approval-content">
							<div class="header">
							<div class="list-title">
							Will be submitted for approval:
							</div>
							</div>
							</div>
							</div> -->

							<div id="choose-data-sources">
							<h2>Choose data sources</h2>
							<div class="text-content">
							<p><span><strong>List the data sources</strong> you will be using for this audit and how they will be accessed. When conducting an audit it is important to make sure your data is as accurate as possible. </span></p>
							<p><span><span style="background-color: #e5f5fb;"><strong>PRO TIP:</strong>&nbsp;Downloading the data from Google, Bing, etc... and incorporating it into an Excel spreadsheet can make it easier to analyze and visualize the data. This also allows you to show the data&nbsp;to clients and your manager to support your recommendations.&nbsp;</span></span></p>
							<p><strong>Fill in the below form field </strong>with the data sources you will be using. You should also upload any data you are analyzing to keep track of it.&nbsp;</p>
							</div>
                            <div class="row">
                                <div class="col-md-10" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Data sources</label>
                                        <textarea name="data_sources" readonly="" required="true">@if(isset($result)) {{ $report['data_sources'] }} @endif</textarea>
                                    </div> 
                                </div>
                                <div class="col-md-10" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Audit data</label>
                                        <a href="{{ url('/') }}{{  $report['audit_data'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Audit Data</a>
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="select-the-time-frame">
							<h2>Select the time frame</h2>
							<div class="text-content">
							<p><strong>Select the time frame of this audit.</strong></p>
							<p>The time frame being analyzed will greatly impact the insights that you gain. We typically recommend that you have at least 3 months of data. <span style="background-color: #e5f5fb;"></span></p>
							<p><strong>Fill out the form fields</strong> below.</p>
							</div>
							<div class="row">
                                <div class="col-md-3" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>Start date</label>
                                        <input type="date" readonly="" @if(isset($result)) value="{{ $report['start_date'] }}" @endif name="start_date" required="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <label>End date</label>
                                        <input readonly="" type="date" @if(isset($result)) value="{{ $report['end_date'] }}" @endif name="end_date" required="true">
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="tracking">
							<h2>Tracking:</h2>
							</div>

							<div id="verify-adwords-tracking-code-is-added-to-your-site">
							<h2>Verify AdWords tracking code is added to your site</h2>
							<div class="text-content">
							<p>Check that your <strong>AdWords tracking code is properly added</strong> to your site.</p>
							<p>To verify the conversion tag you have added on the page is correct you can use Google Tag Assistant. To do this just follow these steps:</p>
							<ul>
							<li>Install Google Tag Assistant</li>
							<li>Open your thank you page in a new tab</li>
							<li>Click on the tag assistant icon</li>
							<li>Enable it and reload your page</li>
							</ul>
							<p>You will then see the list of tags that are being fired on your page. This will include your conversion tag.</p>
							</div>
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <img src="{!! asset($report['google_tag_assistant']) !!}" alt="Google Tag Assistant"> 
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="ensure-conversion-actions-have-been-defined-appropriately">
							<h2>Ensure conversion actions have been defined appropriately</h2>
							<div class="text-content">
							<p><strong>Review current conversion actions</strong> and ensure they are relevant.</p>
							<p>A conversion action is a specific action that you have defined as being valuable to your business. Examples of this could be an online purchase or a phone call. You can setup conversion actions for different sources. Some of these sources could include website actions, an in-app action, an app download, and calls.&nbsp;</p>
							<p>If you need to track multiple instances of the same kind of conversion, such as a form submission, you will need to create multiple conversion actions.&nbsp;</p>
							</div>
							<div class="row">
                                <div class="col-md-12" style="margin-top: 20px;">
                                    <div class="form-group"> 
                                        <img src="{!! asset($report['conversion_actions']) !!}" alt="Google Tag Assistant"> 
                                    </div> 
                                </div>
                            </div>
							</div>

							<div id="campaign-settings">
							<h2>Campaign settings:</h2>
							</div>


<div id="verify-the-delivery-method-selected">
<h2>Verify the ‘Delivery Method’ selected</h2>
<div class="text-content">
<p><strong>Verify the delivery method being used.</strong></p>
<p>The delivery method that you choose will determine how long your budget will last. The default choice for most accounts is "standard" delivery. This is because Google will optimize the delivery of your ads and spread them out throughout the day. You may miss out on some searches but it ensure that your budget will last.</p>
<p>If you choose "accelerated" your budget will most likely get fully utilized. Google doesn't optimize the delivery as much as they do with "standard". Additionally, the funds are usually heavily used for morning ad placements. This can cause your ads not&nbsp;to be shown in the afternoon.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Delivery method</label>
<input type="text" readonly="" @if(isset($result)) value="{{ $report['delivery_method'] }}" @endif name="delivery_method" required="true">
</div> 
</div>
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['google_ads']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="verify-the-ad-rotation-option-selected">
<h2>Verify the ‘Ad Rotation’ option selected</h2>
<div class="text-content">
<p><strong>Verify that the correct option</strong> is selected for ad rotation.&nbsp;</p>
<p>Ad rotation is the method that Google Ads are delivered on both the Search and Display Networks. If you have more than one ad in your ad group then they will be forced to rotate. Google doesn't allow more than one ad from your account to show at a time.&nbsp;</p>
<p>To review your ad rotation settings you will need to ensure that "all features" are enabled. From the ad rotation section, you can choose the frequency that your ads will be served relative to each other. Please be aware that ad rotation settings are not available&nbsp;in a Smart Display campaign.&nbsp;</p>
<p>There are two primary options. You can choose "optimize" or "rotate indefinitely."</p>
<p>Below are instructions as provided by Google.</p>
<h5>Set ad rotation for your campaign</h5>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow">Google Ads</a><span>&nbsp;</span>account.</li>
<li>Click<span>&nbsp;</span><strong>All campaigns<span>&nbsp;</span></strong>in the navigation panel.</li>
<li>Click the campaign you’d like to change.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu.</li>
<li>Click<span>&nbsp;</span><strong>Additional settings</strong>.</li>
<li>Choose<span>&nbsp;</span><strong>Ad rotation</strong>.</li>
<li>Choose an ad rotation method:<span>&nbsp;</span><strong>Optimize</strong><span>&nbsp;</span>or<span>&nbsp;</span><strong>Rotate indefinitely</strong>.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
<h5>Set ad rotation for multiple campaigns at once</h5>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow">Google Ads</a><span>&nbsp;</span>account.</li>
<li>Click<span>&nbsp;</span><strong>All campaigns</strong><span>&nbsp;</span>in the navigation panel.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu.</li>
<li>Check the box next to the campaigns or ad groups you want to change.</li>
<li>Click<span>&nbsp;</span><strong>Edit</strong>.</li>
<li>In the drop-down menu, select<span>&nbsp;</span><strong>Change ad rotation</strong>.</li>
<li>Choose an ad rotation method.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
<h5>Set ad rotation by ad group</h5>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow">Google Ads</a><span>&nbsp;</span>account.</li>
<li>Click<span>&nbsp;</span><strong>Ad groups</strong><span>&nbsp;</span>in the page menu.</li>
<li>Click the ad group that you’d like to change.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu.</li>
<li>Click<span>&nbsp;</span><strong>Ad rotation</strong><span>&nbsp;</span>and then select a method.</li>
</ol>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['new_campaign']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="verify-your-ad-schedule-settings">
<h2>Verify your Ad Schedule settings</h2>
<div class="text-content">
<p><strong>Choose a relevant ad schedule.</strong></p>
<p><span>If you have specific times that you would like your ads to show then this is the section you should modify. However, if no one searches for your specific keyword during the time that you have chosen then your ad will not appear. You can also use this section to increase or decrease your spend on specific days and times.</span><span></span></p>
<p><span>By default, Google ads campaigns are set to display "all day."</span></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['schedule_settings']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>


<div id="check-if-all-features-has-been-selected-for-your-campaign">
<h2>Check if ‘All Features’ has been selected for your campaign</h2>
<div class="text-content">
<p><strong>Always make sure "All features"&nbsp;</strong>has been selected for your Search campaign. This makes sure that any advanced options will be accessible for that campaign. Not having this selected will limit features and could impact ROI.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['all_features']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="verify-if-you-want-to-opt-for-search-partners-from-search-network">
<h2>Verify if you want to opt for ‘Search partners’ from Search Network</h2>
<div class="text-content">
<p><strong>Confirm search partners being used.</strong></p>
<p>If you have a campaign that will be using the Search Network it will automatically include all search partners. This is a default option.&nbsp;</p>
<p>It is also important to note that Google Ads will not provide partner-level reports on ad performance that utilize search partner sites.&nbsp;</p>
<p>Follow these steps to review your current settings:&nbsp;</p>
<ol>
<li>Sign into your<span>&nbsp;</span><a href="http://ads.google.com/" rel="nofollow" target="_blank">Google Ads account</a>.&nbsp;</li>
<li>In the page menu, click<span>&nbsp;</span><strong>Settings</strong>, then click the campaign you want to include or remove search partners to.</li>
<li>Click<span>&nbsp;</span><strong>Networks</strong>.</li>
<li>Check the box next to "Include Google search partners" to enable ads from this campaign to appear on search partner websites, or uncheck it to disable this campaign’s ads from showing on search partner websites.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['search_partners']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="check-the-number-of-ad-variations-per-ad-group">
<h2>Check the number of ad variations per ad group</h2>
<div class="text-content">
<p><strong>Review the number of ad variations per ad group.</strong></p>
<p><span>If you are conducting a search network campaign then Google recommends creating at least </span><strong>2-3 ads</strong><span>&nbsp;per ad group.&nbsp; Another good rule of thumb is to review the number of impressions your ad group will receive and make sure it is in line with what you need to hit your conversion goals.</span></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['ad_variations']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="determine-if-dynamic-keyword-insertion-dki-should-be-used">
<h2>Determine if dynamic keyword insertion (DKI) should be used</h2>
<div class="text-content">
<p><strong>Decide if dynamic keyword insertion (DKI) will be used.</strong></p>
<p><strong>Dynamic keyword insertion</strong> allows you to create ads that serve up more relevant and personalized ads based off of the searchers' keyword. This feature is available on Google Ads and other ad networks. For a more in-depth&nbsp;understanding of DKI, we would highly recommend <a href="https://www.wordstream.com/dynamic-keyword-insertion&nbsp;" rel="nofollow" target="_blank">this guide</a> put together by WordStream.&nbsp;&nbsp;</p>
<p></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['dynamic_search_ads']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Will DKI be used?</label>
<input type="text" name="" readonly="" value="{{ $report['will_dki'] }}" required="true"> 
</div> 
</div>
</div>
</div>

<div id="budget--bidding">
<h2>Budget &amp; bidding:</h2>
</div>

<div id="check-that-budget-is-sufficient">
<h2>Check that budget is sufficient</h2>
<div class="text-content">
<p><strong>Check the current&nbsp;</strong><strong>budget and make sure it is appropriate.</strong></p>
<p><span>Within Google Ads you can set a daily budget for each campaign. Your budget should be based on your traffic or conversion goals. Make sure that you review this to ensure you have sufficient budget to hit your metrics.&nbsp;</span></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['current_budget']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>


<div id="validate-your-bidding-strategy">
<h2>Validate your bidding strategy</h2>
<div class="text-content">
<p><strong>Validate the bidding strategy.</strong></p>
<p><span>It is important to have a solid bid strategy behind your effort. You can choose a Manual CPC strategy or utilize one of the automated strategies that Google Ads has created. Make sure that you have taken the time to understand all the bid strategies available. That way you can determine which strategy is best for hitting your&nbsp;KPI's.&nbsp;</span></p>
<p><span>If goals do change you can always update your strategy by following these steps:</span></p>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow" target="_blank">Google Ads account</a>.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Campaigns</strong>.</li>
<li>Select the campaign you want to edit.</li>
<li>Click<span>&nbsp;</span><strong>Settings</strong><span>&nbsp;</span>in the page menu for this campaign.</li>
<li>Open<span>&nbsp;</span><strong>Bidding</strong><span>&nbsp;</span>and then click<span>&nbsp;</span><strong>Change bid strategy</strong>.</li>
<li>Select your new bid strategy from the drop-down menu.</li>
<li>Click<span>&nbsp;</span><strong>Save</strong>.</li>
</ol>
<p>Google also put together a great article on how to determine your bid strategy. It can be seen by <a href="https://support.google.com/google-ads/answer/2472725" rel="nofollow" target="_blank">clicking here</a>.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Bidding strategy</label>
<textarea rows="3" readonly="" name="bidding_strategy" class="form-control" required="true">@if(isset($result)){{ $report['bidding_strategy'] }} @endif</textarea>
</div> 
</div>
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['bidding_strategy_image']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
<div class="text-content">
<p>If you would like to learn more or need a refresher on bid strategies we have included a very informative video below.&nbsp;</p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/_i7qWiDs4AA?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
</div>

<div id="verify-bid-adjustments">
<h2>Verify bid adjustments</h2>
<div class="text-content">
<p><strong>Review your current bids per interaction</strong>. Then determine if adjustments need to be made.</p>
<p>Sometimes the source or type of interaction from of a click can be worth more than others. Bid adjustments allow you to specify if you would like to increase spend to get more exposure based on certain types of interactions. Examples of the choices you have include time of day, location, device type, etc...</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['bid_adjustments']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="targeting">
<h2>Targeting:</h2>
</div>

<div id="check-location-targeting">
<h2>Check location targeting</h2>
<div class="text-content">
<p><strong>Make sure the locations chosen are correct.&nbsp;&nbsp;</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Locations</label>
<textarea  rows="3" readonly="" name="locations" required="true" class="form-control">@if(isset($result)) {{ $report['locations'] }} @endif</textarea>
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p>You want to make sure that you are targeting customers that you feel will convert the best. If your ideal customer is only in English speaking countries then it wouldn't make any sense to have your ads running in non-English speaking countries. This would be wasted spend and artificially inflate your customer acquisition costs.</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['location_targeting']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="set-proper-device-performance-and-targeting">
<h2>Set proper device performance and targeting</h2>
<div class="text-content">
<p><strong>Make sure that targeting is properly selected.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Targeting selected</label>
<input type="text" readonly="" @if(isset($result)) value="{{ $report['targeting_selected'] }}" @endif name="targeting_selected" required="true">
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p><span>For both display and video campaigns you have the ability to target specific operating systems, ad inventory, device types, wireless networks, and more. Make sure you have fine tuned these to show up to the right people at the right time on the right device.&nbsp;</span></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['targeting_img']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="ensure-proper-demographic-specifications-if-any">
<h2>Ensure proper demographic specifications, if any</h2>
<div class="text-content">
<p><strong>Select demographic information for your target audience.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Demographic details</label>
<textarea  rows="3" name="demographic_details" readonly="" required="true" class="form-control">@if(isset($result)) {{ $report['demographic_details'] }} @endif</textarea>
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p>If you have a specific demographic that you are targeting then you will want to make sure you have that setup properly. Follow the below steps to create demographic groups:</p>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="https://ads.google.com/" rel="nofollow" target="_blank">Google Ads account</a>.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Campaigns</strong>.</li>
<li>Click the name of the campaign to which you want to add demographic targeting. You can add demographic categories to Search, Display, and Video campaigns.</li>
<li>Click the name of the ad group to which you want to add demographic targeting.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Demographics</strong>.</li>
<li>Click the demographic you want to target along the top of the page:
<ul>
<li><strong>Gender</strong></li>
<li><strong>Age</strong></li>
<li><strong>Parental status</strong><span>&nbsp;</span>(Display and Video campaigns only)</li>
<li><strong>Household income</strong><span>&nbsp;</span>(currently available in the U.S., Japan, Australia, and New Zealand only)</li>
</ul> </li>
<li>Check the box next to the specific demographic groups you want to target.</li>
<li>Click the<span>&nbsp;</span><strong>Edit</strong><span>&nbsp;</span>drop-down in the blue banner that appears along the top of the table, then select<span>&nbsp;</span><strong>Enable</strong>. To exclude demographic groups, select<span>&nbsp;</span><strong>Exclude from ad group</strong>.</li>
</ol>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['semographic_specifications']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="check-the-display-ad-placements-for-inappropriate-websites">
<h2>Check the display ad placements for inappropriate websites</h2>
<div class="text-content">
<p><strong>Make sure inappropriate websites are on the exclusion list.&nbsp;&nbsp;</strong></p>
<p>If you have a list of websites or know of types of sites that you don't want your ad showing up on then you should include this in the negative site list. This is important from a branding standpoint. If your ad appears on a site then the common online user will associate your ad with the site they are viewing. Below are the step-by-step instructions provided by Google:</p>
<p></p>
<h5>Exclude individual sites from ad groups or campaigns</h5>
<ol>
<li>Select<span>&nbsp;</span><strong>Keywords and Targeting</strong><span>&nbsp;</span>&gt;<span>&nbsp;</span><strong>Placements, Negative</strong><span>&nbsp;</span>in the type list.</li>
<li>Click<span>&nbsp;</span><strong>Add negative</strong><span>&nbsp;</span>and select the type of negative site (campaign or ad group).</li>
<li>If prompted, select the destination for the new negative site and click<span>&nbsp;</span><strong>OK</strong>.</li>
<li>Enter the negative site in the edit panel.</li>
</ol>
<h5>Add multiple negative sites</h5>
<ol>
<li>Select<span>&nbsp;</span><strong>Keywords and Targeting</strong><span>&nbsp;</span>&gt;<span>&nbsp;</span><strong>Placements, Negative</strong><span>&nbsp;</span>in the type list.</li>
<li>Click<span>&nbsp;</span><strong>Make multiple changes</strong>.</li>
<li>Under "Destination,” select<span>&nbsp;</span><strong>My data includes columns for campaigns and/or ad groups</strong><span>&nbsp;</span>or<span>&nbsp;</span><strong>Use selected destinations</strong>.</li>
<li>Type or paste your changes into the grid.</li>
<li>Click<span>&nbsp;</span><strong>Process</strong>.</li>
<li>To continue, click<span>&nbsp;</span><strong>Finish and review changes</strong>.</li>
<li>To add the pending changes to your account, click<span>&nbsp;</span><strong>Keep</strong>.</li>
</ol>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Exclusion list</label>
<textarea name="exclusion_list" rows="3" readonly="" required="true">@if(isset($result)) {{ $report['exclusion_list'] }} @endif</textarea>
</div> 
</div>
</div>
</div>

<div id="ads">
<h2>Ads:</h2>
</div>

<div id="verify-ad-guidelines-are-followed">
<h2>Verify ad guidelines are followed</h2>
<div class="text-content">
<p><strong>Verify ad guidelines are being followed.</strong></p>
<p>Each company should have branding guidelines and style guides created that should be followed. Ensure that all ads are compliant with internal policies.</p>
<p>Additionally, Google has guidelines and an approval process that has to be adhered&nbsp;to. Please be sure to review<a href="https://support.google.com/adspolicy/answer/6008942" rel="nofollow" target="_blank">&nbsp;Google Ads policies</a>. &nbsp;</p>
<p>Typically any ad created will go through a standard review process to ensure quality and compliance. Most ads are approved within 1 business day. If it hasn't been reviewed or approved by the end of 2 business days then you need to contact them.&nbsp;</p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="380" src="https://www.youtube.com/embed/Dx0ukoJPO4Y?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
</div>

<div id="use-keyword-in-ad-copy">
<h2>Use keyword in ad copy</h2>
<div class="text-content">
<p><strong>Use keywords in the ad copy.</strong></p>
<p><span>Ads are driven by keywords. If your keyword is not in your ad then your ad will not perform or show up very much. Your quality score will also be very low due to irrelevance. This is because Google wants to show ads that are in line with user search intent.&nbsp;</span></p>
<p><span>One good strategy is to use Single Keyword Ad Groups. This is also lovingly called "SKAGs." The strategy is exactly what the name implies. Your ad should have just one keyword associated with each ad group. This allows you to ensure that your keywords are directly in line with the search terms you're trying to convert on.&nbsp;</span></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['search_result']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keyword list</label>
<a href="{{ url('/') }}{{  $report['keyword_list'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Keyword list</a>
</div> 
</div>
</div>
</div>

<div id="ensure-the-landing-page-is-appropriate">
<h2>Ensure the landing page is appropriate</h2>
<div class="text-content">
<p><strong>Make sure your landing pages are following best practices.&nbsp;&nbsp;</strong></p>
<p>Unbounce put together a great best practices article that can be seen <a href="https://unbounce.com/landing-pages/checklist/" rel="nofollow" target="_blank">here</a>. Below are eight core items that should be checked for each landing page.</p>
</div>
<div class="multi-select-content form-field-content">
<ul class="items">
<li class="item">
<div class="item-name-static">
Does your landing page headline match the message on your ads?
</div> </li>
<li class="item">
<div class="item-name-static">
Does your page message have the clarity of a 30-second elevator pitch?
</div> </li>
<li class="item">
<div class="item-name-static">
Does the writing focus primarily on benefits rather than features?
</div> </li>
<li class="item">
<div class="item-name-static">
Are you using a relevant and original main image or video that shows it being used?
</div> </li>
<li class="item">
<div class="item-name-static">
Do you make it clear what the visitor will receive by clicking your CTA?
</div> </li>
<li class="item">
<div class="item-name-static">
Have you optimized your landing page to get a paid search quality score above 7?
</div> </li>
<li class="item">
<div class="item-name-static">
ave you removed extraneous links (like the global nav) – to remove page leaks?
</div> </li>
<li class="item">
<div class="item-name-static">
Do you have clear goals for this landing page and campaign?
</div> </li>
</ul>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['home_vs_landing']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="check-for-any-offer-or-timebound-messaging">
<h2>Check for any offer or time-bound messaging</h2>
<div class="text-content">
<p><strong>Make sure time-bound offers have the correct runtimes.</strong>&nbsp;</p>
<p>If your organization runs special campaigns for discounts, holidays, or special events it is important to keep track of the dates these offers are valid. In the form field below list, any offers that have run over the audit analysis period.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Time bound offer details</label>
<textarea  rows="3" name="time_bound_offer_details" readonly="" required="true" class="form-control">@if(isset($result)) {{ $report['time_bound_offer_details'] }} @endif</textarea>
</div> 
</div>
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['new_offer']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>


<div id="check-your-ads-for-grammatical-or-spelling-mistakes">
<h2>Check your ads for grammatical or spelling mistakes</h2>
<div class="text-content">
<p><strong>Review ads for spelling or grammar mistakes. </strong></p>
<p>Be sure to triple&nbsp;check for any grammatical errors. A great tool to use is <a href="https://www.process.st/how-to-write-a-proposal/" rel="nofollow" target="_blank">Grammarly</a>. You might even want to ask a colleague to take a look. It never hurts to have a fresh pair of eyes.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['grammatical_or_spelling']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="make-sure-ad-has-clear-calltoaction">
<h2>Make sure ad has clear call-to-action</h2>
<div class="text-content">
<p><strong>Ensure each&nbsp;ad</strong><strong>&nbsp;has a clear call-to-action.</strong></p>
<p>The newspaper industry has a great saying, "<strong>never bury the lead</strong>." This is extremely relevant when writing your ad. Make sure you clearly state why people should care to read your offer. Then you need to encourage them to take an action.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['call_to_action']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="prepare-ab-test-review">
<h2>Prepare A/B test review</h2>
<div class="text-content">
<p><strong>Review current A/B tests</strong> that are running in the account. Are there any insights or issues?</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>A/B test insights</label>
<textarea  rows="3" name="a_b_test_insights" readonly="" required="true" class="form-control">@if(isset($result)) {{ $report['a_b_test_insights'] }} @endif</textarea>
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p>If the account isn't running any A/B tests then that is something you should plan on doing. There is always something that can be improved. Be sure that you setup a control and a variant so that you can accurately determine what is working and what isn't.</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['test_insights']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="verify-display-campaigns-are-using-both-image-and-text-ads">
<h2>Verify display campaigns are using both image and text ads</h2>
<div class="text-content">
<p><strong>Verify display campaigns are using text and images.&nbsp;</strong></p>
<p>The more real estate your ads can take the better. Having both text and image ads will increase your ads distribution through the display network.</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['display_campaigns']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>

<div id="keywords">
<h2>Keywords:</h2>
</div>

<div id="check-your-search-terms-for-negative-keywords">
<h2>Check your search terms for negative keywords</h2>
<div class="text-content">
<p>Look over the campaign keywords and make sure negative keywords are being used.&nbsp;</p>
<p>Choosing the proper keyword to associate with your ads will determine success or failure in your PPC efforts. If you need a refresher on conducting keyword research we have put together an in-depth <span style="color: #007db6;"><strong><a href="https://www.process.st/checklist/seo-checklist-the-keyword-research-process/" rel="nofollow" target="_blank">Keyword Research Checklist</a></strong></span>. You can easily apply it to your Process Street account with one simple click.&nbsp;&nbsp;</p>
</div>

<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['search_terms']) !!}" alt="Search Terms"> 
</div> 
</div>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/j8x197-c2X0?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
<div class="text-content">
<p>Record negative keywords being used.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Negative keywords</label>
<textarea  rows="3" name="negative_keywords" readonly="" required="true" class="form-control">@if(isset($result)) {{ $report['negative_keywords'] }} @endif</textarea>
</div> 
</div>
</div>
</div>


<div id="check-keyword-match-types">
<h2>Check keyword match types</h2>
<div class="text-content">
<p><strong>Review the keyword match types</strong> that are associated with each ad. Then determine if they are correct. You can access the Keywords list by clicking on the Keywords tab in Google Ads.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keyword match types</label>
<textarea  rows="3" name="keyword_match_types" readonly="" required="true" class="form-control">@if(isset($result)) {{ $report['keyword_match_types'] }} @endif</textarea>
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 15px;">
<p><span>Keyword match types will allow you to control the type of searches that will trigger your ad to appear. If yous goal is impressions and you&nbsp;want to get more eyes on your ad you could utilize broad match types. However, if you have a keyword that you know converts very well but is more targeted then you will want to use an exact match type.&nbsp;</span></p>
<p><span>Below are some additional resources:</span></p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/XAvTbSAHpxA?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
<div class="description">
Learn how to use the different keyword match types with your Google AdWords search campaigns. You'll learn about broad, phrase, exact and negative keyword match types. Plus broad match modified keywords and close variants. - Benjamin Mangold from Loves Data
</div>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['match_type']) !!}" alt="Google Tag Assistant"> 
</div> 
</div>
</div>
</div>


<div id="ensure-there-are-no-keyword-conflicts">
<h2>Ensure there are no keyword conflicts</h2>
<div class="text-content">
<p><span>Review the account <strong>to ensure there are no keyword conflicts</strong> occurring.&nbsp;</span></p>
<p><span>If a keyword conflict is happening you will want to compare your keyword list versus your negative keyword list. When a conflict occurs your ads won't show, even when they are set to active,&nbsp;due to them being blocked. The first place to look for conflict would be in the search query data.</span></p>
<p>Below is an example tracking document that was developed by Google. You can get it <a href="https://developers.google.com/google-ads/scripts/docs/solutions/adsmanagerapp-negative-keyword-conflicts" rel="nofollow" target="_blank">here</a></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['negative_keyword_conflicts']) !!}" alt="Negative Keyword Conflicts"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Keyword conflict</label>
<input type="text" name="keyword_conflict" value="{{ $report['keyword_conflict'] }}" readonly="" required="true"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Conflict tracking document</label>
<a href="{{ url('/') }}{{  $report['conflict_tracking_document'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Conflict Tracking Document</a>
</div> 
</div>
</div>
</div>


<div id="identify-new-search-query-opportunities">
<h2>Identify new search query opportunities</h2>
<div class="text-content">
<p><strong>Brainstorm new keyword ideas.</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>New keyword ideas</label>
<textarea  rows="3" name="new_keyword_ideas" readonly="" required="true" class="form-control">@if(isset($result)) {{ $report['new_keyword_ideas'] }} @endif</textarea>
</div> 
</div>
</div>
<div class="text-content" style="margin-top: 20px;">
<p>There are countless ways to find inspiration for new keywords or search queries. Make sure you take some time to get into the mind of your ideal customer. Think about what keywords they would use to find your product. What "job" is your product&nbsp;solving for? Working through the JTBD (<a href="https://hbr.org/2016/09/know-your-customers-jobs-to-be-done" rel="nofollow" target="_blank">Jobs to Be Done</a>) framework can be extremely helpful.&nbsp;</p>
</div>
</div>

<div id="verify-proper-search-query-triggered-keyword-results">
<h2>Verify proper search query triggered keyword results</h2>
<div class="text-content">
<p><strong>Go to the Keyword tab of your campaign or adgroup&nbsp;</strong>and&nbsp;verify&nbsp;the which keywords are triggering your ads. You can do this by&nbsp;looking over the&nbsp;Search&nbsp;Query&nbsp;Report.&nbsp;</p>
<p>To access the Search Query report follow these steps:</p>
<ul>
<li>Log into your Google ads account</li>
<li>Click on the "Keywords" tab</li>
<li>Select the keywords you want more information on</li>
<li>Click the "Search Terms" button&nbsp;</li>
</ul>
<p>You will then be presented with a result like this:</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['verify_proper_search']) !!}" alt="Verify proper search"> 
</div> 
</div>
</div>
</div>

<div id="check-the-number-of-keywords-per-ad-group">
<h2>Check the number of keywords per ad group</h2>
<div class="text-content">
<p><strong>Check the number of keywords per ad group.</strong></p>
<p>There is no rule that dictates how many keywords you should have per ad group. You will need to determine this as you review your accounts. However, many argue that 20 should be the maximum. The thought behind this is that if you have more than 20 keywords then your ad copy is probably not as aligned with search intent as it could be. Your results will be too broad to get the best conversion.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['number_of_keywords']) !!}" alt="Number of keywords"> 
</div> 
</div>
</div>
</div>

<div id="remove-keywords-with-zero-impressions">
<h2>Remove keywords with zero impressions</h2>
<div class="text-content">
<p><strong>Remove any keywords that have zero impressions.</strong></p>
<p>If your keywords are getting zero impressions then they are just taking up space in your account. They are also distracting and can clutter your account.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['zero_impressions']) !!}" alt="Keywords with zero impressions"> 
</div> 
</div>
</div>
</div>

<div id="quality-score">
<h2>Quality score:</h2>
</div>

<div id="check-your-keywords-quality-score-performance">
<h2>Check your keyword's quality score performance</h2>
<div class="text-content" style="margin-bottom: 15px;">
<p><strong>Check&nbsp;keyword</strong><strong>&nbsp;quality score.</strong></p>
<p>To check the relevance of your ad in comparison to the keywords that are associated with it, you will want to look at the Quality Score.&nbsp;</p>
<p>The easiest way to check this is by following these steps:</p>
<ol>
<li>Sign in to your<span>&nbsp;</span><a href="http://ads.google.com/" rel="nofollow">Google Ads account</a>.</li>
<li>In the page menu on the left, click<span>&nbsp;</span><strong>Keywords.</strong>&nbsp;</li>
<li>Hover over a keyword’s status in the “Status” column. You'll be able to see ratings for<span>&nbsp;</span><strong>expected clickthrough rate</strong>,<span>&nbsp;</span><strong>ad relevance</strong>, and l<strong>anding page experience</strong>.</li>
</ol>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/1oHwl0tFHz8?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
<div class="description">
AdHawk also put together a short tutorial.
</div>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Ad quality score</label>
<input  type="text" name="ad_quality_score" readonly="" required="true" @if(isset($result)) value="{{ $report['ad_quality_score'] }}" @endif class="form-control">
</div> 
</div>
</div>
</div>

<div id="eliminate-keywords-with-poor-quality-score">
<h2>Eliminate keywords with poor quality score</h2>
<div class="text-content">
<p><strong>Eliminate any keywords with a quality score under 7.&nbsp;</strong></p>
<p>To remove poor performing keywords all you need to do is go to the Keywords section in the page menu and then click on&nbsp;<strong>Search Keywords&nbsp;</strong>or&nbsp;<strong>Display/Video keywords</strong>. Select the keyword you want to remove and choose&nbsp;<strong>Edit</strong> then&nbsp;<strong>Remove</strong>.</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['keywords_with_poor_quality']) !!}" alt="Keywords with poor quality"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Removed keywords</label>
<textarea  rows="3" name="removed_keywords" required="true" readonly="" class="form-control">@if(isset($result)) {{ $report['removed_keywords'] }} @endif</textarea>
</div> 
</div>
</div>
</div>

<div id="ad-extension">
<h2>Ad extension:</h2>
</div>

<div id="ensure-gmb-is-linked-to-your-ads-account-to-enable-location-extensions-if-applicable">
<h2>Ensure GMB is linked to your Ads account to enable location extensions (if applicable)</h2>
<div class="text-content">
<p><strong>Make sure location extensions are setup. This can be done by linking Google Ads with Google My Business. </strong></p>
<p><span>By connecting Google My Business (GMB) to your Google Ads account you will be able to enable location extensions. If your business doesn't utilize a physical location and only handles support through email then this might not be as important to you. However, if your a business focusing on a specific geographic area or want to make your contact information more easily accessible, then this would be good to setup.&nbsp;</span></p>
<p><span>Location extensions will attach your address, a map to your location, or even the distance to your business to the ad you are running. Users will then be able to click the additional information about your location for further details. This could direct them to your location page or even a call button.&nbsp;</span></p>
</div>
<div class="video-content">
<div class="iframe-container">
<iframe width="100%" height="400" src="https://www.youtube.com/embed/Tc8LXO8y_a8?modestbranding=1&amp;showinfo=0" frameborder="0" allowfullscreen="true"> </iframe>
</div>
</div>
</div>

<div id="check-for-campaigns-with-missing-ad-extensions">
<h2>Check for campaigns with missing ad extensions</h2>
<div class="text-content">
<p>Review your campaigns and make sure that <strong>all extensions</strong> that are relevant have been enabled. You can go to the Ads &amp; extensions tab to see which extensions are currently enabled.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['missing_ad_extensions']) !!}" alt="Missing ad extensions"> 
</div> 
</div>
</div>
</div>

<div id="remove-any-nonperforming-automated-extensions">
<h2>Remove any non-performing automated extensions</h2>
<div class="text-content">
<p><strong>Remove any extensions that are not converting.&nbsp;</strong></p>
<p>If an extension isn't bringing any value then it might be distracting from your ad. Be sure to review their performance.</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['non_performing_automated_extensions']) !!}" alt="Non-performing automated extensions"> 
</div> 
</div>
</div>
</div>

<div id="check-the-status-of-ad-extension-approved-or-disapproved">
<h2>Check the status of ad extension (approved or disapproved)</h2>
<div class="text-content">
<p><strong>Check the current approval status of each ad extension.</strong></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['current_approval_status']) !!}" alt="Current approval status"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Ad extensions approved</label>
<input type="text" name="ad_extensions_approved" value="{{ $report['ad_extensions_approved'] }}" readonly="" required="true"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Additional notes</label>
<textarea  rows="3" name="additional_notes" required="true" readonly="" class="form-control">@if(isset($result)) {{ $report['additional_notes'] }} @endif</textarea>
</div> 
</div>
</div>
</div>

<div id="ensure-sitelinks-have-relevant-descriptions">
<h2>Ensure sitelinks have relevant descriptions</h2>
<div class="text-content">
<p><strong>Make sure the sitelink</strong><strong>&nbsp;descriptions being used are supporting your main ad.</strong></p>
<p>To update or edit your sitelinks&nbsp;follow these steps:</p>
<ol>
<li>Click<span>&nbsp;</span><strong>Ads &amp; extensions</strong><span>&nbsp;</span>in the page menu on the left, then click<span>&nbsp;</span><strong>Extensions</strong><span>&nbsp;</span>at the top of the page.</li>
<li>Click the blue plus button,&nbsp;then click&nbsp;<strong>+ Sitelink extension.</strong></li>
<li>From the “Add to” drop-down menu, choose the level you’d like to add the sitelink to.&nbsp;</li>
<li>To use an existing sitelink, click<span>&nbsp;</span><strong>Use existing</strong><span>&nbsp;</span>and select the sitelinks that you’d like to add.</li>
<li>To create a new sitelink, click<span>&nbsp;</span><strong>Create new</strong>.</li>
<li>Fill out the sitelink text and URL.</li>
<li>Enter additional text about your link in the Description fields. When you fill out both lines of description, your sitelink may be eligible to show with these details.</li> 
<li>Click<span>&nbsp;</span><strong>Save</strong><span>&nbsp;</span>to save your sitelink setting for your campaign.</li>
</ol>
<p><span>The more space your ad can take on relevant search engine result pages the better! Be sure you aren't wasting that space with irrelevant descriptions that could decrease your conversion rate and increase your customer acquisition cost.</span></p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['ensure_sitelinks']) !!}" alt="Ensure sitelinks"> 
</div> 
</div>
</div>
</div>

<div id="make-sure-your-call-extension-is-scheduled-during-operational-hours">
<h2>Make sure your call extension is scheduled during operational hours</h2>
<div class="text-content">
<p>Look over your call extensions and <strong>make sure they are scheduled to only run during business hours</strong>.&nbsp;</p>
<p>Nothing is worse than a hot lead turning cold. Don't have call extensions running if no one can answer the phone. This is a simple fix that could dramatically impact your campaigns.&nbsp;&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['call_extension']) !!}" alt="Call extension"> 
</div> 
</div>
</div>
</div>

<div id="check-if-call-reporting-is-enabled-to-track-conversions-from-your-call-extensions">
<h2>Check if ‘call reporting’ is enabled to track conversions from your call extensions</h2>
<div class="text-content">
<p><strong>If you are using call extensions make sure that call reporting has been turned on.&nbsp;</strong></p>
<p>Please note that this is only available on the Search Network. Call reporting runs off of Google forwarding numbers. It lets you track the performance of your call extensions and call-only ads. This report will give you information on call duration, the start and stop time, the callers' area code, and if the call was successfully connected.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['call_reporting']) !!}" alt="Call reporting"> 
</div> 
</div>
</div>
</div>

<div id="remarketing-campaigns">
<h2>Remarketing campaigns:</h2>
</div>

<div id="validate-remarketing-code-has-been-added-correctly">
<h2>Validate remarketing code has been added correctly</h2>
<div class="text-content">
<p><strong>Validate that the remarketing code has been added correctly.</strong></p>
<p>Remarketing allows you to show ads to individuals who have already visited your website. You do this by putting snippets of code on your site. You will need to ensure that the global site tag and event snippets are properly setup on your website. The global site snippet works with Google's conversion tracking and remarketing products. This allows you to target your ads to relevant visitors through tracking cookies.&nbsp;</p>
<p><strong>NOTE: With GDPR and other privacy updates it is important to inform your site visitors about any data collection processes.</strong>&nbsp;</p>
<p>To create or edit your remarketing event snippets and global site tag please follow the below steps:</p>
<ul></ul>
<ol>
<li>Sign in to<span>&nbsp;</span><a href="https://adwords.google.com/" rel="nofollow">AdWords</a>.</li>
<li>Click on the tool icon<span>&nbsp;</span>&nbsp;in the upper right and click&nbsp;<strong>Audience Manager&nbsp;</strong>under the section labeled<span>&nbsp;</span><strong>“Shared library”</strong></li>
<li>On the left, click<span>&nbsp;</span><strong>Audience sources</strong>. This opens a group of sources from which you can create remarketing lists.</li>
<li>In the “AdWords tag” card, click<span>&nbsp;</span><strong>SET UP TAG</strong>.
<ul>
<li>If you’ve already set up a tag, select<span>&nbsp;</span><strong>Edit source</strong><span>&nbsp;</span>from the 3-dot icon in its upper right of the “AdWords tag” card. If you’re just getting the code, skip to step 6.</li>
</ul> </li>
<li>Select which type of data the tag will collect, standard data or specific attributes/parameters.
<ul>
<li>You can also choose to include the user ID parameter with the tag. This will collect the user IDs of people who visit your website so you can show more targeted ads.</li>
<li>If you select specific attributes or parameters, you can choose the business type that best represents your products and services.&nbsp;</li>
</ul> </li>
<li>Click<span>&nbsp;</span><strong>CREATE AND CONTINUE</strong>.
<ul>
<li>This button will say<span>&nbsp;</span><strong>SAVE AND CONTINUE</strong><span>&nbsp;</span>for an existing tag.</li>
</ul> </li>
<li>When the installation screen appears, your global site tag and event snippet will be ready for use. You can copy the code, use Tag Manager, download the tag, or email the tag to a webmaster.
<ul>
<li>To integrate remarketing into your site, copy the code and paste it between the &lt;head&gt;&lt;/head&gt; tags of the website.</li>
<li>The global site tag must be added into every page of your site. The event snippet only needs to be added into the specific pages you want to track for dynamic remarketing events.</li>
</ul> </li>
<li>Click<span>&nbsp;</span><strong>DONE</strong>.</li>
<li>In the following “What’s Next” confirmation screen, click<span>&nbsp;</span><strong>DONE</strong><span>&nbsp;</span>again.
<ul>
<li>Optional: If you previously added the global site tag from another Google product such as Google Analytics, make sure you add the 'config' command from the AdWords tag (highlighted below) to every existing instance of the tag. Note that "AW-123456789" is only an example of an ID. You'll need to replace this part of the 'config' command with your customer ID in the global site tag box.<br>&nbsp;&nbsp; &nbsp;&lt;script async src="https://www.googletagmanager.com/gtag/js?id=AW-123456789"&gt;&lt;/script&gt;<br>&nbsp;&nbsp; &nbsp;&lt;script&gt;<br>&nbsp;&nbsp; &nbsp;window.dataLayer = window.dataLayer || [];<br>&nbsp;&nbsp; &nbsp;function gtag(){dataLayer.push(arguments);}<br>&nbsp;&nbsp; &nbsp;gtag('js', new Date());<br>&nbsp;&nbsp;&nbsp; &nbsp;<br>&nbsp;&nbsp; &nbsp;gtag('config', 'AW-123456789');<br>&nbsp;&nbsp; &nbsp;&lt;/script&gt;</li>
<li>Optional: If you’re using an event snippet for remarketing and don’t want the global site tag to send an extra hit, add the highlighted portion below to your global site tag’s ‘config’ command.<br>&nbsp;&nbsp; &nbsp;gtag('config', 'AW-123456789',<span>&nbsp;</span>{'send_page_view': false});</li>
</ul> </li>
</ol>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['validate_remarketing_code']) !!}" alt="Validate remarketing code"> 
</div> 
</div>
</div>
</div>

<div id="validate-remarketing-lists-are-properly-collecting-visitors">
<h2>Validate remarketing lists are properly collecting visitors</h2>
<div class="text-content">
<p><strong>Make sure that your remarketing lists are properly collecting visitor data.</strong> If the data isn't showing impressions or you see a lot of zeros then you should ensure that remarketing snippets have been properly installed.</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['validate_remarketing_list']) !!}" alt="Validate remarketing lists"> 
</div> 
</div>
</div>
</div>

<div id="verify-the-remarketing-lists-created">
<h2>Verify the remarketing lists created</h2>
<div class="text-content">
<p>Be sure that the <strong>remarketing lists created are relevant to your ads</strong> and are properly named. Using a consistent naming convention will help you stay organized.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-12" style="margin-top: 20px;">
<div class="form-group"> 
<img src="{!! asset($report['verify_the_remarketing_lists']) !!}" alt="Verify the remarketing lists"> 
</div> 
</div>
</div>
</div>

<div id="metric-review">
<h2>Metric review:</h2>
</div>

<div id="review-primary-kpis">
<h2>Review primary KPI's</h2>
<div class="text-content">
<p><strong>Review primary KPI's for the account.</strong></p>
<p>There are many metrics that can be analyzed when conducting a PPC Audit. Each account is different and has specific goals created for them. Below we have listed a few common metrics that are reviewed.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>KPI's measured</label>
<input type="text" readonly="" value="{{ $report['kpis_measured'] }}" name="kpis_measured" required="true"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>KPI notes</label>
<textarea  rows="3" name="kpi_notes" required="true" class="form-control" readonly>@if(isset($result)) {{ $report['kpi_notes'] }} @endif</textarea>
</div> 
</div>
</div>
</div>

<div id="validate-budget-utilization">
<h2>Validate budget utilization</h2>
<div class="text-content">
<p><strong>Make sure the budget was properly utilized.</strong> You should include notes on why the budget was or wasn't fully used. This can be input in the form field below.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Was the campaign budget fully attained?</label>
<input name="campaign_budget" value="{{ $report['campaign_budget'] }}" readonly="" required="true"> 
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Budget notes</label>
<textarea  rows="3" name="budget_notes" required="true" class="form-control" readonly="">@if(isset($result)) {{ $report['budget_notes'] }} @endif</textarea>
</div> 
</div>
</div>
</div>

<div id="reportaction-items">
<h2>Report/Action items:</h2>
</div>

<div id="prioritize-action-items">
<h2>Prioritize action items</h2>
<div class="text-content">
<p>Now that you have completed the audit you should <strong>write down your major takeaways</strong> and prioritize action items.</p>
<p>The best place to start is to identify any concerns. List anything that you noticed was wrong or incorrectly setup. From the list of concerns, you should be able to identify quick wins. Some examples could be that the remarketing pixel isn't setup correctly or that incorrect demographic information had been chosen for an ad. These would be easy fixes that could make a significant impact.</p>
<p>After you have written out quick wins then you should put together a list of next steps. Think about what would make the most impact in the quickest amount of time.</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Concerns</label>
<textarea  rows="3" name="concerns" required="true" class="form-control" readonly="">@if(isset($result)) {{ $report['concerns'] }} @endif</textarea>
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Quick wins</label>
<textarea  rows="3" name="quick_wins" required="true" class="form-control" readonly="">@if(isset($result)) {{ $report['quick_wins'] }} @endif</textarea>
</div> 
</div>
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Next steps</label>
<textarea  rows="3" name="next_steps" required="true" class="form-control" readonly="">@if(isset($result)) {{ $report['next_steps'] }} @endif</textarea>
</div> 
</div>
</div>
</div>

               
<div id="create-a-report-if-needed">
<h2>Create a report (if needed)</h2>
<div class="text-content">
<p><strong>Decide if a report needs to be made.</strong>&nbsp;If you do need to put together a&nbsp;client facing report then it should, at a minimum, include the following:</p>
<ul>
<li>Areas of concern</li>
<li>Quick wins</li>
<li>Action items</li>
</ul>
<p>Every organization has a different structure or tool utilized to create a PPC audit report.&nbsp; If you are unsure of what tool you should use we would recommend reviewing this article by <a href="https://www.wordstream.com/blog/ws/2018/02/09/best-reporting-tools" rel="nofollow" target="_blank">Wordstream</a>.&nbsp;</p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Report created</label>
<input type="text" value="{{ $report['report_created'] }}" readonly="">
</div> 
</div>
</div>
<div class="text-content" style="margin-top:15px;">
<p>Using the Members form field below,&nbsp;<strong>assign the individual who will approve the audit report.</strong></p>
</div>
</div>


<div id="prepare-report-for-review">
<h2>Prepare report for review</h2>
<div class="text-content">
<p>After your report is complete you will want to email it or print it.&nbsp;</p>
<p class="style-info"><span><strong>PRO TIP: A great way to merge your data from Process Street with your report is through our integration with <a href="https://www.process.st/help/docs/webmerge/" rel="nofollow" target="_blank">WebMerge.</a>&nbsp;</strong></span></p>
<p>To send the email you can&nbsp;use the <a href="https://www.process.st/help/docs/using-email-widget/" rel="nofollow">email widget</a> below. All you need to do is customize the message and upload your file.&nbsp;</p>
<p>Then just <strong>make sure everything looks okay and hit "Send".</strong></p>
</div>
<div class="row">
<div class="col-md-10" style="margin-top: 20px;">
<div class="form-group"> 
<label>Report</label>
<a href="{{ url('/') }}{{  $report['report'] }}" class="download_anchor"><i class="fa fa-download"></i> &nbsp; Report</a>

</div> 
</div>
</div>
</div>

<!-- <div id="approval-audit-report">
<h2>Approval: Audit report</h2>
<div class="approval-content">
<div class="header">
<div class="list-title">
Will be submitted for approval:
</div>
</div>
</div>
</div> -->

<div id="sources">
<h2>Sources:</h2>
<div class="text-content">
<ul>
<li><a href="https://adespresso.com/blog/google-ads-negative-keywords/" rel="nofollow" target="_blank">Ad Espresso - Google Ads Negative Keywords List</a></li>
<li><a href="https://www.wordstream.com/allen-finn-3540" rel="nofollow" target="_blank">Allen Finn</a> - <a href="https://www.wordstream.com/blog/ws/2018/02/13/adwords-ad-variations" rel="nofollow" target="_blank">10X Your A/B Testing with Google Ads Variations</a></li>
<li><a href="https://www.pigzilla.co/author/dani-owens/" rel="nofollow" target="_blank">Dani Owens</a> -<a href="https://www.pigzilla.co/blog/local-conversion-actions-in-google-ads/" rel="nofollow" target="_blank"> Local Conversion Actions in Google Ads</a></li>
<li><a href="https://www.semrush.com/user/145674065/" rel="nofollow" target="_blank">Emma McHale</a> - <a href="https://www.semrush.com/blog/how-to-conduct-a-complete-ppc-audit/" rel="nofollow" target="_blank">How to Conduct a Complete PPC Audit</a></li>
<li><a href="https://support.google.com/google-ads/answer/7102466?co=ADWORDS.IsAWNCustomer%3Dfalse&amp;hl=en" rel="nofollow" target="_blank">Google - Google Ads Help</a></li>
<li><a href="https://www.marketdominationmedia.com/author/jlong/" rel="nofollow" target="_blank">Jonathan Long</a> - <a href="https://www.marketdominationmedia.com/write-strong-paid-search-ad-copy/" rel="nofollow" target="_blank">How to Write Strong Paid Search ad Copy</a></li>
<li><a href="https://www.karooya.com/blog/author/kirti/" rel="nofollow" target="_blank">Kirti</a> - <a href="https://www.karooya.com/blog/adwords-audit-template/" rel="nofollow" target="_blank">Adwords Audit Template</a></li>
<li><a href="http://marketlytics.com/blog/?author=5799fe06bebafb6333a620d5" rel="nofollow" target="_blank">Noman Karim</a> - <a href="http://marketlytics.com/blog/google-tag-manager-adwords-conversion-tracking" rel="nofollow" target="_blank">Google Tag Manager Adwords Conversion Tracking 2018</a>&nbsp;</li>
<li><a href="https://supermetrics.com/blog/google-adwords-audit-1" rel="nofollow" target="_blank">Supermetrics - Google Adwords Audit</a></li>
</ul>
<p></p>
</div>
</div>


<div id="related-checklists">
<h2>Related checklists:</h2>
<div class="text-content">
<ul>
<li><a href="https://process.st/marketing-process" rel="nofollow">Marketing Process Toolkit: 10 Checklists to Crush Your Competition</a></li>
<li><a href="https://www.process.st/checklist/ppc-audit-checklist/%20" rel="nofollow">PPC Audit Checklist</a></li>
<li><a href="https://www.process.st/checklist/new-facebook-ads-creation-checklist/%20" rel="nofollow">New Facebook Ads Creation Checklist</a></li>
<li><a href="https://www.process.st/checklist/ppc-daily-campaign-review-checklist/" rel="nofollow">PPC Daily Campaign Review Checklist</a></li>
<li><a href="https://www.process.st/checklist/ppc-weekly-campaign-review-checklist/" rel="nofollow">PPC Weekly Campaign Review Checklist</a></li>
<li><a href="https://www.process.st/checklist/ppc-monthly-campaign-review-checklist/" rel="nofollow">PPC Monthly Campaign Review Checklist</a></li>
<li><a href="https://www.process.st/checklist/performance-marketing-ppc-keyword-competition-analysis-checklist/%20" rel="nofollow">Performance Marketing (PPC) Keyword Competition Analysis Checklist</a></li>
<li><a href="https://www.process.st/checklist/seo-checklist-the-keyword-research-process/" rel="nofollow">SEO Checklist: The Keyword Research Process</a></li>
</ul>
</div>                            
</div>
@endif
                         
                        
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>

<style type="text/css">
.scrollable {
    padding-left: 0px;
}    
nav.main-menu{
    display: none;
}

.main-grid {
    padding-left: 0px !important;
    padding-right: 0px !important;
}
h1.page-header{
    padding-left: 35px;
    padding-right: 35px;
}
.forms label{
    font-weight: 700;
    font-size: 14px;
    width: 100%;    
}
body {
    background: #fff;
}
.panel.panel-widget.forms-panel{
    border:0px;
}
.dashboard-page .title-bar {
    position: fixed;
    width: 100%;
    z-index: 99999;
    background: #0a7b9d;
}
.logo h1 a{
	color: #fff;
}
span.prfil-img i.fa.fa-user {
    color: #0a7b9d;
    background: #fff;
}
textarea {
    min-height: 85px;
    border-radius: 5px;
}
input[type="file"]{
	border: 0px !important;
	padding-left: 0px !important;
}
#form-side {
	margin-left: 16.5%;
}
.dashboard-page .title-bar{
	display: none;
}

#form-side input {
    font-size: 14px;
    margin-top: 2px;
}
.download_anchor{
	margin-top: 5px;
    float: left;
}
#form-side input, 
#form-side textarea{
    border: 0px !important;
    padding: 0px;
    outline: none;
}
#form-side .view-report input{
    border: 1px solid #ccc !important;
    padding-left: 6px !important;
    padding-right: 6px !important;
}
</style>







@stop

