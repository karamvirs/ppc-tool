<nav class="main-menu">
    <ul>
        <li>
            <a href="{!! route('dashboard') !!}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
                Dashboard
                </span>
            </a>
        </li>
        @if(((\Auth::user()->user_type)) == 1)
        <li class="has-subnav"> 
            <a href="javascript:;">
            <i class="fa fa-user" aria-hidden="true"></i>
            <span class="nav-text"> Users </span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li><a class="subnav-text" href="{!! route('customer') !!}">Users List</a></li>
            </ul>
        </li>
        @endif

        <li class="has-subnav"> 
            <a href="javascript:;">
            <i class="fa fa-file" aria-hidden="true"></i>
            <span class="nav-text"> Reports </span>
            <i class="icon-angle-right"></i><i class="icon-angle-down"></i>
            </a>
            <ul>
                <li><a class="subnav-text" href="{!! route('report.create') !!}"> Create Report </a> </li>
                <li><a class="subnav-text" href="{!! route('report.index') !!}"> Reports </a> </li>
            </ul>
        </li>
      
        </ul>
        </li>
        <li>
        <a href="{!! route('admin-logout') !!}">
        <i class="icon-off nav-icon"></i>
        <span class="nav-text">
        Logout
        </span>
        </a>
        </li>
    </ul>
 
</nav>




