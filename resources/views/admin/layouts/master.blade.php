<!DOCTYPE html>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
<title>PPC TOOL</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Custom Admin Theme Design" />
<meta name="csrf-token" content="{!! csrf_token() !!}" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap-css -->
{!! HTML::style('css/bootstrap.css') !!}
<!-- //bootstrap-css -->
<!-- Custom CSS -->
{!! HTML::style('css/style.css') !!}

<!-- font CSS -->
{!! HTML::style('http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic') !!}
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- font-awesome icons -->
{!! HTML::style('css/font.css') !!}
{!! HTML::style('css/font-awesome.css') !!}

<!-- //font-awesome icons -->
{!! HTML::script('js/jquery2.0.3.min.js') !!}
{!! HTML::script('js/modernizr.js') !!}
{!! HTML::script('js/jquery.cookie.js') !!}
{!! HTML::script('js/screenfull.js') !!}

@yield('css')
    <script>
    $(function () {
        $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

        if (!screenfull.enabled) {
            return false;
        }            

        $('#toggle').click(function () {
            screenfull.toggle($('#container')[0]);
        }); 
    });
    </script>
<!-- charts -->
{!! HTML::script('js/raphael-min.js') !!}
{!! HTML::script('js/morris.js') !!}
{!! HTML::script('js/morris.js') !!}
{!! HTML::style('css/morris.css') !!}
{!! HTML::style('css/template.css') !!}
<!-- //charts -->
<!--skycons-icons-->
{!! HTML::script('js/skycons.js') !!}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{!! HTML::script('js/jquery.validationEngine.js') !!}
{!! HTML::script('js/jquery.validationEngine-en.js') !!}

<!--//skycons-icons-->
</head>
@php
$route  = \Route::currentRouteName();
@endphp

<body class="dashboard-page @if($route == 'report_view') report_view_page @endif">

    @if($route != "report_view" and $route != "view-report")
    @include('admin.layouts.sidebar')    
    @endif
    <section class="wrapper scrollable">
        <nav class="user-menu">
            <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
            </a>
        </nav>
        @include('admin.layouts.header')
        <div class="main-grid">
            @yield('content')
        </div>
        @include('admin.layouts.footer')     
    </section>

<script type="text/javascript">

    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
          URL.revokeObjectURL(output.src) // free memory
        }
    };

    var loadFile1 = function(event) {
        var output1 = document.getElementById('output1');
        output1.src = URL.createObjectURL(event.target.files[0]);
        output1.onload = function() {
          URL.revokeObjectURL(output1.src) // free memory
        }
    };

    var loadFile2 = function(event) {
        var output2 = document.getElementById('output2');
        output2.src = URL.createObjectURL(event.target.files[0]);
        output2.onload = function() {
          URL.revokeObjectURL(output2.src) // free memory
        }
    };

    var loadFile3 = function(event) {
        var output3 = document.getElementById('output3');
        output3.src = URL.createObjectURL(event.target.files[0]);
        output3.onload = function() {
          URL.revokeObjectURL(output3.src) // free memory
        }
    };

    var loadFile4 = function(event) {
        var output4 = document.getElementById('output4');
        output4.src = URL.createObjectURL(event.target.files[0]);
        output4.onload = function() {
          URL.revokeObjectURL(output4.src) // free memory
        }
    };


    var loadFile5 = function(event) {
        var output5 = document.getElementById('output5');
        output5.src = URL.createObjectURL(event.target.files[0]);
        output5.onload = function() {
          URL.revokeObjectURL(output5.src) // free memory
        }
    };

    var loadFile6 = function(event) {
        var output6 = document.getElementById('output6');
        output6.src = URL.createObjectURL(event.target.files[0]);
        output6.onload = function() {
          URL.revokeObjectURL(output6.src) // free memory
        }
    };

    var loadFile7 = function(event) {
        var output7 = document.getElementById('output7');
        output7.src = URL.createObjectURL(event.target.files[0]);
        output7.onload = function() {
          URL.revokeObjectURL(output7.src) // free memory
        }
    };

    var loadFile8 = function(event) {
        var output8 = document.getElementById('output8');
        output8.src = URL.createObjectURL(event.target.files[0]);
        output8.onload = function() {
          URL.revokeObjectURL(output8.src) // free memory
        }
    };


    var loadFile9 = function(event) {
        var output9 = document.getElementById('output9');
        output9.src = URL.createObjectURL(event.target.files[0]);
        output9.onload = function() {
          URL.revokeObjectURL(output9.src) // free memory
        }
    };

    var loadFile10 = function(event) {
        var output10 = document.getElementById('output10');
        output10.src = URL.createObjectURL(event.target.files[0]);
        output10.onload = function() {
          URL.revokeObjectURL(output10.src) // free memory
        }
    };

    var loadFile11 = function(event) {
        var output11 = document.getElementById('output11');
        output11.src = URL.createObjectURL(event.target.files[0]);
        output11.onload = function() {
          URL.revokeObjectURL(output11.src) // free memory
        }
    };

    var loadFile12 = function(event) {
        var output12 = document.getElementById('output12');
        output12.src = URL.createObjectURL(event.target.files[0]);
        output12.onload = function() {
          URL.revokeObjectURL(output12.src) // free memory
        }
    };


    var loadFile13 = function(event) {
        var output13 = document.getElementById('output13');
        output13.src = URL.createObjectURL(event.target.files[0]);
        output13.onload = function() {
          URL.revokeObjectURL(output13.src) // free memory
        }
    };

    var loadFile14 = function(event) {
        var output14 = document.getElementById('output14');
        output14.src = URL.createObjectURL(event.target.files[0]);
        output14.onload = function() {
          URL.revokeObjectURL(output14.src) // free memory
        }
    };

    var loadFile15 = function(event) {
        var output15 = document.getElementById('output15');
        output15.src = URL.createObjectURL(event.target.files[0]);
        output15.onload = function() {
          URL.revokeObjectURL(output15.src) // free memory
        }
    };

    var loadFile16 = function(event) {
        var output16 = document.getElementById('output16');
        output16.src = URL.createObjectURL(event.target.files[0]);
        output16.onload = function() {
          URL.revokeObjectURL(output16.src) // free memory
        }
    };


    var loadFile17 = function(event) {
        var output17 = document.getElementById('output17');
        output17.src = URL.createObjectURL(event.target.files[0]);
        output17.onload = function() {
          URL.revokeObjectURL(output17.src) // free memory
        }
    };

    var loadFile18 = function(event) {
        var output18 = document.getElementById('output18');
        output18.src = URL.createObjectURL(event.target.files[0]);
        output18.onload = function() {
          URL.revokeObjectURL(output18.src) // free memory
        }
    };

    var loadFile19 = function(event) {
        var output19 = document.getElementById('output19');
        output19.src = URL.createObjectURL(event.target.files[0]);
        output19.onload = function() {
          URL.revokeObjectURL(output19.src) // free memory
        }
    };


    var loadFile20 = function(event) {
        var output20 = document.getElementById('output20');
        output20.src = URL.createObjectURL(event.target.files[0]);
        output20.onload = function() {
          URL.revokeObjectURL(output20.src) // free memory
        }
    };

    var loadFile21 = function(event) {
        var output21 = document.getElementById('output21');
        output21.src = URL.createObjectURL(event.target.files[0]);
        output21.onload = function() {
          URL.revokeObjectURL(output21.src) // free memory
        }
    };

    var loadFile22 = function(event) {
        var output22 = document.getElementById('output22');
        output22.src = URL.createObjectURL(event.target.files[0]);
        output22.onload = function() {
          URL.revokeObjectURL(output22.src) // free memory
        }
    };

    var loadFile23 = function(event) {
        var output23 = document.getElementById('output23');
        output23.src = URL.createObjectURL(event.target.files[0]);
        output23.onload = function() {
          URL.revokeObjectURL(output23.src) // free memory
        }
    };

    var loadFile231 = function(event) {
        var output231 = document.getElementById('output231');
        output231.src = URL.createObjectURL(event.target.files[0]);
        output231.onload = function() {
          URL.revokeObjectURL(output231.src) // free memory
        }
    };

    var loadFile24 = function(event) {
        var output24 = document.getElementById('output24');
        output24.src = URL.createObjectURL(event.target.files[0]);
        output24.onload = function() {
          URL.revokeObjectURL(output24.src) // free memory
        }
    };

    var loadFile25 = function(event) {
        var output25 = document.getElementById('output25');
        output25.src = URL.createObjectURL(event.target.files[0]);
        output25.onload = function() {
          URL.revokeObjectURL(output25.src) // free memory
        }
    };

    var loadFile26 = function(event) {
        var output26 = document.getElementById('output26');
        output26.src = URL.createObjectURL(event.target.files[0]);
        output26.onload = function() {
          URL.revokeObjectURL(output26.src) // free memory
        }
    };

    var loadFile27 = function(event) {
        var output27 = document.getElementById('output27');
        output27.src = URL.createObjectURL(event.target.files[0]);
        output27.onload = function() {
          URL.revokeObjectURL(output27.src) // free memory
        }
    };


    var loadFile28 = function(event) {
        var output28 = document.getElementById('output28');
        output28.src = URL.createObjectURL(event.target.files[0]);
        output28.onload = function() {
          URL.revokeObjectURL(output28.src) // free memory
        }
    };

    var loadFile29 = function(event) {
        var output29 = document.getElementById('output29');
        output29.src = URL.createObjectURL(event.target.files[0]);
        output29.onload = function() {
          URL.revokeObjectURL(output29.src) // free memory
        }
    };

    var loadFile30 = function(event) {
        var output30 = document.getElementById('output30');
        output30.src = URL.createObjectURL(event.target.files[0]);
        output30.onload = function() {
          URL.revokeObjectURL(output30.src) // free memory
        }
    };

    var loadFile31 = function(event) {
        var output31 = document.getElementById('output31');
        output31.src = URL.createObjectURL(event.target.files[0]);
        output31.onload = function() {
          URL.revokeObjectURL(output31.src) // free memory
        }
    };

    var loadFile32 = function(event) {
        var output32 = document.getElementById('output32');
        output32.src = URL.createObjectURL(event.target.files[0]);
        output32.onload = function() {
          URL.revokeObjectURL(output32.src) // free memory
        }
    };

    var loadFile33 = function(event) {
        var output33 = document.getElementById('output33');
        output33.src = URL.createObjectURL(event.target.files[0]);
        output33.onload = function() {
          URL.revokeObjectURL(output33.src) // free memory
        }
    }; 
 
    var loadFile34 = function(event) {
        var output34 = document.getElementById('output34');
        output34.src = URL.createObjectURL(event.target.files[0]);
        output34.onload = function() {
          URL.revokeObjectURL(output34.src) // free memory
        }
    };

    var loadFile35 = function(event) {
        var output35 = document.getElementById('output35');
        output35.src = URL.createObjectURL(event.target.files[0]);
        output35.onload = function() {
          URL.revokeObjectURL(output35.src) // free memory
        }
    };

    var loadFile36 = function(event) {
        var output36 = document.getElementById('output36');
        output36.src = URL.createObjectURL(event.target.files[0]);
        output36.onload = function() {
          URL.revokeObjectURL(output36.src) // free memory
        }
    };

jQuery(document).ready(function(){
    $("#form-side").css("padding-top", "70px;");
jQuery("#ajaxSave").validationEngine();

$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
});



    $('#ajaxSave').submit(function(e) {
       $("#form-side").css("padding-top", "70px;");
      
    if ($("#ajaxSave").validationEngine('validate') ) {
        e.preventDefault();
      //  serialized = $("#ajaxSave").serialize();
        var formData = new FormData(this);
        $.ajax({
           type:'POST',
           url: "{{ route('report.store') }}",
           data: formData,
           cache:false,
           timeout: 50000,
           processData: false,
           contentType: false,
            beforeSend: function() {
              $("#loading-image").show();
            },
           success:function(data){
       
                // $("#login-succes").html(data.succes);
                  window.location='/admin/report';
            
           }
        });
    }
    });



mybutton = document.getElementById("Go_to_Bottom1");
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
</script>

<script type="text/javascript">
$(document).ready(function(){
  $("a").on('click', function(event) {

    if (this.hash !== "") {
      event.preventDefault();

      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){

        window.location.hash = hash;
      });
    } 
  });
});


</script>
</body>
</html>
