@extends('admin.layouts.master')
@section('content')

@if(session()->has('pas-updated')) 
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close">×</button>
    <i class="fa fa-check-circle"></i> &nbsp; Password is Updated</div>
@endif

<div class="agile-grids">   
    <div class="grids">       
        <div class="row">
            
            <div class="col-md-6">
                <h1 class="page-header">Update Password</h1>
                <div class="panel panel-widget forms-panel">
                    <div class="forms">
                        <div class="form-grids widget-shadow" data-example-id="basic-forms"> 
                            <div class="form-title">
                                <h4>{{ $user->name }}</h4>
                            </div>
                            <div class="form-body">
                                    {!! Form::open(array('method' => 'POST', 'route' => array('customer.update-password-save'), 'id' => 'ajaxSave1', 'class' => '')) !!}                               
                                <div class="row">
                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                    <div class="col-md-12">
                                        <div class="form-group"> 
                                            {!! Form::label('new_password', lang('setting.new_password'), array('class' => '')) !!}

                                            {!! Form::password('new_password', array('class' => 'form-control', 'required'=> 'true')) !!}
                                            <span>Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters</span>
                                            @if($errors->has('new_password'))
                                             <span class="text-danger"><br>{{$errors->first('new_password')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <p>&nbsp;</p>
                                    <div class="col-md-12">
                                         <button type="submit" class="btn btn-default w3ls-button" style="width: 28%;">Submit</button> 
                                    </div>
                                </div>
                                    
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

