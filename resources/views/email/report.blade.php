<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- utf-8 works for most cases -->
        <meta name="viewport" content="width=device-width">
        <!-- Forcing initial-scale shouldn't be necessary -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Use the latest (edge) version of IE rendering engine -->
        <meta name="x-apple-disable-message-reformatting">
        <!-- Disable auto-scale in iOS 10 Mail entirely -->
        <title>Emailer</title>
    </head>
    <body style="margin: 0; mso-line-height-rule: exactly; margin-top: 50px !important; margin-bottom: 50px !important; background-color: #f3f3f3;padding: 20px;">
      <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,500,600" rel="stylesheet" type="text/css">

        <style>
            html, body {
               font-family: 'Open Sans', sans-serif;
            }
        </style>

        <center style="width: 100%; text-align: left;">
            <div style="width:700px; min-height: 300px; margin: 0 auto; background-color:white; border: 12px solid white;">
               
               <div style="width: 100%; height: 100%; margin: 0 auto;">
                  <div class="header-logo" style="width: 100%; padding:20px 0px; overflow: hidden; margin: 0 auto;">
                    <a href="#" target="_blank" style="text-decoration: none; font-weight: 700; color: #0a7b9d; font-size: 24px;">PPC TOOL</a>
                  </div>
                  <div style="padding: 0px 0px 15px 0; width: 100%; background: #fff;">
                  <p><b>Hey {{ $name }},</b> <br><br>
                  Kindly use this password to view your report "<b>{{ $password }}</b>"<br><br>
                 Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer<br><br>
                  <b>took a galley of type and scrambled it to make a type specimen book.</b> </p>
                  </div>
                  <a href="{!! route('report_view', $id) !!}" style="background: #0a7b9d; color: #fff; padding: 10px 20px; text-decoration: none;">View Report</a>                  
               </div>
            </div>
      </center>
   </body>
</html>