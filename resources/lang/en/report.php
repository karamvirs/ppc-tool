<?php 
/**
 * :: report Language File ::
 * To manage category related language phrases.
 *
 **/

return [
	'report'		  => 'Report',
	'reports'	      => 'Reports',
	'report_info'	  => 'Report Information',
];