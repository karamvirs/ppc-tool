<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myAccount(Request $request)
    {  
         
     
     try{


        $user_id =  authUserIdNull();
        $user_result = User::where('id', $user_id)->first();


        $inputs = $request->all();
        if (count($inputs) > 0) {

            $validator = (new User)->validatePassword($inputs);
            if ($validator->fails()) {
            if($user_result->user_type==1){
                return redirect()->route('setting.manage-account')
                    ->withErrors($validator);
            }
            else if($user_result->user_type==2){

                return redirect()->route('setting.manage-account')
                    ->withErrors($validator);
            } else{
             return redirect()->route('setting.manage-account')
                    ->withErrors($validator);
            }
            }


            $password = \Auth::user()->password;
            if(!(\Hash::check($inputs['password'], $password))){

            if($user_result->user_type==1){
                return redirect()->route('setting.manage-account')
                    ->with("error", lang('messages.invalid_password'));
            }
            else if($user_result->user_type==2){
                return redirect()->route('setting.manage-account')
                    ->with("error", lang('messages.invalid_password'));
            }
            else{
                return view('frontend.pages.change-password')->with('not_update', 'error');;
            }
            }

            (new User)->updatePassword(\Hash::make($inputs['new_password']));

            if($user_result->user_type==1){
            return redirect()->route('setting.manage-account')
                ->with('success', lang('messages.password_updated'));
            }
            else if($user_result->user_type==2){
            return redirect()->route('setting.manage-account')
                ->with('success', lang('messages.password_updated'));
            }
            else{
          
                return redirect()->back()->with('update', 'success');
            }
        }
        if($user_result->user_type==1){
          return view('admin.setting.account');
        }
        else if($user_result->user_type==2){
          return view('admin.setting.account');
        }
        else{
              return view('admin.setting.account');  
       }
    } catch (\Exception $exception) {
          
            dd($exception); 

            return back();
        }
}


    
}
