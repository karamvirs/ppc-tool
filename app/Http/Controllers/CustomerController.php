<?php

/**
 * @Author Akhilesh
 * @Created_at 14-04-2021
 */
namespace App\Http\Controllers;

//use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserType;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\Mail;

class CustomerController extends Controller
{
 
    public function index()
    {
      
      if(((\Auth::user()->user_type)) == 1){
        return view('admin.customer.index');
      } else {

        echo "404";
      }
    }


    public function create()
    {
    if(((\Auth::user()->user_type)) == 1){
        $UserType = UserType::where('status', 1)->where('id', '!=', 1)->get();
        return view('admin.customer.create', compact('UserType'));
      } else {
        echo "404";
      }
    }


    public function store( Request $request ){

        $inputs = $request->all(); 
          
        $validator = (new User)->validate($inputs);
        if ($validator->fails()) {
            return redirect()->route('customer.create')
            ->withInput()->withErrors($validator);
        }            
        
        try{

            $pwd = $inputs['password'];
            $password = \Hash::make($inputs['password']);
            unset($inputs['password']);
            $inputs = $inputs + ['password' => $password];
            // Generating API key
    
            $inputs = $inputs + [
                                  'status'    => 1,
                                ];
            $user_id = (new User)->store($inputs);  
             
           return view('admin.customer.index')
                ->with('success', lang('messages.created', lang('customer.customer')));
           
        }
        catch (Exception $exception) {
            //\DB::rollBack();
            return redirect()->route('customer.create')
                ->withInput()
                ->with('error', lang('messages.server_error'));
        }
    }
    

     public function updatePassword($id = null)
    {
      
      if(((\Auth::user()->user_type)) == 1){

        $user = User::where('id', $id)->first();

         if($user){
           
           return view('admin.customer.change_pasword', compact('user'));

         }

        
      } else {

        echo "Wrong Url";
      }
    }

    public function updatePasswordSave(Request $request){

        try{

            $inputs = $request->all();
 
            $validator = (new User)->pas_validate($inputs);
            if( $validator->fails() ) {
                return back()->withErrors($validator)->withInput();
            }
        
            $password =  (\Hash::make($inputs['new_password']));


            User::where('id', $request->user_id)
                ->update([
                    'password' =>  $password,
            ]);


          return back()->with('pas-updated', 'pas-updated');

        } catch (\Exception $exception) {
          
            dd($exception); 

            return redirect()->route('customer.update-password', $request->user_id)
                ->withInput()
                ->with('error', lang('messages.server_error'));
        }

    }

    public function update(Request $request, $id = null)
    {
        $result = User::find($id);
        $user_type = $result->user_type;

        if (!$result) {

            return redirect()->route('customer.index')
                ->with('error', lang('messages.invalid_id', string_manip(lang('customer.customer'))));
        }

        $inputs = $request->all();
        $validator = (new User)->validate_update($inputs, $id);
        if ($validator->fails()) {
            return redirect()->route('customer.edit',[$id])
            ->withInput()->withErrors($validator);
        } 

        try {
          
            (new User)->store($inputs, $id); 

          return redirect()->route('customer')
                ->with('success', lang('messages.created', lang('customer.customer')));
      
        } catch (\Exception $exception) {

       // dd($exception);
            return redirect()->route('customer.edit',[$id])
                ->with('error', lang('messages.server_error'));
        }
    }
    

    private function generateTokenKey() {
        return md5(uniqid(rand(), true));
    }


    public function edit($id = null)
    {
        $result = User::find($id);
        if (!$result) {
            abort(404);
        }
   
       if(((\Auth::user()->user_type)) == 1){

        $UserType = UserType::where('id', $result->user_type)->first()->type;

         return view('admin.customer.create', compact('result', 'UserType'));
      } else {
        echo "404";
      }

    }


    

    public function changePwd(Request $request)
    {
        try {
            $id=\Auth::user()->id;
            \DB::beginTransaction();
            /* FIND WHETHER THE USER EXISTS OR NOT */
            $user = User::find($id);
            if(!$user) {
                return apiResponse(false, 404, lang('messages.not_found', lang('user.user')));
            }
            $inputs = $request->all();
            $rules = [
                    'password' => 'required',
                    'new_password'=>'required|min:6'
                    ];
            $validator=\Validator::make($inputs, $rules);
            if ($validator->fails()) {
                return apiResponse(false, 406, "", errorMessages($validator->messages()));
            }
      
                if (!\Hash::check($inputs['password'], \Auth::user()->password) ){
                    return apiResponse(false, 406,lang('user.password_not_match'));
                }

                $password = \Hash::make($inputs['new_password']);
                unset($inputs['password']);
                $inputs = $inputs + ['password' => $password];
                
                (new User)->store($inputs, $id);
                \DB::commit();
                return apiResponse(true, 200, lang('messages.updated', lang('user.user')));
           
        }
        catch (Exception $exception) {
            \DB::rollBack();
            return apiResponse(false, 500, lang('messages.server_error'));
        }
    }


    public function customerPaginate(Request $request, $id, $pageNumber = null)
    {

        if (!\Request::isMethod('post') && !\Request::ajax()) { //
            return lang('messages.server_error');
        }

        $inputs = $request->all();
        $page = 1;
        if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
            $page = $inputs['page'];
        }

        $perPage = 20;
        if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
            $perPage = $inputs['perpage'];
        }

        $start = ($page - 1) * $perPage;
        if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
            $inputs = array_filter($inputs);
            unset($inputs['_token']);

            $data = (new User)->getCustomer($inputs, $start, $perPage);
            $totalGameMaster = (new User)->totalCustomer($inputs);
            $total = $totalGameMaster->total;
        } else {

            $data = (new User)->getCustomer($inputs, $start, $perPage, $id);
            $totalGameMaster = (new User)->totalCustomer();
            $total = $totalGameMaster->total;
        }

        return view('admin.customer.load_data', compact('inputs', 'data', 'total', 'page', 'perPage'));
    }




    public function customerToggle($id = null)
    {
         if (!\Request::isMethod('post') && !\Request::ajax()) {
            return lang('messages.server_error');
        }

        try {
            $game = User::find($id);
            //dd($game);

        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('Order')));
        }

        $game->update(['status' => !$game->status]);
        $response = ['status' => 1, 'data' => (int)$game->status . '.gif'];
 

        // return json response
        return json_encode($response);
    }


    public function customerAction(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['tick']) || count($inputs['tick']) < 1) {
            // return redirect()->route('customer.index')
             return view('admin.customer.index')->with('error', lang('messages.atleast_one', string_manip(lang('customer.customer'))));
        }

        $ids = '';
        foreach ($inputs['tick'] as $key => $value) {
            $ids .= $value . ',';
        }

        $ids = rtrim($ids, ',');
        $status = 0;
        if (isset($inputs['active'])) {
            $status = 1;
        }

        User::whereRaw('id IN (' . $ids . ')')->update(['status' => $status]);
        return redirect()->route('customer.index')
            ->with('success', lang('messages.updated', lang('game_master.game')));
    }


    public function customerAction_data_entry(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['tick']) || count($inputs['tick']) < 1) {
            // return redirect()->route('customer.index')
             return view('admin.customer.data-entry')->with('error', lang('messages.atleast_one', string_manip(lang('customer.customer'))));
        }

        $ids = '';
        foreach ($inputs['tick'] as $key => $value) {
            $ids .= $value . ',';
        }

        $ids = rtrim($ids, ',');
        $status = 0;
        if (isset($inputs['active'])) {
            $status = 1;
        }

        DataEntry::whereRaw('id IN (' . $ids . ')')->update(['status' => $status]);
        return redirect()->route('customer.data-entry')
            ->with('success', lang('messages.updated', lang('game_master.game')));
    }
    



  public function export_users(){
     
     try{
          $users = User::Orderby('created_at', 'desc')->where('id', '!=', 1)->get();
        

            \Excel::create('users', function($excel) use($users) {
            $excel->sheet('customer', function($sheet) use($users) {
                $excelData = [];
                $excelData[] = [
                'Name',
                'Email',
                'Mobile',
                'Status',
                'Created At',
                ];
                foreach ($users as $key => $value) {
                $excelData[] = [
                $value->name,
                $value->email,
                $value->mobile,
                $value->status  == 1 ? 'Active' : 'Inactive',
                date("M d Y", strtotime($value->created_at)),
                ]; 
                }
                $sheet->fromArray($excelData, null, 'A1', true, false);
            });
            })->download('xlsx');

     } catch(Exception $exc){
     // dd($exc);

       $response = ['status' => 0, 'message' => lang('messages.server_error')];

     }

 }
   
}