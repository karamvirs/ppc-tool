<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;


class DashboardController extends Controller
{
  
    public function index()
    {
        $user_type = \Auth::user()->user_type;
        $user_id =  Auth::id();

        $user = \DB::table("users")->where('user_type', '!=', 1)->count();
        if($user_type == 1){
            $reports = \DB::table("reports")->count();
        } else {
        	$reports = \DB::table("reports")->where('created_by', $user_id)->count();
        }
        
        $currentMonth = date('m');
		$newusers = \DB::table("users")->whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('user_type', '!=', 1)->count();
		if($user_type == 1){
           $newreports = \DB::table("reports")->whereRaw('MONTH(created_at) = ?',[$currentMonth])->count();
		} else {
			$newreports = \DB::table("reports")->whereRaw('MONTH(created_at) = ?',[$currentMonth])->where('created_by', $user_id)->count();
		}
		
        return view('admin.dashboard', compact('user', 'newusers', 'newreports', 'reports'));
    }  







}
