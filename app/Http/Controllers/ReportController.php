<?php

/**
 * @Author Akhilesh
 * @Created_at 14-04-2021
 */
namespace App\Http\Controllers;

use Auth;
use Files;
use Illuminate\Support\Facades\Storage;
use App\Models\Report;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Illuminate\Support\Facades\Mail;

class ReportController extends Controller
{
 
    public function index()
    {
      
        return view('admin.report.index');

    }


    public function create()
    {

        return view('admin.report.create');
     
    }

    public function store( Request $request ){

        $inputs = $request->all();             
        
        try{
    
            $user_id = authUserId();

            if(isset($inputs['audit_data']) or !empty($inputs['audit_data']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('audit_data')) {
                    $file1 = $request->file('audit_data') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $audit_data = $fname1.$fileName1;
            }
            else{
                $audit_data = null;
            }
            unset($inputs['audit_data']);
            $inputs['audit_data'] = $audit_data;

            if(isset($inputs['google_tag_assistant']) or !empty($inputs['google_tag_assistant']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('google_tag_assistant')) {
                    $file1 = $request->file('google_tag_assistant') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $google_tag_assistant = $fname1.$fileName1;
            }
            else{
                $google_tag_assistant = null;
            }
            unset($inputs['google_tag_assistant']);
            $inputs['google_tag_assistant'] = $google_tag_assistant;

            if(isset($inputs['conversion_actions']) or !empty($inputs['conversion_actions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('conversion_actions')) {
                    $file1 = $request->file('conversion_actions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $conversion_actions = $fname1.$fileName1;
            }
            else{
                $conversion_actions = null;
            }
            unset($inputs['conversion_actions']);
            $inputs['conversion_actions'] = $conversion_actions;

            if(isset($inputs['google_ads']) or !empty($inputs['google_ads']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('google_ads')) {
                    $file1 = $request->file('google_ads') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $google_ads = $fname1.$fileName1;
            }
            else{
                $google_ads = null;
            }
            unset($inputs['google_ads']);
            $inputs['google_ads'] = $google_ads;

            if(isset($inputs['new_campaign']) or !empty($inputs['new_campaign']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('new_campaign')) {
                    $file1 = $request->file('new_campaign') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $new_campaign = $fname1.$fileName1;
            }
            else{
                $new_campaign = null;
            }
            unset($inputs['new_campaign']);
            $inputs['new_campaign'] = $new_campaign;

            if(isset($inputs['schedule_settings']) or !empty($inputs['schedule_settings']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('schedule_settings')) {
                    $file1 = $request->file('schedule_settings') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $schedule_settings = $fname1.$fileName1;
            }
            else{
                $schedule_settings = null;
            }
            unset($inputs['schedule_settings']);
            $inputs['schedule_settings'] = $schedule_settings;

            if(isset($inputs['all_features']) or !empty($inputs['all_features']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('all_features')) {
                    $file1 = $request->file('all_features') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $all_features = $fname1.$fileName1;
            }
            else{
                $all_features = null;
            }
            unset($inputs['all_features']);
            $inputs['all_features'] = $all_features;

            if(isset($inputs['search_partners']) or !empty($inputs['search_partners']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('search_partners')) {
                    $file1 = $request->file('search_partners') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $search_partners = $fname1.$fileName1;
            }
            else{
                $search_partners = null;
            }
            unset($inputs['search_partners']);
            $inputs['search_partners'] = $search_partners;

            if(isset($inputs['ad_variations']) or !empty($inputs['ad_variations']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('ad_variations')) {
                    $file1 = $request->file('ad_variations') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $ad_variations = $fname1.$fileName1;
            }
            else{
                $ad_variations = null;
            }
            unset($inputs['ad_variations']);
            $inputs['ad_variations'] = $ad_variations;

            if(isset($inputs['dynamic_search_ads']) or !empty($inputs['dynamic_search_ads']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('dynamic_search_ads')) {
                    $file1 = $request->file('dynamic_search_ads') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $dynamic_search_ads = $fname1.$fileName1;
            }
            else{
                $dynamic_search_ads = null;
            }
            unset($inputs['dynamic_search_ads']);
            $inputs['dynamic_search_ads'] = $dynamic_search_ads;

            if(isset($inputs['current_budget']) or !empty($inputs['current_budget']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('current_budget')) {
                    $file1 = $request->file('current_budget') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $current_budget = $fname1.$fileName1;
            }
            else{
                $current_budget = null;
            }
            unset($inputs['current_budget']);
            $inputs['current_budget'] = $current_budget;

            if(isset($inputs['bidding_strategy_image']) or !empty($inputs['bidding_strategy_image']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('bidding_strategy_image')) {
                    $file1 = $request->file('bidding_strategy_image') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $bidding_strategy_image = $fname1.$fileName1;
            }
            else{
                $bidding_strategy_image = null;
            }
            unset($inputs['bidding_strategy_image']);
            $inputs['bidding_strategy_image'] = $bidding_strategy_image;

            if(isset($inputs['bid_adjustments']) or !empty($inputs['bid_adjustments']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('bid_adjustments')) {
                    $file1 = $request->file('bid_adjustments') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $bid_adjustments = $fname1.$fileName1;
            }
            else{
                $bid_adjustments = null;
            }
            unset($inputs['bid_adjustments']);
            $inputs['bid_adjustments'] = $bid_adjustments;

            if(isset($inputs['location_targeting']) or !empty($inputs['location_targeting']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('location_targeting')) {
                    $file1 = $request->file('location_targeting') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $location_targeting = $fname1.$fileName1;
            }
            else{
                $location_targeting = null;
            }
            unset($inputs['location_targeting']);
            $inputs['location_targeting'] = $location_targeting;

            if(isset($inputs['targeting_img']) or !empty($inputs['targeting_img']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('targeting_img')) {
                    $file1 = $request->file('targeting_img') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $targeting_img = $fname1.$fileName1;
            }
            else{
                $targeting_img = null;
            }
            unset($inputs['targeting_img']);
            $inputs['targeting_img'] = $targeting_img;

            if(isset($inputs['semographic_specifications']) or !empty($inputs['semographic_specifications']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('semographic_specifications')) {
                    $file1 = $request->file('semographic_specifications') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $semographic_specifications = $fname1.$fileName1;
            }
            else{
                $semographic_specifications = null;
            }
            unset($inputs['semographic_specifications']);
            $inputs['semographic_specifications'] = $semographic_specifications;

            if(isset($inputs['negative_keyword_conflicts']) or !empty($inputs['negative_keyword_conflicts']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('negative_keyword_conflicts')) {
                    $file1 = $request->file('negative_keyword_conflicts') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $negative_keyword_conflicts = $fname1.$fileName1;
            }
            else{
                $negative_keyword_conflicts = null;
            }
            unset($inputs['negative_keyword_conflicts']);
            $inputs['negative_keyword_conflicts'] = $negative_keyword_conflicts;


            if(isset($inputs['search_result']) or !empty($inputs['search_result']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('search_result')) {
                    $file1 = $request->file('search_result') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $search_result = $fname1.$fileName1;
            }
            else{
                $search_result = null;
            }
            unset($inputs['search_result']);
            $inputs['search_result'] = $search_result;

            if(isset($inputs['keyword_list']) or !empty($inputs['keyword_list']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('keyword_list')) {
                    $file1 = $request->file('keyword_list') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $keyword_list = $fname1.$fileName1;
            }
            else{
                $keyword_list = null;
            }
            unset($inputs['keyword_list']);
            $inputs['keyword_list'] = $keyword_list;

            if(isset($inputs['home_vs_landing']) or !empty($inputs['home_vs_landing']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('home_vs_landing')) {
                    $file1 = $request->file('home_vs_landing') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $home_vs_landing = $fname1.$fileName1;
            }
            else{
                $home_vs_landing = null;
            }
            unset($inputs['home_vs_landing']);
            $inputs['home_vs_landing'] = $home_vs_landing;

            if(isset($inputs['new_offer']) or !empty($inputs['new_offer']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('new_offer')) {
                    $file1 = $request->file('new_offer') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $new_offer = $fname1.$fileName1;
            }
            else{
                $new_offer = null;
            }
            unset($inputs['new_offer']);
            $inputs['new_offer'] = $new_offer;

            if(isset($inputs['grammatical_or_spelling']) or !empty($inputs['grammatical_or_spelling']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('grammatical_or_spelling')) {
                    $file1 = $request->file('grammatical_or_spelling') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $grammatical_or_spelling = $fname1.$fileName1;
            }
            else{
                $grammatical_or_spelling = null;
            }
            unset($inputs['grammatical_or_spelling']);
            $inputs['grammatical_or_spelling'] = $grammatical_or_spelling;

            if(isset($inputs['call_to_action']) or !empty($inputs['call_to_action']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('call_to_action')) {
                    $file1 = $request->file('call_to_action') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $call_to_action = $fname1.$fileName1;
            }
            else{
                $call_to_action = null;
            }
            unset($inputs['call_to_action']);
            $inputs['call_to_action'] = $call_to_action;

             if(isset($inputs['test_insights']) or !empty($inputs['test_insights']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('test_insights')) {
                    $file1 = $request->file('test_insights') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $test_insights = $fname1.$fileName1;
            }
            else{
                $test_insights = null;
            }
            unset($inputs['test_insights']);
            $inputs['test_insights'] = $test_insights;


             if(isset($inputs['display_campaigns']) or !empty($inputs['display_campaigns']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('display_campaigns')) {
                    $file1 = $request->file('display_campaigns') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $display_campaigns = $fname1.$fileName1;
            }
            else{
                $display_campaigns = null;
            }
            unset($inputs['display_campaigns']);
            $inputs['display_campaigns'] = $display_campaigns;


             if(isset($inputs['search_terms']) or !empty($inputs['search_terms']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('search_terms')) {
                    $file1 = $request->file('search_terms') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $search_terms = $fname1.$fileName1;
            }
            else{
                $search_terms = null;
            }
            unset($inputs['search_terms']);
            $inputs['search_terms'] = $search_terms;

             if(isset($inputs['match_type']) or !empty($inputs['match_type']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('match_type')) {
                    $file1 = $request->file('match_type') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $match_type = $fname1.$fileName1;
            }
            else{
                $match_type = null;
            }
            unset($inputs['match_type']);
            $inputs['match_type'] = $match_type;

             if(isset($inputs['conflict_tracking_document']) or !empty($inputs['conflict_tracking_document']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('conflict_tracking_document')) {
                    $file1 = $request->file('conflict_tracking_document') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $conflict_tracking_document = $fname1.$fileName1;
            }
            else{
                $conflict_tracking_document = null;
            }
            unset($inputs['conflict_tracking_document']);
            $inputs['conflict_tracking_document'] = $conflict_tracking_document;


            if(isset($inputs['verify_proper_search']) or !empty($inputs['verify_proper_search']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('verify_proper_search')) {
                    $file1 = $request->file('verify_proper_search') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $verify_proper_search = $fname1.$fileName1;
            }
            else{
                $verify_proper_search = null;
            }
            unset($inputs['verify_proper_search']);
            $inputs['verify_proper_search'] = $verify_proper_search;

            if(isset($inputs['number_of_keywords']) or !empty($inputs['number_of_keywords']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('number_of_keywords')) {
                    $file1 = $request->file('number_of_keywords') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $number_of_keywords = $fname1.$fileName1;
            }
            else{
                $number_of_keywords = null;
            }
            unset($inputs['number_of_keywords']);
            $inputs['number_of_keywords'] = $number_of_keywords;

            if(isset($inputs['zero_impressions']) or !empty($inputs['zero_impressions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('zero_impressions')) {
                    $file1 = $request->file('zero_impressions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $zero_impressions = $fname1.$fileName1;
            }
            else{
                $zero_impressions = null;
            }
            unset($inputs['zero_impressions']);
            $inputs['zero_impressions'] = $zero_impressions;

            if(isset($inputs['keywords_with_poor_quality']) or !empty($inputs['keywords_with_poor_quality']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('keywords_with_poor_quality')) {
                    $file1 = $request->file('keywords_with_poor_quality') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $keywords_with_poor_quality = $fname1.$fileName1;
            }
            else{
                $keywords_with_poor_quality = null;
            }
            unset($inputs['keywords_with_poor_quality']);
            $inputs['keywords_with_poor_quality'] = $keywords_with_poor_quality;

            if(isset($inputs['missing_ad_extensions']) or !empty($inputs['missing_ad_extensions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('missing_ad_extensions')) {
                    $file1 = $request->file('missing_ad_extensions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $missing_ad_extensions = $fname1.$fileName1;
            }
            else{
                $missing_ad_extensions = null;
            }
            unset($inputs['missing_ad_extensions']);
            $inputs['missing_ad_extensions'] = $missing_ad_extensions;

            if(isset($inputs['non_performing_automated_extensions']) or !empty($inputs['non_performing_automated_extensions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('non_performing_automated_extensions')) {
                    $file1 = $request->file('non_performing_automated_extensions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $non_performing_automated_extensions = $fname1.$fileName1;
            }
            else{
                $non_performing_automated_extensions = null;
            }
            unset($inputs['non_performing_automated_extensions']);
            $inputs['non_performing_automated_extensions'] = $non_performing_automated_extensions;

            if(isset($inputs['current_approval_status']) or !empty($inputs['current_approval_status']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('current_approval_status')) {
                    $file1 = $request->file('current_approval_status') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $current_approval_status = $fname1.$fileName1;
            }
            else{
                $current_approval_status = null;
            }
            unset($inputs['current_approval_status']);
            $inputs['current_approval_status'] = $current_approval_status;

            if(isset($inputs['ensure_sitelinks']) or !empty($inputs['ensure_sitelinks']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('ensure_sitelinks')) {
                    $file1 = $request->file('ensure_sitelinks') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $ensure_sitelinks = $fname1.$fileName1;
            }
            else{
                $ensure_sitelinks = null;
            }
            unset($inputs['ensure_sitelinks']);
            $inputs['ensure_sitelinks'] = $ensure_sitelinks;

            if(isset($inputs['call_extension']) or !empty($inputs['call_extension']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('call_extension')) {
                    $file1 = $request->file('call_extension') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $call_extension = $fname1.$fileName1;
            }
            else{
                $call_extension = null;
            }
            unset($inputs['call_extension']);
            $inputs['call_extension'] = $call_extension;

            if(isset($inputs['call_reporting']) or !empty($inputs['call_reporting']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('call_reporting')) {
                    $file1 = $request->file('call_reporting') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $call_reporting = $fname1.$fileName1;
            }
            else{
                $call_reporting = null;
            }
            unset($inputs['call_reporting']);
            $inputs['call_reporting'] = $call_reporting;

            if(isset($inputs['validate_remarketing_code']) or !empty($inputs['validate_remarketing_code']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('validate_remarketing_code')) {
                    $file1 = $request->file('validate_remarketing_code') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $validate_remarketing_code = $fname1.$fileName1;
            }
            else{
                $validate_remarketing_code = null;
            }
            unset($inputs['validate_remarketing_code']);
            $inputs['validate_remarketing_code'] = $validate_remarketing_code;

            if(isset($inputs['validate_remarketing_list']) or !empty($inputs['validate_remarketing_list']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('validate_remarketing_list')) {
                    $file1 = $request->file('validate_remarketing_list') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $validate_remarketing_list = $fname1.$fileName1;
            }
            else{
                $validate_remarketing_list = null;
            }
            unset($inputs['validate_remarketing_list']);
            $inputs['validate_remarketing_list'] = $validate_remarketing_list;

            if(isset($inputs['verify_the_remarketing_lists']) or !empty($inputs['verify_the_remarketing_lists']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('verify_the_remarketing_lists')) {
                    $file1 = $request->file('verify_the_remarketing_lists') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $verify_the_remarketing_lists = $fname1.$fileName1;
            }
            else{
                $verify_the_remarketing_lists = null;
            }
            unset($inputs['verify_the_remarketing_lists']);
            $inputs['verify_the_remarketing_lists'] = $verify_the_remarketing_lists;

            if(isset($inputs['report']) or !empty($inputs['report']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('report')) {
                    $file1 = $request->file('report') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $report = $fname1.$fileName1;
            }
            else{
                $report = null;
            }
            unset($inputs['report']);
            $inputs['report'] = $report;


                           
            $record = Report::create(["data" => json_encode($inputs), "created_by" => $user_id]);  

             
           return view('admin.report.index')
                ->with('success', lang('messages.created', lang('report.report')));
           
        }
        catch (Exception $exception) {
            //dd($exception);
            return redirect()->route('report.create')
                ->withInput()
                ->with('error', lang('messages.server_error'));
        }
    }


    public function update(Request $request, $id = null)
    {
        $result = Report::find($id);

        if (!$result) {
            return redirect()->route('report.index')
                ->with('error', lang('messages.invalid_id', string_manip(lang('report.report'))));
        }

        $inputs = $request->all();

        try {
          
            $report = json_decode($result->data, true);
            $user_id = authUserId();

            if(isset($inputs['audit_data']) or !empty($inputs['audit_data']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('audit_data')) {
                    $file1 = $request->file('audit_data') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $audit_data = $fname1.$fileName1;
            }
            else{
                $audit_data = $report['audit_data'];
            }
            unset($inputs['audit_data']);
            $inputs['audit_data'] = $audit_data;

            if(isset($inputs['google_tag_assistant']) or !empty($inputs['google_tag_assistant']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('google_tag_assistant')) {
                    $file1 = $request->file('google_tag_assistant') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $google_tag_assistant = $fname1.$fileName1;
            }
            else{
                $google_tag_assistant = $report['google_tag_assistant'];
            }
            unset($inputs['google_tag_assistant']);
            $inputs['google_tag_assistant'] = $google_tag_assistant;

            if(isset($inputs['conversion_actions']) or !empty($inputs['conversion_actions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('conversion_actions')) {
                    $file1 = $request->file('conversion_actions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $conversion_actions = $fname1.$fileName1;
            }
            else{
                $conversion_actions = $report['conversion_actions'];
            }
            unset($inputs['conversion_actions']);
            $inputs['conversion_actions'] = $conversion_actions;

            if(isset($inputs['google_ads']) or !empty($inputs['google_ads']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('google_ads')) {
                    $file1 = $request->file('google_ads') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $google_ads = $fname1.$fileName1;
            }
            else{
                $google_ads = $report['google_ads'];
            }
            unset($inputs['google_ads']);
            $inputs['google_ads'] = $google_ads;


            if(isset($inputs['negative_keyword_conflicts']) or !empty($inputs['negative_keyword_conflicts']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('negative_keyword_conflicts')) {
                    $file1 = $request->file('negative_keyword_conflicts') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $negative_keyword_conflicts = $fname1.$fileName1;
            }
            else{
                $negative_keyword_conflicts = $report['negative_keyword_conflicts'];
            }
            unset($inputs['negative_keyword_conflicts']);
            $inputs['negative_keyword_conflicts'] = $negative_keyword_conflicts;



            if(isset($inputs['new_campaign']) or !empty($inputs['new_campaign']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('new_campaign')) {
                    $file1 = $request->file('new_campaign') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $new_campaign = $fname1.$fileName1;
            }
            else{
                $new_campaign = $report['new_campaign'];
            }
            unset($inputs['new_campaign']);
            $inputs['new_campaign'] = $new_campaign;

            if(isset($inputs['schedule_settings']) or !empty($inputs['schedule_settings']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('schedule_settings')) {
                    $file1 = $request->file('schedule_settings') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $schedule_settings = $fname1.$fileName1;
            }
            else{
                $schedule_settings = $report['schedule_settings'];
            }
            unset($inputs['schedule_settings']);
            $inputs['schedule_settings'] = $schedule_settings;

            if(isset($inputs['all_features']) or !empty($inputs['all_features']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('all_features')) {
                    $file1 = $request->file('all_features') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $all_features = $fname1.$fileName1;
            }
            else{
                $all_features = $report['all_features'];
            }
            unset($inputs['all_features']);
            $inputs['all_features'] = $all_features;

            if(isset($inputs['search_partners']) or !empty($inputs['search_partners']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('search_partners')) {
                    $file1 = $request->file('search_partners') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $search_partners = $fname1.$fileName1;
            }
            else{
                $search_partners = $report['search_partners'];
            }
            unset($inputs['search_partners']);
            $inputs['search_partners'] = $search_partners;

            if(isset($inputs['ad_variations']) or !empty($inputs['ad_variations']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('ad_variations')) {
                    $file1 = $request->file('ad_variations') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $ad_variations = $fname1.$fileName1;
            }
            else{
                $ad_variations = $report['ad_variations'];
            }
            unset($inputs['ad_variations']);
            $inputs['ad_variations'] = $ad_variations;

            if(isset($inputs['dynamic_search_ads']) or !empty($inputs['dynamic_search_ads']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('dynamic_search_ads')) {
                    $file1 = $request->file('dynamic_search_ads') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $dynamic_search_ads = $fname1.$fileName1;
            }
            else{
                $dynamic_search_ads = $report['dynamic_search_ads'];
            }
            unset($inputs['dynamic_search_ads']);
            $inputs['dynamic_search_ads'] = $dynamic_search_ads;

            if(isset($inputs['current_budget']) or !empty($inputs['current_budget']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('current_budget')) {
                    $file1 = $request->file('current_budget') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $current_budget = $fname1.$fileName1;
            }
            else{
                $current_budget = $report['current_budget'];
            }
            unset($inputs['current_budget']);
            $inputs['current_budget'] = $current_budget;

            if(isset($inputs['bidding_strategy_image']) or !empty($inputs['bidding_strategy_image']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('bidding_strategy_image')) {
                    $file1 = $request->file('bidding_strategy_image') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $bidding_strategy_image = $fname1.$fileName1;
            }
            else{
                $bidding_strategy_image = $report['bidding_strategy_image'];
            }
            unset($inputs['bidding_strategy_image']);
            $inputs['bidding_strategy_image'] = $bidding_strategy_image;

            if(isset($inputs['bid_adjustments']) or !empty($inputs['bid_adjustments']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('bid_adjustments')) {
                    $file1 = $request->file('bid_adjustments') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $bid_adjustments = $fname1.$fileName1;
            }
            else{
                $bid_adjustments = $report['bid_adjustments'];
            }
            unset($inputs['bid_adjustments']);
            $inputs['bid_adjustments'] = $bid_adjustments;

            if(isset($inputs['location_targeting']) or !empty($inputs['location_targeting']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('location_targeting')) {
                    $file1 = $request->file('location_targeting') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $location_targeting = $fname1.$fileName1;
            }
            else{
                $location_targeting = $report['location_targeting'];
            }
            unset($inputs['location_targeting']);
            $inputs['location_targeting'] = $location_targeting;

            if(isset($inputs['targeting_img']) or !empty($inputs['targeting_img']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('targeting_img')) {
                    $file1 = $request->file('targeting_img') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $targeting_img = $fname1.$fileName1;
            }
            else{
                $targeting_img = $report['targeting_img'];
            }
            unset($inputs['targeting_img']);
            $inputs['targeting_img'] = $targeting_img;

            if(isset($inputs['semographic_specifications']) or !empty($inputs['semographic_specifications']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('semographic_specifications')) {
                    $file1 = $request->file('semographic_specifications') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $semographic_specifications = $fname1.$fileName1;
            }
            else{
                $semographic_specifications = $report['semographic_specifications'];
            }
            unset($inputs['semographic_specifications']);
            $inputs['semographic_specifications'] = $semographic_specifications;


            if(isset($inputs['search_result']) or !empty($inputs['search_result']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('search_result')) {
                    $file1 = $request->file('search_result') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $search_result = $fname1.$fileName1;
            }
            else{
                $search_result = $report['search_result'];
            }
            unset($inputs['search_result']);
            $inputs['search_result'] = $search_result;

            if(isset($inputs['keyword_list']) or !empty($inputs['keyword_list']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('keyword_list')) {
                    $file1 = $request->file('keyword_list') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $keyword_list = $fname1.$fileName1;
            }
            else{
                $keyword_list = $report['keyword_list'];
            }
            unset($inputs['keyword_list']);
            $inputs['keyword_list'] = $keyword_list;

            if(isset($inputs['home_vs_landing']) or !empty($inputs['home_vs_landing']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('home_vs_landing')) {
                    $file1 = $request->file('home_vs_landing') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $home_vs_landing = $fname1.$fileName1;
            }
            else{
                $home_vs_landing = $report['home_vs_landing'];
            }
            unset($inputs['home_vs_landing']);
            $inputs['home_vs_landing'] = $home_vs_landing;

            if(isset($inputs['new_offer']) or !empty($inputs['new_offer']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('new_offer')) {
                    $file1 = $request->file('new_offer') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $new_offer = $fname1.$fileName1;
            }
            else{
                $new_offer = $report['new_offer'];
            }
            unset($inputs['new_offer']);
            $inputs['new_offer'] = $new_offer;

            if(isset($inputs['grammatical_or_spelling']) or !empty($inputs['grammatical_or_spelling']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('grammatical_or_spelling')) {
                    $file1 = $request->file('grammatical_or_spelling') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $grammatical_or_spelling = $fname1.$fileName1;
            }
            else{
                $grammatical_or_spelling = $report['grammatical_or_spelling'];
            }
            unset($inputs['grammatical_or_spelling']);
            $inputs['grammatical_or_spelling'] = $grammatical_or_spelling;

            if(isset($inputs['call_to_action']) or !empty($inputs['call_to_action']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('call_to_action')) {
                    $file1 = $request->file('call_to_action') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $call_to_action = $fname1.$fileName1;
            }
            else{
                $call_to_action = $report['call_to_action'];
            }
            unset($inputs['call_to_action']);
            $inputs['call_to_action'] = $call_to_action;

             if(isset($inputs['test_insights']) or !empty($inputs['test_insights']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('test_insights')) {
                    $file1 = $request->file('test_insights') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $test_insights = $fname1.$fileName1;
            }
            else{
                $test_insights = $report['test_insights'];
            }
            unset($inputs['test_insights']);
            $inputs['test_insights'] = $test_insights;


             if(isset($inputs['display_campaigns']) or !empty($inputs['display_campaigns']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('display_campaigns')) {
                    $file1 = $request->file('display_campaigns') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $display_campaigns = $fname1.$fileName1;
            }
            else{
                $display_campaigns = $report['display_campaigns'];
            }
            unset($inputs['display_campaigns']);
            $inputs['display_campaigns'] = $display_campaigns;


             if(isset($inputs['search_terms']) or !empty($inputs['search_terms']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('search_terms')) {
                    $file1 = $request->file('search_terms') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $search_terms = $fname1.$fileName1;
            }
            else{
                $search_terms = $report['search_terms'];
            }
            unset($inputs['search_terms']);
            $inputs['search_terms'] = $search_terms;

             if(isset($inputs['match_type']) or !empty($inputs['match_type']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('match_type')) {
                    $file1 = $request->file('match_type') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $match_type = $fname1.$fileName1;
            }
            else{
                $match_type = $report['match_type'];
            }
            unset($inputs['match_type']);
            $inputs['match_type'] = $match_type;

             if(isset($inputs['conflict_tracking_document']) or !empty($inputs['conflict_tracking_document']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('conflict_tracking_document')) {
                    $file1 = $request->file('conflict_tracking_document') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $conflict_tracking_document = $fname1.$fileName1;
            }
            else{
                $conflict_tracking_document = $report['conflict_tracking_document'];
            }
            unset($inputs['conflict_tracking_document']);
            $inputs['conflict_tracking_document'] = $conflict_tracking_document;


            if(isset($inputs['verify_proper_search']) or !empty($inputs['verify_proper_search']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('verify_proper_search')) {
                    $file1 = $request->file('verify_proper_search') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $verify_proper_search = $fname1.$fileName1;
            }
            else{
                $verify_proper_search = $report['verify_proper_search'];
            }
            unset($inputs['verify_proper_search']);
            $inputs['verify_proper_search'] = $verify_proper_search;

            if(isset($inputs['number_of_keywords']) or !empty($inputs['number_of_keywords']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('number_of_keywords')) {
                    $file1 = $request->file('number_of_keywords') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $number_of_keywords = $fname1.$fileName1;
            }
            else{
                $number_of_keywords = $report['number_of_keywords'];
            }
            unset($inputs['number_of_keywords']);
            $inputs['number_of_keywords'] = $number_of_keywords;

            if(isset($inputs['zero_impressions']) or !empty($inputs['zero_impressions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('zero_impressions')) {
                    $file1 = $request->file('zero_impressions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $zero_impressions = $fname1.$fileName1;
            }
            else{
                $zero_impressions = $report['zero_impressions'];
            }
            unset($inputs['zero_impressions']);
            $inputs['zero_impressions'] = $zero_impressions;

            if(isset($inputs['keywords_with_poor_quality']) or !empty($inputs['keywords_with_poor_quality']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('keywords_with_poor_quality')) {
                    $file1 = $request->file('keywords_with_poor_quality') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $keywords_with_poor_quality = $fname1.$fileName1;
            }
            else{
                $keywords_with_poor_quality = $report['keywords_with_poor_quality'];
            }
            unset($inputs['keywords_with_poor_quality']);
            $inputs['keywords_with_poor_quality'] = $keywords_with_poor_quality;

            if(isset($inputs['missing_ad_extensions']) or !empty($inputs['missing_ad_extensions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('missing_ad_extensions')) {
                    $file1 = $request->file('missing_ad_extensions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $missing_ad_extensions = $fname1.$fileName1;
            }
            else{
                $missing_ad_extensions = $report['missing_ad_extensions'];
            }
            unset($inputs['missing_ad_extensions']);
            $inputs['missing_ad_extensions'] = $missing_ad_extensions;

            if(isset($inputs['non_performing_automated_extensions']) or !empty($inputs['non_performing_automated_extensions']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('non_performing_automated_extensions')) {
                    $file1 = $request->file('non_performing_automated_extensions') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $non_performing_automated_extensions = $fname1.$fileName1;
            }
            else{
                $non_performing_automated_extensions = $report['non_performing_automated_extensions'];
            }
            unset($inputs['non_performing_automated_extensions']);
            $inputs['non_performing_automated_extensions'] = $non_performing_automated_extensions;

            if(isset($inputs['current_approval_status']) or !empty($inputs['current_approval_status']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('current_approval_status')) {
                    $file1 = $request->file('current_approval_status') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $current_approval_status = $fname1.$fileName1;
            }
            else{
                $current_approval_status = $report['current_approval_status'];
            }
            unset($inputs['current_approval_status']);
            $inputs['current_approval_status'] = $current_approval_status;

            if(isset($inputs['ensure_sitelinks']) or !empty($inputs['ensure_sitelinks']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('ensure_sitelinks')) {
                    $file1 = $request->file('ensure_sitelinks') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $ensure_sitelinks = $fname1.$fileName1;
            }
            else{
                $ensure_sitelinks = $report['ensure_sitelinks'];
            }
            unset($inputs['ensure_sitelinks']);
            $inputs['ensure_sitelinks'] = $ensure_sitelinks;

            if(isset($inputs['call_extension']) or !empty($inputs['call_extension']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('call_extension')) {
                    $file1 = $request->file('call_extension') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $call_extension = $fname1.$fileName1;
            }
            else{
                $call_extension = $report['call_extension'];
            }
            unset($inputs['call_extension']);
            $inputs['call_extension'] = $call_extension;

            if(isset($inputs['call_reporting']) or !empty($inputs['call_reporting']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('call_reporting')) {
                    $file1 = $request->file('call_reporting') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $call_reporting = $fname1.$fileName1;
            }
            else{
                $call_reporting = $report['call_reporting'];
            }
            unset($inputs['call_reporting']);
            $inputs['call_reporting'] = $call_reporting;

            if(isset($inputs['validate_remarketing_code']) or !empty($inputs['validate_remarketing_code']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('validate_remarketing_code')) {
                    $file1 = $request->file('validate_remarketing_code') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $validate_remarketing_code = $fname1.$fileName1;
            }
            else{
                $validate_remarketing_code = $report['validate_remarketing_code'];
            }
            unset($inputs['validate_remarketing_code']);
            $inputs['validate_remarketing_code'] = $validate_remarketing_code;

            if(isset($inputs['validate_remarketing_list']) or !empty($inputs['validate_remarketing_list']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('validate_remarketing_list')) {
                    $file1 = $request->file('validate_remarketing_list') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $validate_remarketing_list = $fname1.$fileName1;
            }
            else{
                $validate_remarketing_list = $report['validate_remarketing_list'];
            }
            unset($inputs['validate_remarketing_list']);
            $inputs['validate_remarketing_list'] = $validate_remarketing_list;

            if(isset($inputs['verify_the_remarketing_lists']) or !empty($inputs['verify_the_remarketing_lists']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('verify_the_remarketing_lists')) {
                    $file1 = $request->file('verify_the_remarketing_lists') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $verify_the_remarketing_lists = $fname1.$fileName1;
            }
            else{
                $verify_the_remarketing_lists = $report['verify_the_remarketing_lists'];
            }
            unset($inputs['verify_the_remarketing_lists']);
            $inputs['verify_the_remarketing_lists'] = $verify_the_remarketing_lists;

            if(isset($inputs['report']) or !empty($inputs['report']))
            {
                $file_name = rand(100000, 999999);
                $fileName1 = '';
                if($file1 = $request->hasFile('report')) {
                    $file1 = $request->file('report') ;
                    $img_name1 = $file1->getClientOriginalName();
                    $fileName1 = $file_name.$img_name1;
                    $destinationPath1 = public_path().'/uploads/report/' ;
                    $file1->move($destinationPath1, $fileName1);
                }   
                $fname1 ='/uploads/report/';
                $report = $fname1.$fileName1;
            }
            else{
                $report = $report['report'];
            }
            unset($inputs['report']);
            $inputs['report'] = $report;


            Report::where('id', $id)
                ->update([
                'data' =>  json_encode($inputs),
                'updated_by' => $user_id,
            ]); 

          return redirect()->route('report.index')
                ->with('success', lang('messages.updated', lang('report.report')));
      
        } catch (\Exception $exception) {

          //dd($exception);
            return redirect()->route('report.edit',[$id])
                ->with('error', lang('messages.server_error'));
        }
    }
    

    private function generateTokenKey() {
        return md5(uniqid(rand(), true));
    }


    public function edit($id = null)
    {
        $result = Report::find($id);
        if (!$result) {
            abort(404);
        }
        
        $user_type = \Auth::user()->user_type;
        $user_id =  Auth::id();

        if($user_type == 1){
            return view('admin.report.create', compact('result'));
        } else {
           if($result->created_by == $user_id){
             return view('admin.report.create', compact('result'));
           } else {
            return back();
           }

        }
        
    }

      public function drop($id) {
        if (!\Request::ajax()) {
            return lang('messages.server_error');
        }

        $result = (new Report)->find($id);
        if (!$result) {
            // use ajax return response not abort because ajaz request abort not works
            abort(401);
        }

        try {
            // get the unit w.r.t id
             $result = (new Report)->find($id);
         
                 (new Report)->tempDelete($id);
                 $response = ['status' => 1, 'message' => lang('messages.deleted', lang('report.report'))];
             
        }
        catch (Exception $exception) {
            $response = ['status' => 0, 'message' => lang('messages.server_error')];
        }        
        // return json response
        return json_encode($response);
    }


    public function report_view($id = null){
        try{
            $result = Report::find($id);
            if (!$result) {
                abort(404);
            }
            $view = 0;
          return view('admin.report.report', compact('result', 'view'));

        } catch (\Exception $exception) {
           //dd($exception);
            return back();
        }
    }

    public function view_report(Request $request){

        try {

            $result = Report::where('id', $request->id)->first();
            if($result){
                $report = json_decode($result->data, true);
                if($request->password == $report['password']){
                    $view = 1;
                    return view('admin.report.report', compact('result', 'view'));
                } else {

                    return back()->with('wrong_password',  'wrong_password');
                }

            } else {
               return back(); 
            }
            



            
        } catch (Exception $e) {
            
            return back();
        }

    }
    
    public function share_report($id = null){
        try{
 
            $result = Report::find($id);
            if (!$result) {
                abort(404);
            }
            $report = json_decode($result->data, true);
            $data['name'] = $report['account_name'];
            $data['password'] = $report['password'];
            $data['id'] = $result->id;
            $email = $report['email'];

            \Mail::send('email.report', $data, function($message) use ($email){
                $message->from('info@ppc-tool.com');
                $message->to($email);
                $message->subject('PPC TOOL Report');
            });

            return back()->with('report_sent', 'report_sent');

        } catch (\Exception $exception) {
           // dd($exception);
            return redirect()->route('report.index')
                ->with('error', lang('messages.server_error'));
        }
    }

    public function reportPaginate(Request $request, $id, $pageNumber = null)
    {

        if (!\Request::isMethod('post') && !\Request::ajax()) { //
            return lang('messages.server_error');
        }

        $user_type = \Auth::user()->user_type;
        $user_id =  Auth::id();

        $inputs = $request->all();
        $page = 1;
        if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
            $page = $inputs['page'];
        }

        $perPage = 20;
        if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
            $perPage = $inputs['perpage'];
        }

        $start = ($page - 1) * $perPage;
        if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
            $inputs = array_filter($inputs);
            unset($inputs['_token']);

            $data = (new Report)->getReport($inputs, $start, $perPage, $user_type, $user_id);
            $totalGameMaster = (new Report)->totalReport($inputs, $user_type, $user_id);
            $total = $totalGameMaster->total;
        } else {

            $data = (new Report)->getReport($inputs, $start, $perPage, $user_type, $user_id);
            $totalGameMaster = (new Report)->totalReport('', $user_type, $user_id);
            $total = $totalGameMaster->total;
        }

        return view('admin.report.load_data', compact('inputs', 'data', 'total', 'page', 'perPage'));
    }




    public function reportToggle($id = null)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) {
            return lang('messages.server_error');
        }

        try {
            $game = Report::find($id);
            //dd($game);

        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('report')));
        }

        $game->update(['status' => !$game->status]);
        $response = ['status' => 1, 'data' => (int)$game->status . '.gif'];
 

        // return json response
        return json_encode($response);
    }


    public function reportAction(Request $request)
    {
        $inputs = $request->all();
        if (!isset($inputs['tick']) || count($inputs['tick']) < 1) {
             return view('admin.report.index')->with('error', lang('messages.atleast_one', string_manip(lang('report.report'))));
        }

        $ids = '';
        foreach ($inputs['tick'] as $key => $value) {
            $ids .= $value . ',';
        }

        $ids = rtrim($ids, ',');
        $status = 0;
        if (isset($inputs['active'])) {
            $status = 1;
        }

        Report::whereRaw('id IN (' . $ids . ')')->update(['status' => $status]);
        return redirect()->route('report.index')
            ->with('success', lang('messages.updated', lang('report.report')));
    }

   
}