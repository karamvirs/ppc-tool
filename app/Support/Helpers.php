<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;

/**
 * @param $status
 * @param $statusCode
 * @param $message
 * @param array $errors
 * @param array $result
 * @return \Illuminate\Http\JsonResponse
 */
function apiResponse($status, $statusCode, $message, $errors = [], $result = [])
{
    $response = ['success' => $status, 'status' => $statusCode];

    if ($message != "") {
        $response['message']['success'] = $message;
    }

    if (count($errors) > 0) {
        $response['message']['errors'] = $errors;
    }

    if (count($result) > 0) {
        $response['message']['data'] = $result;
    }
    return response()->json($response, $statusCode);
}


function lang($path = null, $string = null)
{
    $lang = $path;
    if (trim($path) != '' && trim($string) == '') {
        $lang = \Lang::get($path);
    } elseif (trim($path) != '' && trim($string) != '') {
        $lang = \Lang::get($path, ['attribute' => $string]);
    }
    return $lang;
}


//For App
function apiResponseApp($status, $statusCode, $message, $errors = [], $data = [])
{
    $response = ['success' => $status, 'status' => $statusCode];
    
    if ($message != "") {
        $response['message']['success'] = $message;
    }

    if(isset($errors)){
        if (count($errors) > 0) {
            $response['message']['errors'] = $errors;
        }
    }
    
    if (isset($data)) {
        $response['message']['data'] = $data;
    }
    return response()->json($response);
}

function apiResponseAppOther($status, $statusCode, $message, $errors = [], $data = [])
{
    $response = ['success' => $status, 'status' => $statusCode];
    
    if ($message != "") {
        $response['message']['failure'] = $message;
    }

    if (count($errors) > 0) {
        $response['message']['errors'] = $errors;
    }

    if (count($data) > 0) {
        $response['message']['data'] = $data;
    }
    return response()->json($response);
}




function errorMessages($errors = [])
{
    $error = [];
    foreach($errors->toArray() as $key => $value) {
        foreach($value as $messages) {
            $error[$key] = $messages;
        }
    }
    return $error;
}

function convertToLocal($utcDate, $format = null)
{
    $currentTimezone = getCompanySettings('timezone');
    $dateFormat = ($format != "") ? $format : getCompanySettings('datetime_format');
    if($currentTimezone !='') {
        $date = new \DateTime($utcDate, new DateTimeZone('UTC'));
        $date->setTimezone(new DateTimeZone($currentTimezone));
        return $date->format($dateFormat);
    } else {
        $date = new \DateTime($utcDate, new DateTimeZone('UTC'));
        return $date->format($dateFormat);
    }
}


function convertToUtc($localDate = null, $format = null)
{
    //$currentTimezone = getCompanySettings('timezone');
    //$currentTimezone = 'America/Los_Angeles';
    $currentTimezone = 'UTC';    
    $format = ($format == "") ? 'Y-m-d H:i:s' : $format;
    $localDate = ($localDate == "") ? date('Y-m-d H:i:s') : $localDate;
    $date = new \DateTime($localDate, new DateTimeZone($currentTimezone));
    $date->setTimezone(new DateTimeZone('UTC'));
    return $date->format($format);
}


function currentDate($withTime = false)
{
    $date = date('Y-m-d H:i:s');
    if (!$withTime) {
        $date = date('Y-m-d');
    }
    return $date;
}


function dateFormat($format, $date)
{
    if (trim($date) != '') {
        if (trim($date) == '0000-00-00' || trim($date) == '0000-00-00 00:00') {
            return null;
        } else {
            return date($format, strtotime($date));
        }
    }
    return $date;
}

/**
 * @return bool
 */
function isSystemAdmin()
{
    return (\Auth::user()->id == 1) ? true : false;

}

function isAdmin()
{
    if(\Auth::check()) {
        return (\Auth::user()->user_type == 1) ? true : false;
    }

}


function isSuperAdmin()
{
    if(\Auth::check()) {
        return (\Auth::user()->user_type == 1) ? true : false;
    }
}

function isDataEntry()
{
    if(\Auth::check()) {
        return (\Auth::user()->user_type == 2) ? true : false;
    }
}


function authUserId()
{
    $id = 1;
    if (\Auth::check()) {
        $id = \Auth::user()->id;
    }

    return $id;
}

/**
 * @return null
 */
function authUserIdNull()
{
    $id = null;
    if (\Auth::check()) {
        $id = \Auth::user()->id;
    }

    return $id;
}

function authUser()
{
    $user = null;
    if (\Auth::check()) {
        $user = \Auth::user();
    }
    return $user;
}


function arrayFilter($arr)
{
    foreach($arr as $key => $value)
    {
        if($value == '' || $value == null)
        {
            unset($arr[$key]);
        }
    }
    return $arr;
}


function validationResponse($status, $statusCode, $message = null, $url = null, $errors = [], $data = [])
{
    $response = ['success' => $status, 'status' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if ($url != "") {
        $response['url'] = $url;
    }

    if (count($errors) > 0) {
        $response['errors'] = errorMessages($errors);
    }

    if (count($data) > 0) {
        $response['data'] = $data;
    }
    return response()->json($response, $statusCode);
}

/**
 * @param $value
 * @param string $seprator
 *
 * @return string
 */
function numberFormat($value, $seprator = ',')
{
    return ($value > 0) ? number_format($value, 2, '.', $seprator) : '0.00';
}

/**
 * @param $icon
 * @return mixed
 */
function sortIcon($icon)
{
    $iconArray = [
        '0' => 'fa fa-sort',
        '2' => 'fa fa-sort-up',
        '1' => 'fa fa-sort-down',
    ];

    $icon = sortAction($icon);
    return $iconArray[$icon];
}

/**
 * @param $action
 *
 * @return int
 */
function sortAction($action)
{
    $sortAction = 0;
    if($action != "") {
        if ($action == 0) {
            $sortAction = 1;
        } elseif ($action == 1) {
            $sortAction = 2;
        } else {
            $sortAction = 1;
        }
        //$sortAction = ((int)$action === 0) ? 1 : ((int)$action === 1) ? 2 : 3;
    }
    return $sortAction;
}

/**
 * Method is used to return string in lower, upper or ucfirst.
 *
 * @param string $string
 * @param string $type L -> lower, U -> upper, UC -> upper character first
 * @return Response
 */
function string_manip($string = null, $type = 'L')
{
    switch ($type) {
        case 'U':
            return strtoupper($string);
            break;
        case 'UC':
            return ucfirst($string);
            break;
        case 'UCW':
            return ucwords($string);
            break;
        default:
            return strtolower($string);
            break;
    }
}

/**
 * Method is used to create pagination controls
 *
 * @param int $page
 * @param int $total
 * @param int $perPage
 *
 * @return string
 */
function paginationControls($page, $total, $perPage = 20)
{
    $paginates = '';
    $curPage = $page;
    $page -= 1;
    $previousButton = true;
    $next_btn = true;
    $first_btn = false;
    $last_btn = false;
    $noOfPaginations = ceil($total / $perPage);

    /* ---------------Calculating the starting and ending values for the loop----------------------------------- */
    if ($curPage >= 10) {
        $start_loop = $curPage - 5;
        if ($noOfPaginations > $curPage + 5) {
            $end_loop = $curPage + 5;
        } elseif ($curPage <= $noOfPaginations && $curPage > $noOfPaginations - 9) {
            $start_loop = $noOfPaginations - 9;
            $end_loop = $noOfPaginations;
        } else {
            $end_loop = $noOfPaginations;
        }
    } else {
        $start_loop = 1;
        if ($noOfPaginations > 10)
            $end_loop = 10;
        else
            $end_loop = $noOfPaginations;
    }

    $paginates .= '<div class="col-sm-5 padding0 pull-left custom-martop">' .
        lang('common.jump_to') .
        '<input type="text" class="goto" size="1" />
					<button type="button" id="go_btn" class="small-btn small-btn-primary"> <span class="fa fa-arrow-right"> </span> </button> ' .
        lang('common.pages') . ' ' .  $curPage . ' of <span class="_total">' . $noOfPaginations . '</span> | ' . lang('common.total_records', $total) .
        '</div> <ul class="pagination pagination-sm pull-right custom-martop">';

    // FOR ENABLING THE FIRST BUTTON
    if ($first_btn && $curPage > 1) {
        $paginates .= '<li p="1" class="disabled">
	    					<a href="javascript:void(0);">' .
            lang('common.first')
            . '</a>
	    			   </li>';
    } elseif ($first_btn) {
        $paginates .= '<li p="1" class="disabled">
	    					<a href="javascript:void(0);">' .
            lang('common.first')
            . '</a>
	    			   </li>';
    }

    // FOR ENABLING THE PREVIOUS BUTTON
    if ($previousButton && $curPage > 1) {
        $pre = $curPage - 1;
        $paginates .= '<li p="' . $pre . '" class="_paginate">
	    					<a href="javascript:void(0);" aria-label="Previous">
					        	<span aria-hidden="true">&laquo;</span>
				      		</a>
	    			   </li>';
    } elseif ($previousButton) {
        $paginates .= '<li class="disabled">
	    					<a href="javascript:void(0);" aria-label="Previous">
					        	<span aria-hidden="true">&laquo;</span>
				      		</a>
	    			   </li>';
    }

    for ($i = $start_loop; $i <= $end_loop; $i++) {
        if ($curPage == $i)
            $paginates .= '<li p="' . $i . '" class="active"><a href="javascript:void(0);">' . $i . '</a></li>';
        else
            $paginates .= '<li p="' . $i . '" class="_paginate"><a href="javascript:void(0);">' . $i . '</a></li>';
    }

    // TO ENABLE THE NEXT BUTTON
    if ($next_btn && $curPage < $noOfPaginations) {
        $nex = $curPage + 1;
        $paginates .= '<li p="' . $nex . '" class="_paginate">
	    					<a href="javascript:void(0);" aria-label="Next">
					        	<span aria-hidden="true">&raquo;</span>
					      	</a>
	    			   </li>';
    } elseif ($next_btn) {
        $paginates .= '<li class="disabled">
	    					<a href="javascript:void(0);" aria-label="Next">
					        	<span aria-hidden="true">&raquo;</span>
					      	</a>
	    			   </li>';
    }

    // TO ENABLE THE END BUTTON
    if ($last_btn && $curPage < $noOfPaginations) {
        $paginates .= '<li p="' . $noOfPaginations . '" class="_paginate">
	    					<a href="javascript:void(0);">' .
            lang('common.last')
            . '</a>
	    			   </li>';
    } elseif ($last_btn) {
        $paginates .= '<li p="' . $noOfPaginations . '" class="disabled">
	    					<a href="javascript:void(0);">' .
            lang('common.last')
            . '</a>
			   		   </li>';
    }

    $paginates .= '</ul>';

    return $paginates;
}

/**
 * Trim whitespace from inputs
 *
 * @param Request $request
 * @return bool
 */
function trimInputs()
{
    $inputs = Input::all();
    array_walk_recursive($inputs, function (&$value) {
        $value = trim($value);
    });
    Input::merge($inputs);
    return true;
}

/**
 * @param $index
 * @param $page
 * @param $perPage
 * @return mixed
 */
function pageIndex($index, $page, $perPage)
{
    return (($page - 1) * $perPage) + $index;
}



/**
 * @param null $id
 * @return array|null
 */
function getRequestStatus($id = null)
{
    $status = [
        1 => lang('common.pending'),
        2 => lang('common.partially'),
        3 => lang('common.completed'),
    ];
    if ($id) {
        if (!array_key_exists($id, $status)) {
            return null;
        }
        return $status[$id];
    }
    return $status;
}


function getPermittedRoutes($id = 1)
{
    $routes = [
        1 => [
            'products.index',
            'products.create',
            'products.edit',
            'products.update',
            'products.drop',
            'product-group.index',
            'product-group.create',
            'product-group.edit',
            'product-group.update',
            'product-group.drop',
            'unit.index',
            'unit.create',
            'unit.edit',
            'unit.update',
            'unit.drop',
            'qualification.index',
            'qualification.create',
            'qualification.edit',
            'qualification.update',
            'qualification.drop',
            'specialization.index',
            'specialization.create',
            'specialization.edit',
            'specialization.update',
            'specialization.drop',
            'request-sample.detail',
            'allotted-item-delete',
            'request-sample.cancel',
            'request-sample-list',
            'request-sample.paginate',
            'request.allot',
            'edit.allotted-item'
        ],
        2=> [
            'report.attendances',
        ],
        3 => [
            'report.expenses',
        ],
        4 => [
            'report.tours'
        ]
    ];
    return $routes[$id];
}


