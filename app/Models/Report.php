<?php

 namespace App\Models;
 use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

 class Report extends Model
 {
     use SoftDeletes;

     protected $table = 'reports';

     protected $fillable = [
        'data',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at'
     ];

   
    public function scopeActive($query)
    {
         return $query->where('status', 1);
    }


    public function store($input, $id = null)
    {
         if ($id) {
             return $this->find($id)->update($input);
         } else {
             return $this->create($input)->id;
         }
    }


    public function getReport($search = null, $skip, $perPage, $user_type, $user_id)
    {
         $take = ((int)$perPage > 0) ? $perPage : 20;
         $filter = 1; // default filter if no search

         $fields = [
            'reports.id',
            'reports.data',
            'reports.created_by',
            'reports.status',
            'users.name',
            'users.id as user_id'
         ];

         $sortBy = [
             'data' => 'data',
         ];

         $orderEntity = 'id';
         $orderAction = 'desc';
         if (isset($search['sort_action']) && $search['sort_action'] != "") {
             $orderAction = ($search['sort_action'] == 1) ? 'desc' : 'asc';
         }

         if (isset($search['sort_entity']) && $search['sort_entity'] != "") {
             $orderEntity = (array_key_exists($search['sort_entity'], $sortBy)) ? $sortBy[$search['sort_entity']] : $orderEntity;
         }

         if (is_array($search) && count($search) > 0) {
             $keyword = (array_key_exists('keyword', $search)) ?
                 " AND (data LIKE '%" .addslashes(trim($search['keyword'])) . "%')" : "";
             $filter .= $keyword;
         }
        if($user_type == 2){
            return $this->join('users', 'users.id' ,'=', 'reports.created_by')
             ->whereRaw($filter)
             ->where('reports.created_by', $user_id)
             ->orderBy($orderEntity, $orderAction)
             ->skip($skip)->take($take)->get($fields);
        } else {
            return $this->join('users', 'users.id' ,'=', 'reports.created_by')
             ->whereRaw($filter)
             ->orderBy($orderEntity, $orderAction)
             ->skip($skip)->take($take)->get($fields);
        }
    }


     public function totalReport($search = null, $user_type, $user_id)
     {
         $filter = 1; // if no search add where

         // when search
         if (is_array($search) && count($search) > 0) {
             $partyName = (array_key_exists('keyword', $search)) ? " AND name LIKE '%" .
                 addslashes(trim($search['keyword'])) . "%' " : "";
             $filter .= $partyName;
         }
        if($user_type == 2){
          return $this->select(\DB::raw('count(*) as total'))
             ->whereRaw($filter)->where('created_by', $user_id)->first();
        } else {
           return $this->select(\DB::raw('count(*) as total'))
             ->whereRaw($filter)->first();
        }
         
     }

     public function tempDelete($id)
    {
        $this->find($id)->update([ 'deleted_by' => authUserId(), 'deleted_at' => convertToUtc()]);
    }

 
}