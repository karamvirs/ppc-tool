<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{

    protected $fillable = [
        'type', 'status', 'created_at', 
        'updated_at', 'deleted_at'
    ];
}
