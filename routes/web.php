<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\AuthController@getLogin')->name('admin');
Route::any('/admin/login', 'Auth\AuthController@postLogin')->name('admin.login');
Route::get('/admin/logout', array('as' => 'admin-logout','uses' => 'Auth\AuthController@adminLogout'));

Route::group(['middleware' => 'auth', 'after' => 'no-cache'], function () {

    Route::prefix('admin')->group(function () {

        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
        
     
        // Customer route start
            Route::resource('customer','CustomerController', [
                'names' => [
                    // 'index'     => 'customer.index',
                    'create'    => 'customer.create',
                    'store'     => 'customer.store',
                    'edit'      => 'customer.edit',
                    'update'    => 'customer.update',
                ],
                'except' => ['show','destroy']
            ]);
            Route::get('customer', 'CustomerController@index')->name('customer');
            Route::any('customer/paginate/{page?}', ['as' => 'customer.paginate',
                'uses' => 'CustomerController@customerPaginate']);
            Route::any('customer/action', ['as' => 'customer.action',
                'uses' => 'CustomerController@customerAction']);
            Route::any('customer/toggle/{id?}', ['as' => 'customer.toggle',
                'uses' => 'CustomerController@customerToggle']);
            Route::any('customer/drop/{id?}', ['as' => 'customer.drop',
                'uses' => 'CustomerController@drop']);
            Route::any('customer/export-users', 'CustomerController@export_users')->name('customer.export-users');

            Route::any('customer/update-password/{id}', 'CustomerController@updatePassword')->name('customer.update-password');
            Route::post('customer/update-password-save', 'CustomerController@updatePasswordSave')->name('customer.update-password-save');
            // Customer route end   


            // Change Password Routes
            Route::any('myaccount', ['as' => 'setting.manage-account',
                'uses' => 'SettingController@myAccount']);
            // Change Password Routes


            // Report route start
            Route::resource('report','ReportController', [
                'names' => [
                    'index'     => 'report.index',
                    'create'    => 'report.create',
                    'store'     => 'report.store',
                    'edit'      => 'report.edit',
                    'update'    => 'report.update',
                ],
                'except' => ['show','destroy']
            ]);

            Route::any('report/paginate/{page?}', ['as' => 'report.paginate',
                'uses' => 'ReportController@reportPaginate']);
            Route::any('report/action', ['as' => 'report.action',
                'uses' => 'ReportController@reportAction']);
            Route::any('report/toggle/{id?}', ['as' => 'report.toggle',
                'uses' => 'ReportController@reportToggle']);
            Route::any('report/drop/{id?}', ['as' => 'report.drop',
                'uses' => 'ReportController@drop']);
            Route::any('report/share/{id}', 'ReportController@share_report')->name('share_report');
            // Report route end



   });
});

            Route::any('view-report/{id}', 'ReportController@report_view')->name('report_view');
            Route::any('view-report', 'ReportController@view_report')->name('view-report');  




